<?php 

// sau 2 ví dụ dưới đây thì

// throw để tạo ra thông báo lỗi theo từng trường hợp lỗi mình quy định sẵn, throw được đặt trong 1 function hoặc trong try

// try nơi thực thi lệch

// catch nơi lấy lỗi


try {
	$error = 'Luôn luôn ném lỗi này!';
	throw new Exception($error);

	// Phần code mà theo sau một ngoại lệ sẽ không được thực thi.
	echo 'Phần code này không bao giờ được thực thi';

} catch (Exception $e) {
	echo 'Bắt ngoại lệ: ', $e->getMessage(), "<br>";
}

// Tiếp tục tiến trình thực thi
echo "Hello World!<br>";








//create function with an exception
function checkNum($number) {
  if($number>1) {
    throw new Exception("Số phải nhỏ hơn hoặc bằng 1!");
  }

  if($number<1) {
    throw new Exception("Số phải bằng 1!");
  }
  //throw new Exception("Đây là số 1!");
  return true;
}

//trigger exception in a "try" block

//checkNum(2);
try {
  checkNum(1);

  $num = 1/0;
  throw new Exception($num = 1/0);
  //If the exception is thrown, this text will not be shown
  echo 'Nếu bạn nhìn thấy thông báo này thì số đã nhỏ hơn hoặc bằng 1';
}

//catch exception
catch(Exception $e) {
  echo 'Thông báo: ' .$e->getMessage();
}






try {
  //Khối lệnh thực thi
} catch (Exception $exception) {
  //Xử lý ngoại lệ
}

function phepChia($soChia, $soBiChia)
{
  if ($soBiChia == 0) {
    throw new Exception("Không thể chia cho 0");
  } else {
    $ketQua = $soChia / $soBiChia;
    return $ketQua;
  }
}

try {
  echo "Kết quả là: " . phepChia(5, 0);
} catch (Exception $exception) {
  echo "Lỗi phát sinh : " . $exception->getMessage();
}
echo "<p>Kết thúc phép toán</p>";












try {
  //Khối lệnh thực thi
} catch (Exception $exception) {
  //Xử lý ngoại lệ
} finally {
  //Khối lệnh tiếp tục được xử lý
}

function phepChia($soChia, $soBiChia)
{
  if ($soBiChia == 0) {
    throw new Exception("Không thể chia cho 0");
  } else {
    $ketQua = $soChia / $soBiChia;
    return $ketQua;
  }
}

try {
  echo "Kết quả là: " . phepChia(5, 0);
} catch (Exception $exception) {
  echo "Lỗi phát sinh : " . $exception->getMessage();
} finally {
  echo "<p>Khối lệnh trong finally</p>";
}
echo "<p>Kết thúc phép toán</p>";