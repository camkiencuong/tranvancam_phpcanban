<?php

require MODEL_PATH . 'Review.php';

class ReviewController {

	protected $reviewModel;

	public function __construct()
	{
		$this->reviewModel = new Review();
	}

	public function index()
	{
		$where = $orderBy = '';
		$page = isset($_GET['page']) && $_GET['page'] != '' ? $_GET['page'] : 1;
		$limit = isset($_GET['limit']) && $_GET['limit'] != '' ? $_GET['limit'] : 10;
		$offset = ($page-1)*$limit;
		$queryLimit = queryString($_GET,['limit','page']);
		$queryOrderby = queryString($_GET,['col','by']);
		$queryString = queryString($_GET,'page');

		if (isset($_GET['col']) && isset($_GET['by'])) {
			$col = $_GET['col']; $by = $_GET['by'];
			$orderBy = " ORDER BY $col $by";
		}

		if (isset($_GET['keyword']) && $_GET['keyword'] != '') {
			$keyword = trim($_GET['keyword']);
			$where = "AND reviews.content LIKE '%$keyword%'";
		}
		if (isset($_POST['finding']) && $_POST['keyword'] != '') {
			header('location: index.php?c=review&keyword=' . trim($_POST['keyword']));exit;
		}

		$reviews = $this->reviewModel->getReviews($where, $limit, $offset, $orderBy);
		$totalRecord = $this->reviewModel->totalReview($where);

		$data = [
			'reviews' => $reviews,
			'totalRecord' => $totalRecord,
			'limit' => $limit,
			'page' => $page,
			'queryString' => $queryString,
			'queryLimit' => $queryLimit,
			'queryOrderby' => $queryOrderby,
		];
		return view('reviews.index', $data);
	}

	public function update()
	{
		if (isset($_GET['id']) && $_GET['id'] != '') {
			$id = (int)$_GET['id'];
			$where = "AND reviews.id=$id";
			$review = $this->reviewModel->getReview($where);
			if (is_null($review)) {
				header('location: index.php?c=review');
				exit;
			}
		}

		if (isset($_POST['submit'])) {
			$id2 = trim($_POST['data']['id']);
			$content = trim($_POST['data']['content']);
			$status = trim($_POST['data']['status']);

			$where = "AND reviews.id=$id AND reviews.id<>$id2";
			$testId = $this->reviewModel->getReview($where);
			if (!is_null($testId)) {
				$_SESSION['flash_msg'] = "ID = $id2 đã tồn tại!";
			} else {
				$set = "content='$content', status=$status";
				$result = $this->reviewModel->editReview($set, $id);
				if (is_null($result)) {
					$_SESSION['flash_msg'] = 'Cập nhật thành công!';
					header('location: index.php?c=review');
					exit;
				}
			}
		}

		$data = [
			'review' => $review,
		];

		return view('reviews.update', $data);
	}

	public function delete()
	{
		if (isset($_GET['id']) && $_GET['id'] != '') {
			$id = (int)$_GET['id'];
			$where = "AND reviews.id=$id";
			$review = $this->reviewModel->getReview($where);
			if (!is_null($review)) {
				$this->reviewModel->delReview($id);
			}
		}
		header('location: index.php?c=review');
		exit;
	}

}