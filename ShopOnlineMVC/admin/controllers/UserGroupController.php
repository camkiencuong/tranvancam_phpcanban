<?php

require MODEL_PATH . 'UserGroup.php';

class UserGroupController {

	protected $userGroupModel;

	public function __construct()
	{
		$this->userGroupModel = new UserGroup();
	}

	public function index()
	{
		$where = $orderBy = '';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
		$offset = ($page-1)*$limit;
		$queryLimit = queryString($_GET,['limit','page']);
		$queryOrderby = queryString($_GET,['col','by']);
		$queryString = queryString($_GET,'page');

		if (isset($_GET['col']) && isset($_GET['by'])) {
			$col = $_GET['col']; $by = $_GET['by'];
			$orderBy = " ORDER BY $col $by";
		}

		if (isset($_GET['keyword']) && $_GET['keyword'] != '') {
			$keyword = trim($_GET['keyword']);
			$where = "AND name LIKE '%$keyword%'";
		}
		if (isset($_POST['finding']) && $_POST['keyword'] != '') {
			header('location: index.php?c=usergroup&keyword=' . trim($_POST['keyword']));exit;
		}

		$usergroups = $this->userGroupModel->getUserGroups($where, $limit, $offset, $orderBy);
		$totalRecord = $this->userGroupModel->totalUserGroup($where);

		$data = [
			'usergroups' => $usergroups,
			'totalRecord' => $totalRecord,
			'page' => $page,
			'limit' => $limit,
			'queryString' => $queryString,
			'queryLimit' => $queryLimit,
			'queryOrderby' => $queryOrderby,
		];
		
		return view('user_groups.index', $data);
	}
	
	public function create()
	{
		$lastUserGroup = $this->userGroupModel->getUserGroup('ORDER BY id DESC');
		$idMax = $lastUserGroup['id'];
		$data = [
			'idMax' => $idMax,
		];

		if (isset($_POST['submit'])) {
			$id = $idMax + 1;
			$name = trim($_POST['data']['name']);
			$type = trim($_POST['data']['type']);
			$amount = trim($_POST['data']['amount']) == '' ? 0 : trim($_POST['data']['amount']);

			if ($name == '') {
				$_SESSION['flash_msg'] = 'Tên nhóm không được bỏ trống!';
			} else {
				$value = "$id, '$name', '$type', $amount";
				$result = $this->userGroupModel->addUserGroup($value);
				if (is_null($result)) {
					$_SESSION['flash_msg'] = 'Thêm thành công!';
					header('location: index.php?c=usergroup');
					exit;
				}
			}
		}
		return view('user_groups.create', $data);
	}
	
	public function update()
	{
		if (isset($_GET['id']) && $_GET['id'] != '') {
			$id = (int)$_GET['id'];
			$where = "AND id=$id";
			$usergroup = $this->userGroupModel->getUserGroup($where);
			if (is_null($usergroup)) {
				header('location: index.php?c=usergroup');
				exit;
			}
			$data = [
				'usergroup' => $usergroup,
			];
		}

		if (isset($_POST['submit'])) {
			$id2 = trim($_POST['data']['id']);
			$name = trim($_POST['data']['name']);
			$type = trim($_POST['data']['type']);
			$amount = trim($_POST['data']['amount']) == '' ? 0 : trim($_POST['data']['amount']);

			if ($name == '') {
				$_SESSION['flash_msg'] = 'Tên nhóm không được bỏ trống!';
			}

			$where = "AND id=$id AND id<>$id2";
			$testId = $this->userGroupModel->getUserGroup($where);
			if (!is_null($testId)) {
				$_SESSION['flash_msg'] = "ID = $id2 đã tồn tại";
			}

			if (!isset($_SESSION['flash_msg'])) {
				$set = "name='$name', type='$type', amount=$amount";
				$result = $this->userGroupModel->editUserGroup($set, $id);
				if (is_null($result)) {
					$_SESSION['flash_msg'] = 'Cập nhật thành công!';
					header('location: index.php?c=usergroup');
					exit;
				}
			}

		}
		return view('user_groups.update', $data);
	}
	
	public function delete()
	{
		if (isset($_GET['id']) && $_GET['id'] != '') {
			$id = (int)$_GET['id'];
			$where = "AND id=$id";
			$result = $this->userGroupModel->getUserGroup($where);
			if (!is_null($result)) {
				$this->userGroupModel->delUserGroup($id);
			}
		}
		header('location: index.php?c=usergroup');
		exit;
	}

}