<?php

require MODEL_PATH . 'ProductCategory.php';

class ProductCategoryController {

	protected $productCategoryModel;

	public function __construct()
	{
		$this->productCategoryModel = new ProductCategory();
	}

	public function index()
	{
		$where = $orderBy = '';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
		$offset = ($page-1)*$limit;
		$queryLimit = queryString($_GET,['limit','page']);
		$queryOrderby = queryString($_GET,['col','by']);
		$queryString = queryString($_GET,'page');

		if (isset($_GET['col']) && isset($_GET['by'])) {
			$col = $_GET['col']; $by = $_GET['by'];
			$orderBy = " ORDER BY $col $by";
		}

		if (isset($_GET['keyword']) && $_GET['keyword'] != '') {
			$keyword = trim($_GET['keyword']);
			$where = "AND name LIKE '%$keyword%'";
		}
		if (isset($_POST['finding']) && $_POST['keyword'] != '') {
			header('location: index.php?c=productcategory&keyword=' . trim($_POST['keyword']));exit;
		}

		$categories = $this->productCategoryModel->getCategories($where, $limit, $offset, $orderBy);
		$totalRecord = $this->productCategoryModel->totalCategory($where);
		$data = [
			'categories' => $categories,
			'totalRecord' => $totalRecord,
			'page' => $page,
			'limit' => $limit,
			'queryLimit' => $queryLimit,
			'queryOrderby' => $queryOrderby,
			'queryString' => $queryString,
		];

		return view('product_categories.index', $data);
	}

	public function create()
	{
		$categories = $this->productCategoryModel->getCategories();
		$lastCategory = $categories[count($categories)-1];
		$idMax = $lastCategory['id'];
		$data = [
			'categories' => $categories,
			'idMax' => $idMax,
		];

		if (isset($_POST['submit'])) {
			$dataPost = $_POST['data'];

			$id = $idMax + 1;
			$name = trim($dataPost['name']);
			$slug = to_slug($name);
			$image = trim($dataPost['image']);
			$description = trim($dataPost['description']);
			$parent_id = $dataPost['parent_id'];
			$meta_title = trim($dataPost['meta_title']);
			$meta_keyword = trim($dataPost['meta_keyword']);
			$meta_description = trim($dataPost['meta_description']);
			$status = $dataPost['status'];

			if ($name == '') {
				$_SESSION['flash_msg'] = 'Tên danh mục không được bỏ trống!';
			}

			$where = " AND slug='$slug'";
			$testSlug = $this->productCategoryModel->getCategory($where);
			if (count($testSlug) > 0) {
				$_SESSION['flash_msg'] = "Slug = '$slug' đã tồn tại!";
			}

			if (!isset($_SESSION['flash_msg'])) {
				$dataStr = "'$id', '$name', '$slug', '$image', '$description', '$parent_id', '$meta_title', '$meta_keyword', '$meta_description', '$status'";
				$result = $this->productCategoryModel->addCategory($dataStr);
				if (is_null($result)) {
					$_SESSION['flash_msg'] = 'Thêm thành công!';
					header('location: index.php?c=productcategory');
					exit;
				}
			}
		}
		return view('product_categories.create', $data);
	}

	public function update()
	{
		if (isset($_GET['id']) && $_GET['id'] != '') {
			$id = (int)$_GET['id'];

			$where = "AND id=$id";
			$category = $this->productCategoryModel->getCategory($where);
			$where = "AND id<>$id";
			$categories = $this->productCategoryModel->getCategories($where);
			if (is_null($category)) {
				header('location: index.php?c=productcategory');
				exit;
			}

			$data = [
				'category' => $category,
				'categories' => $categories,
			];
		}

		if (isset($_POST['submit'])) {
			$dataPost = $_POST['data'];

			$id2 = trim($dataPost['id']);
			$name = trim($dataPost['name']);
			$slug = to_slug($name);
			$image = trim($dataPost['image']);
			$description = trim($dataPost['description']);
			$parent_id = trim($dataPost['parent_id']);
			$meta_title = trim($dataPost['meta_title']);
			$meta_keyword = trim($dataPost['meta_keyword']);
			$meta_description = trim($dataPost['meta_description']);
			$status = trim($dataPost['status']);

			if ($name == '') {
				$_SESSION['flash_msg'] = 'Tên danh mục không được bỏ trống!';
			}

			$where = " AND id='$id' AND id<>'$id2'";
			$testId = $this->productCategoryModel->getCategory($where);
			if (count($testId) > 0) {
				$_SESSION['flash_msg'] = "Id = '$id2' đã tồn tại!";
			}

			$where = " AND slug='$slug' AND id<>'$id2'";
			$testSlug = $this->productCategoryModel->getCategory($where);
			if (count($testSlug) > 0) {
				$_SESSION['flash_msg'] = "Slug = '$slug' đã tồn tại!";
			}

			if (!isset($_SESSION['flash_msg'])) {
				$set = "name='$name', slug='$slug', image='$image', description='$description', parent_id=$parent_id, meta_title='$meta_title', meta_keyword='$meta_keyword', meta_description='$meta_description', status=$status";
				$result = $this->productCategoryModel->editCategory($set, $id);
				if (is_null($result)) {
					$_SESSION['flash_msg'] = 'Cập nhật thành công!';
					header('location: index.php?c=productcategory');
					exit;
				}
			}
		}
		return view('product_categories.update', $data);
	}

	public function delete()
	{
		if (isset($_GET['id']) && $_GET['id'] != '') {
			$id = (int)$_GET['id'];

			$where = "AND id=$id";
			$category = $this->productCategoryModel->getCategory($where);
			if (!is_null($category)) {
				$result = $this->productCategoryModel->delCategory($id);
			}
			header('location: index.php?c=productcategory');
			exit;
		}
	}

}