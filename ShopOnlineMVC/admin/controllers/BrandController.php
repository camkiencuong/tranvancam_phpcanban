<?php 

require MODEL_PATH . 'Brand.php';

class BrandController {

	protected $brandModel;

	public function __construct()
	{
		$this->brandModel = new Brand();
	}

	public function index()
	{
		$where = $orderBy = '';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
		$offset = ($page-1)*$limit;
		$queryLimit = queryString($_GET,['limit','page']);
		$queryOrderby = queryString($_GET,['col','by']);
		$queryString = queryString($_GET,'page');

		if (isset($_GET['col']) && isset($_GET['by'])) {
			$col = $_GET['col']; $by = $_GET['by'];
			$orderBy = " ORDER BY $col $by";
		}

		if (isset($_GET['keyword']) && $_GET['keyword'] != '') {
			$keyword = trim($_GET['keyword']);
			$where = "AND name LIKE '%$keyword%'";
		}
		if (isset($_POST['finding']) && $_POST['keyword'] != '') {
			header('location: index.php?c=brand&keyword=' . trim($_POST['keyword']));exit;
		}

		$brands = $this->brandModel->getBrands($where, $limit, $offset, $orderBy);
		$totalRecord = $this->brandModel->totalBrand($where);
		$data = [
			'brands' => $brands,
			'totalRecord' => $totalRecord,
			'page' => $page,
			'limit' => $limit,
			'queryLimit' => $queryLimit,
			'queryOrderby' => $queryOrderby,
			'queryString' => $queryString,

		];
		return view('brands.index', $data);
	}
	
	public function create()
	{
		$lastBrand = $this->brandModel->getbrand('ORDER BY id DESC');
		$idMax = $lastBrand['id'];

		$data = [
			'idMax' => $idMax,
		];
		if (isset($_POST['submit'])) {
			$dataPost = $_POST['data'];

			$id = $idMax + 1;
			$name = trim($dataPost['name']);
			$slug = to_slug($name);
			$logo = trim($dataPost['logo']);
			$image = trim($dataPost['image']);
			$description = trim($dataPost['description']);
			$meta_title = trim($dataPost['meta_title']);
			$meta_keyword = trim($dataPost['meta_keyword']);
			$meta_description = trim($dataPost['meta_description']);
			$status = trim($dataPost['status']);

			if ($name == '') {
				$_SESSION['flash_msg'] = 'Tên thương hiệu không được bỏ trống!';
			}

			$where = " AND brands.slug='$slug'";
			$testSlug = $this->brandModel->getbrand($where);
			if (count($testSlug) > 0) {
				$_SESSION['flash_msg'] = "Slug = '$slug' đã tồn tại!";
			}

			if (!isset($_SESSION['flash_msg'])) {
				$dataStr = "'$id', '$name', '$slug', '$logo', '$image', '$description', '$meta_title', '$meta_keyword', '$meta_description', '$status'";
				$result = $this->brandModel->addBrand($dataStr);
				if (is_null($result)) {
					$_SESSION['flash_msg'] = 'Thêm thành công!';
					header('location: index.php?c=brand');
					exit;
				}
			}
		}
		return view('brands.create', $data);
	}
	
	public function update()
	{
		if (isset($_GET['id']) && $_GET['id'] != '') {
			$id = (int)$_GET['id'];

			$where = "AND id=$id";
			$brand = $this->brandModel->getBrand($where);
			if (is_null($brand)) {
				header('location: index.php?c=brand');
				exit;
			}

			$data = [
				'brand' => $brand,
			];

			if (isset($_POST['submit'])) {
				$dataPost = $_POST['data'];

				$id2 = trim($dataPost['id']);
				$name = trim($dataPost['name']);
				$slug = to_slug($name);
				$logo = trim($dataPost['logo']);
				$image = trim($dataPost['image']);
				$description = trim($dataPost['description']);
				$meta_title = trim($dataPost['meta_title']);
				$meta_keyword = trim($dataPost['meta_keyword']);
				$meta_description = trim($dataPost['meta_description']);
				$status = trim($dataPost['status']);

				if ($name == '') {
					$_SESSION['flash_msg'] = 'Tên thương hiệu không được bỏ trống!';
				}

				$where = " AND id='$id' AND id<>'$id2'";
				$testId = $this->brandModel->getbrand($where);
				if (count($testId) > 0) {
					$_SESSION['flash_msg'] = "id = '$id2' đã tồn tại!";
				}

				$where = " AND slug='$slug' AND id<>'$id2'";
				$testSlug = $this->brandModel->getbrand($where);
				if (count($testSlug) > 0) {
					$_SESSION['flash_msg'] = "Slug = '$slug' đã tồn tại!";
				}

				if (!isset($_SESSION['flash_msg'])) {
					$set = "name='$name', slug='$slug', logo='$logo', image='$image', description='$description', meta_title='$meta_title', meta_keyword='$meta_keyword', meta_description='$meta_description', status=$status";
					$result = $this->brandModel->editBrand($set,$id);
					if (is_null($result)) {
						$_SESSION['flash_msg'] = 'Cập nhật Thành công!';
						header('location: index.php?c=brand');
						exit;
					}
				}
			}
			return view('brands.update', $data);
		}		
	}
	
	public function delete()
	{
		if (isset($_GET['id']) && $_GET['id'] != '') {
			$id = (int)$_GET['id'];

			$where = "AND id=$id";
			$brand = $this->brandModel->getBrand($where);
			if (!is_null($brand)) {
				$brand = $this->brandModel->delBrand($id);
			}
			header('location: index.php?c=brand');
			exit;
		}
	}

}