<?php

require MODEL_PATH . 'Product.php';

class ProductController {

	protected $productModel;

	public function __construct()
	{
		$this->productModel = new Product();
	}

	public function index()
	{
		$where = $orderBy = '';
		$page = isset($_GET['page']) ? $_GET['page'] : 1;
		$limit = isset($_GET['limit']) ? $_GET['limit'] : 10;
		$offset = ($page-1)*$limit;
		$queryLimit = queryString($_GET,['limit','page']);
		$queryOrderby = queryString($_GET,['col','by']);
		$queryString = queryString($_GET,'page');

		if (isset($_GET['col']) && isset($_GET['by'])) {
			$col = $_GET['col']; $by = $_GET['by'];
			$orderBy = " ORDER BY $col $by";
		}

		if (isset($_GET['keyword']) && $_GET['keyword'] != '') {
			$keyword = trim($_GET['keyword']);
			$where = "AND products.name LIKE '%$keyword%'";
		}
		if (isset($_POST['finding']) && $_POST['keyword'] != '') {
			header('location: index.php?c=product&keyword=' . trim($_POST['keyword']));exit;
		}

		$products = $this->productModel->getProducts($where, $limit, $offset, $orderBy);
		$totalRecord = $this->productModel->totalProduct($where);

		// chỉ những thứ truyền vào mảng data mới khả dụng hiển thị trong view
		$data = [
			'products' => $products,
			'totalRecord' => $totalRecord,
			'page' => $page,
			'limit' => $limit,
			'offset' => $offset,
			'queryLimit' => $queryLimit,
			'queryOrderby' => $queryOrderby,
			'queryString' => $queryString,
		];

		return view('products.index', $data);
	}

	public function create()
	{
		$lastProduct = $this->productModel->getProduct('', 'ORDER BY products.id DESC');
		$idMax = $lastProduct['id'];
		$brands = $this->productModel->getBrands();
		$categories = $this->productModel->getCategories();

		$data = [
			'idMax' => $idMax,
			'brands' => $brands,
			'categories' => $categories,
		];

		if (isset($_POST['submit'])) {
			$dataPost = $_POST['data'];
			
			$id = $idMax + 1;
			$name = trim($dataPost['name']);
			$slug = to_slug($name);
			$price = trim($dataPost['price']);
			$colors = trim($dataPost['colors']);
			$sizes = trim($dataPost['sizes']);
			$qty = trim($dataPost['qty']);
			$brand_id = $dataPost['brand_id'];
			$product_category_id = $dataPost['product_category_id'];
			$description = trim($dataPost['description']);
			$content = trim($dataPost['content']);
			$rate = trim($dataPost['rate']);
			$is_new = (isset($dataPost['is_new']) ? $dataPost['is_new'] : 0);
			$is_promotion = (isset($dataPost['is_promotion']) ? $dataPost['is_promotion'] : 0);
			$is_featured = (isset($dataPost['is_featured']) ? $dataPost['is_featured'] : 0);
			$is_sale = (isset($dataPost['is_sale']) ? $dataPost['is_sale'] : 0);
			$created_at = trim($dataPost['created_at']);
			$updated_at = trim($dataPost['updated_at']);
			$meta_title = trim($dataPost['meta_title']);
			$meta_keyword = trim($dataPost['meta_keyword']);
			$meta_description = trim($dataPost['meta_description']);
			$status = $dataPost['status'];
			$sku = create_sku($product_category_id,$brand_id,$id);

			if ($name == '') {
				$_SESSION['flash_msg'] = 'Tên sản phẩm không được bỏ trống!';
			}

			$where = " AND products.sku='$sku'";
			$testsku = $this->productModel->getProduct($where);
			if (count($testsku) > 0) {
				$_SESSION['flash_msg'] = "Sku = '$sku' đã tồn tại!";
			}

			$where = " AND products.slug='$slug'";
			$testSlug = $this->productModel->getProduct($where);
			if (count($testSlug) > 0) {
				$_SESSION['flash_msg'] = "Slug = '$slug' đã tồn tại!";
			}

			if (!isset($_SESSION['flash_msg'])) {
				$dataStr = "'$id', '$sku', '$name', '$slug', '$price', '$colors', '$sizes', '$qty', '$brand_id', '$product_category_id', '$description', '$content', '$rate', '$is_new', '$is_promotion', '$is_featured', '$is_sale', '$created_at', '$updated_at', '$meta_title', '$meta_keyword', '$meta_description', '$status'";
				$result = $this->productModel->addProduct($dataStr);
				if (is_null($result)) {
					$_SESSION['flash_msg'] = 'Thêm Thành Công!';
					header('location: index.php?c=product');
					exit;
				}
			}
		}
		return view('products.create', $data);
	}

	public function update()
	{
		if (isset($_GET['id']) && $_GET['id'] != '') {
			$id = (int)$_GET['id'];
			$where = "AND products.id=$id";
			$product = $this->productModel->getProduct($where);
			if (is_null($product)) {
				header('location: index.php?c=product');
				exit;
			}	
			$brands = $this->productModel->getBrands();
			$categories = $this->productModel->getCategories();
			$data = [
				'product' => $product,
				'brands' => $brands,
				'categories' => $categories,
			];			
		}

		if (isset($_POST['submit'])) {
			$dataPost = $_POST['data'];
			
			$id2 = trim($dataPost['id']);
			$name = trim($dataPost['name']);
			$slug = to_slug($name);
			$price = trim($dataPost['price']);
			$colors = trim($dataPost['colors']);
			$sizes = trim($dataPost['sizes']);
			$qty = trim($dataPost['qty']);
			$brand_id = $dataPost['brand_id'];
			$product_category_id = $dataPost['product_category_id'];
			$description = trim($dataPost['description']);
			$content = trim($dataPost['content']);
			$rate = trim($dataPost['rate']);
			$is_new = (isset($dataPost['is_new']) ? $dataPost['is_new'] : 0);
			$is_promotion = (isset($dataPost['is_promotion']) ? $dataPost['is_promotion'] : 0);
			$is_featured = (isset($dataPost['is_featured']) ? $dataPost['is_featured'] : 0);
			$is_sale = (isset($dataPost['is_sale']) ? $dataPost['is_sale'] : 0);
			$created_at = trim($dataPost['created_at']);
			$updated_at = trim($dataPost['updated_at']);
			$meta_title = trim($dataPost['meta_title']);
			$meta_keyword = trim($dataPost['meta_keyword']);
			$meta_description = trim($dataPost['meta_description']);
			$status = $dataPost['status'];
			$sku = create_sku($product_category_id,$brand_id,$id);

			if ($name == '') {
				$_SESSION['flash_msg'] = 'Tên sản phẩm không được bỏ trống!';
			}

			$where = " AND products.id='$id' AND products.id<>'$id2'";
			$testid = $this->productModel->getProduct($where);
			if (count($testid) > 0) {
				$_SESSION['flash_msg'] = "id = '$id2' đã tồn tại!";
			}

			$where = " AND products.sku='$sku' AND products.id<>'$id2'";
			$testsku = $this->productModel->getProduct($where);
			if (count($testsku) > 0) {
				$_SESSION['flash_msg'] = "Sku = '$sku' đã tồn tại!";
			}

			$where = " AND products.slug='$slug' AND products.id<>'$id2'";
			$testSlug = $this->productModel->getProduct($where);
			if (count($testSlug) > 0) {
				$_SESSION['flash_msg'] = "Slug = '$slug' đã tồn tại!";
			}

			if (!isset($_SESSION['flash_msg'])) {
				$set = "sku='$sku', name='$name', slug='$slug', price=$price, colors='$colors', sizes='$sizes', qty=$qty, brand_id=$brand_id, product_category_id=$product_category_id, description='$description', content='$content', rate=$rate, is_new=$is_new, is_promotion=$is_promotion, is_featured=$is_featured, is_sale=$is_sale, created_at='$created_at', updated_at='$updated_at', meta_title='$meta_title', meta_keyword='$meta_keyword', meta_description='$meta_description', status=$status";
				$result = $this->productModel->editProduct($set, $id);
				if (is_null($result)) {
					$_SESSION['flash_msg'] = 'Cập nhật thành công!';
					header('location: index.php?c=product');
					exit;
				}
			}
		}
		return view('products.update', $data);		
	}

	public function delete()
	{
		if (isset($_GET['id']) && $_GET['id'] != '') {
			$id = (int)$_GET['id'];
			$where = "AND products.id=$id";
			$product = $this->productModel->getProduct($where);
			if (!is_null($product)) {
				$result = $this->productModel->delProduct($id);
			}
		}
		header('location: index.php?c=product');
		exit;
	}

}