<?php 
date_default_timezone_set('Asia/Ho_Chi_Minh');

if (isset($_POST['data'])) {
	$post = $_POST['data'];
}
?>

<?php if(isset($brand)): ?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<h1>Chỉnh sửa Thương Hiệu</h1>
		</div>
	</div>
	<div class="form-create clear">
		<div class="container">
			<form action="" method="post">
				<table>
					<tr>
						<td>ID</td>
						<td>
							<input type="text" name="data[id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php if(isset($post)) {echo $post['id'];} else {echo $brand['id'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Tên thương hiệu (*)</td>
						<td>
							<input type="text" onkeyup="ToSlug(this.value)" onchange="ToSlug(this.value)" id='title' name="data[name]" placeholder="Tên thương hiệu" class="honnua" autofocus value="<?php if(isset($post)) {echo $post['name'];} else {echo $brand['name'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Slug</td>
						<td>
							<input type="text" id='slug' name="data[slug]" placeholder="slug (auto)" class="honnua" readonly value="<?php if(isset($post)) {echo $post['slug'];} else {echo $brand['slug'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Logo</td>
						<td>
							<input name="data[logo]" placeholder="logo" class="honnua" value="<?php if(isset($post)) {echo $post['logo'];} else {echo $brand['logo'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Hình ảnh</td>
						<td>
							<input name="data[image]" placeholder="image" class="honnua" value="<?php if(isset($post)) {echo $post['image'];} else {echo $brand['image'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Mô tả</td>
						<td>
							<textarea name="data[description]" placeholder="mô tả" class="text"><?php if(isset($post)) {echo $post['description'];} else {echo $brand['description'];} ?></textarea>
						</td>
					</tr>
					<tr>
						<td>meta title</td>
						<td>
							<input type="text" name="data[meta_title]" placeholder="meta_title" value="<?php if(isset($post)) {echo $post['meta_title'];} else {echo $brand['meta_title'];} ?>">
						</td>
					</tr>
					<tr>
						<td>meta keyword</td>
						<td>
							<input type="text" name="data[meta_keyword]" placeholder="meta_keyword" value="<?php if(isset($post)) {echo $post['meta_keyword'];} else {echo $brand['meta_keyword'];} ?>">
						</td>
					</tr>
					<tr>
						<td>meta description</td>
						<td>
							<input type="text" name="data[meta_description]" placeholder="meta_description" value="<?php if(isset($post)) {echo $post['meta_description'];} else {echo $brand['meta_description'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Trạng thái</td>
						<td>
							<select name="data[status]">
								<option value="1" <?php if ((isset($post) && $post['status'] == '1') || (!isset($post) && $brand['status'] == '1')) {echo 'selected';} ?>>Kích hoạt</option>
								<option value="0" <?php if ((isset($post) && $post['status'] == '0') || (!isset($post) && $brand['status'] == '0')) {echo 'selected';} ?>>Vô hiệu</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dt-center">
							<input type="submit" name="submit" value="Cập Nhật" class="submit">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</section>
<?php endif; ?>