<?php 
if (isset($_POST['data'])) {
	$post = $_POST['data'];
}
?>

<?php if(isset($usergroup)): ?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<h1>Chỉnh sửa Nhóm thành viên</h1>
		</div>
	</div>
	<div class="form-create clear">
		<div class="container">
			<form action="" method="post">
				<table>
					<tr>
						<td>ID</td>
						<td>
							<input type="text" name="data[id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php if(isset($post['id'])) {echo $post['id'];} else {echo $usergroup['id'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Tên nhóm (*)</td>
						<td>
							<input type="text" onkeyup="ToSlug(this.value)" onchange="ToSlug(this.value)" id='title' name="data[name]" placeholder="Tên nhóm" class="honnua" autofocus value="<?php if(isset($post['name'])) {echo $post['name'];} else {echo $usergroup['name'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Kiếu</td>
						<td>
							<select name="data[type]">
								<option value="rate" <?php if ((isset($post['type']) && $post['type'] == 'rate') || (!isset($post) && $usergroup['type'] == 'rate')) {echo 'selected';} ?>>Phần trăm</option>
								<option value="price" <?php if ((isset($post['type']) && $post['type'] == 'price') || (!isset($post) && $usergroup['type'] == 'price')) {echo 'selected';} ?>>Số tiền</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Giá trị</td>
						<td>
							<input type="number" name="data[amount]" placeholder="% / vnđ" class="nuagia" value="<?php if(isset($post['amount'])) {echo $post['amount'];} else {echo $usergroup['amount'];} ?>">
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dt-center">
							<input type="submit" name="submit" value="Cập Nhật" class="submit">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</section>
<?php endif; ?>