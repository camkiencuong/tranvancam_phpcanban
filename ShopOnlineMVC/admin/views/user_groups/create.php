<?php
if (isset($_POST['data'])) {
	$post = $_POST['data'];
}
?>

<?php if (isset($idMax)): ?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<h1>Thêm Nhóm Thành Viên</h1>
		</div>
	</div>
	<div class="form-create clear">
		<div class="container">
			<form action="" method="post">
				<table>
					<tr>
						<td>ID</td>
						<td>
							<input type="text" name="data[id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php echo $idMax + 1; ?>">
						</td>
					</tr>
					<tr>
						<td>Tên nhóm (*)</td>
						<td>
							<input type="text" onkeyup="ToSlug(this.value)" onchange="ToSlug(this.value)" id='title' name="data[name]" placeholder="Tên nhóm" class="honnua" autofocus value="<?php if(isset($post) && $post['name'] != '') {echo $post['name'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Kiểu</td>
						<td>
							<select name="data[type]">
								<option value="rate">Phần trăm</option>
								<option value="price">Số tiền</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Giá trị</td>
						<td>
							<input type="number" name="data[amount]" placeholder="(%) / (vnđ)" class="nuagia" value="0">
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dt-center">
							<input type="submit" name="submit" value="Thêm Mới" class="submit">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</section>
<?php endif; ?>