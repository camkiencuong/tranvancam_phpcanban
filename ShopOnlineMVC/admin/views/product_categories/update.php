<?php 
if (isset($_POST['data'])) {
	$post = $_POST['data'];
}
?>
<?php if (isset($category)): ?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<h1>Chỉnh sửa Danh mục Sản phẩm</h1>
		</div>
	</div>
	<div class="form-create clear">
		<div class="container">
			<form action="" method="post">
				<table>
					<tr>
						<td>ID</td>
						<td>
							<input type="text" name="data[id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php if(isset($post)) {echo $post['id'];} else {echo $category['id'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Tên danh mục (*)</td>
						<td>
							<input type="text" onkeyup="ToSlug(this.value)" onchange="ToSlug(this.value)" id='title' name="data[name]" placeholder="Tên danh mục" class="honnua" autofocus value="<?php if(isset($post)) {echo $post['name'];} else {echo $category['name'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Slug</td>
						<td>
							<input type="text" id='slug' name="data[slug]" placeholder="slug (auto)" class="honnua" readonly value="<?php if(isset($post)) {echo $post['slug'];} else {echo $category['slug'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Hình ảnh</td>
						<td>
							<input name="data[image]" placeholder="image" class="honnua" value="<?php if(isset($post)) {echo $post['image'];} else {echo $category['image'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Mô tả</td>
						<td>
							<textarea name="data[description]" placeholder="mô tả" class="textbe"><?php if(isset($post)) {echo $post['description'];} else {echo $category['description'];} ?></textarea>
						</td>
					</tr>
					<tr>
						<td>Danh mục cha</td>
						<td>
							<select name="data[parent_id]">
								<option value="0">không</option>
							<?php foreach ($categories as $cate): ?>
								<option value="<?php echo $cate['id']; ?>" <?php if ((isset($post) && $post['parent_id'] == $cate['id']) || (!isset($post) && $category['parent_id'] == $cate['id'])) {echo 'selected';} ?>><?php echo $cate['name']; ?></option>
							<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>meta title</td>
						<td>
							<input type="text" name="data[meta_title]" placeholder="meta_title" value="<?php if(isset($post)) {echo $post['meta_title'];} else {echo $category['meta_title'];} ?>">
						</td>
					</tr>
					<tr>
						<td>meta keyword</td>
						<td>
							<input type="text" name="data[meta_keyword]" placeholder="meta_keyword" value="<?php if(isset($post)) {echo $post['meta_keyword'];} else {echo $category['meta_keyword'];} ?>">
						</td>
					</tr>
					<tr>
						<td>meta description</td>
						<td>
							<input type="text" name="data[meta_description]" placeholder="meta_description" value="<?php if(isset($post)) {echo $post['meta_description'];} else {echo $category['meta_description'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Trạng thái</td>
						<td>
							<select name="data[status]">
								<option value="1" <?php if ((isset($post) && $post['status'] == '1') || (!isset($post) && $category['status'] == '1')) {echo 'selected';} ?>>Kích hoạt</option>
								<option value="0" <?php if ((isset($post) && $post['status'] == '0') || (!isset($post) && $category['status'] == '0')) {echo 'selected';} ?>>Vô hiệu</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dt-center">
							<input type="submit" name="submit" value="Thêm Mới" class="submit">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</section>
<?php endif; ?>