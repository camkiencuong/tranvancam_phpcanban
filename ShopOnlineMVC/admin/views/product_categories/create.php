<?php 
if (isset($_POST['data'])) {
	$post = $_POST['data'];
}
?>
<?php if (isset($idMax)): ?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<h1>Thêm Danh Mục Sản Phẩm</h1>
		</div>
	</div>
	<div class="form-create clear">
		<div class="container">
			<form action="" method="post">
				<table>
					<tr>
						<td>ID</td>
						<td>
							<input type="text" name="data[id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php echo $idMax + 1; ?>">
						</td>
					</tr>
					<tr>
						<td>Tên danh mục (*)</td>
						<td>
							<input type="text" onkeyup="ToSlug(this.value)" onchange="ToSlug(this.value)" id='title' name="data[name]" placeholder="Tên danh mục" class="honnua" autofocus value="<?php if(isset($post) && $post['name'] != '') {echo $post['name'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Slug</td>
						<td>
							<input type="text" id='slug' name="data[slug]" placeholder="slug (auto)" class="honnua" readonly>
						</td>
					</tr>
					<tr>
						<td>Hình ảnh</td>
						<td>
							<input name="data[image]" placeholder="image" class="honnua">
						</td>
					</tr>
					<tr>
						<td>Mô tả</td>
						<td>
							<textarea name="data[description]" placeholder="mô tả" class="textbe"></textarea>
						</td>
					</tr>
					<tr>
						<td>Danh mục cha</td>
						<td>
							<select name="data[parent_id]">
								<option value="0">không</option>
							<?php foreach ($categories as $category): ?>
								<option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
							<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>meta title</td>
						<td>
							<input type="text" name="data[meta_title]" placeholder="meta_title">
						</td>
					</tr>
					<tr>
						<td>meta keyword</td>
						<td>
							<input type="text" name="data[meta_keyword]" placeholder="meta_keyword">
						</td>
					</tr>
					<tr>
						<td>meta description</td>
						<td>
							<input type="text" name="data[meta_description]" placeholder="meta_description">
						</td>
					</tr>
					<tr>
						<td>Trạng thái</td>
						<td>
							<select name="data[status]">
								<option value="1">Kích hoạt</option>
								<option value="0">Vô hiệu</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dt-center">
							<input type="submit" name="submit" value="Thêm Mới" class="submit">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</section>
<?php endif; ?>