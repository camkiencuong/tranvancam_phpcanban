
<?php if (isset($products)): ?>
<section class="show clear">
	<div class="namepage clear">
		<div class="container">
			<h1>Quản lý Sản phẩm</h1>
		</div>
	</div>
	<div class="menubar clear">
		<div class="container">
			<ul>
				<li><a href="index.php?c=product&m=create">Thêm sản phẩm</a></li>
			</ul>
		</div>
	</div>
	<div class="tuychinh clear">
		<div class="container">
			<div class="left">
				<span><big>Tổng: <?php echo $totalRecord; ?></big></span>
				<span class="number">
						Hiển thị: 
						<select onchange="window.location.href=this.value">
							<option value="<?php echo 'index.php'.$queryLimit.'limit=10';?>" <?php if ($limit=='10') {echo 'selected';}?>>10</option>
							<option value="<?php echo 'index.php'.$queryLimit.'limit=20';?>" <?php if ($limit=='20') {echo 'selected';}?>>20</option>
							<option value="<?php echo 'index.php'.$queryLimit.'limit=50';?>" <?php if ($limit=='50') {echo 'selected';}?>>50</option>
							<option value="<?php echo 'index.php'.$queryLimit.'limit=100';?>" <?php if ($limit=='100') {echo 'selected';}?>>100</option>
						</select>
				</span>
			</div>
			<div class="right">
				<span class="orderby">
					Sắp xếp: 
					<select onchange="window.location.href=this.value">
						<option value="<?php echo 'index.php'.$queryOrderby.'col=id&by=ASC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'ASC') {echo 'selected';} ?> >id Tăng</option>
						<option value="<?php echo 'index.php'.$queryOrderby.'col=id&by=DESC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'DESC') {echo 'selected';} ?> >id Giảm</option>
						<option value="<?php echo 'index.php'.$queryOrderby.'col=name&by=ASC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'ASC') {echo 'selected';} ?> >name Tăng</option>
						<option value="<?php echo 'index.php'.$queryOrderby.'col=name&by=DESC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'DESC') {echo 'selected';} ?> >name Giảm</option>
					</select>
				</span>
				<span class="finding">
					<form action="" method="post" accept-charset="utf-8">
						<input id="keyword" type="text" name="keyword" class="form" placeholder="Tìm kiếm theo tên" value="<?php if(isset($_GET['keyword'])) {echo $_GET['keyword'];}?>">
						<input onclick="return testFind()" type="submit" name="finding" value="Tìm kiếm">
					</form>
				</span>
			</div>
		</div>
	</div>
<?php if (count($products) > 0): ?>
	<div class="tb-border clear">
		<div class="container">
			<form action="" method="post">
				<table border="1">
					<tr>
						<th colspan="2">Thao tác</th>
						<th>id</th>
						<th>sku</th>
						<th>Tên sản phẩm</th>
						<th>slug</th>
						<th>Giá</th>
						<th>màu sắc</th>
						<th>sizes</th>
						<th>Số lượng</th>
						<th>Thương hiệu</th>
						<th>Danh mục</th>
						<th>Mô tả</th>
						<th>Chi tiết</th>
						<th>Sao</th>
						<th>Mới</th>
						<th>Khuyến mãi</th>
						<th>Nổi bật</th>
						<th>Bán chạy</th>
						<th>Ngày tạo</th>
						<th>Ngày sửa</th>
						<th>Trạng thái</th>
					</tr>
					<?php foreach ($products as $product): ?>
						<tr>
							<td class="options"><a href="<?php echo 'index.php?c=product&m=update&id=' . $product['id'];?>">sửa</a></td>
							<td class="options"><a onclick="return confirm('Bạn chắc chắn muốn xóa không?');" href="<?php echo 'index.php?c=product&m=delete&id=' . $product['id'];?>">xóa</a></td>
							<td class='data-show'><?php echo $product['id']; ?></td>
							<td class='data-show'><?php echo $product['sku']; ?></td>
							<td class='data-show'><?php echo $product['name']; ?></td>
							<td class='data-show'><?php echo $product['slug']; ?></td>
							<td class='data-show'><?php echo $product['price']; ?></td>
							<td class='data-show'><?php echo $product['colors']; ?></td>
							<td class='data-show'><?php echo $product['sizes']; ?></td>
							<td class='data-show'><?php echo $product['qty']; ?></td>
							<td class='data-show'><?php echo $product['brand_name']; ?></td>
							<td class='data-show'><?php echo $product['cate_name']; ?></td>
							<td class='data-show'><?php echo $product['description']; ?></td>
							<td class='data-show'><?php echo $product['content']; ?></td>
							<td class='data-show'><?php echo $product['rate']; ?></td>
							<td class='data-show'><?php echo isYes($product['is_new']); ?></td>
							<td class='data-show'><?php echo isYes($product['is_promotion']); ?></td>
							<td class='data-show'><?php echo isYes($product['is_featured']); ?></td>
							<td class='data-show'><?php echo isYes($product['is_sale']); ?></td>
							<td class='data-show'><?php echo $product['created_at']; ?></td>
							<td class='data-show'><?php echo $product['updated_at']; ?></td>
							<td class='data-show'><?php echo status($product['status']); ?></td>
						</tr>
					<?php endforeach; ?>
				</table>
			</form>
		</div>
	</div>
	<div class="paging clear">
		<div class="container">
			<?php echo paging($queryString, $totalRecord, $page, $limit);?>
		</div>
	</div>
<?php else: ?>
	<div class="container dt-center clear">Không có bản ghi!</div>
<?php endif; ?>
</div>
</section>
<?php endif; ?>
