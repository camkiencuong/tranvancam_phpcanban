<?php 
date_default_timezone_set('Asia/Ho_Chi_Minh');

if (isset($_POST['data'])) {
	$post = $_POST['data'];
}
?>
<?php if (isset($product)): ?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<h1>Thêm Sản phẩm</h1>
		</div>
	</div>
	<div class="form-create clear">
		<div class="container">
			<form action="" method="post">
				<table>
					<tr>
						<td>id</td>
						<td>
							<input type="text" name="data[id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php if (isset($post['id'])) {echo $post['id'];} else {echo $product['id'];} ?>">
						</td>
					</tr>
					<tr>
						<td>sku</td>
						<td>
							<input type="text" id='sku<?php echo $i; ?>' name="data[sku]" placeholder="sku (auto)" readonly class="gianua" value="<?php if (isset($post['sku'])) {echo $post['sku'];} else {echo $product['sku'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Tên sản phẩm (*)</td>
						<td>
							<input type="text" onkeyup="ToSlug(this.value)" onchange="ToSlug(this.value)" id='title' name="data[name]" placeholder="tên sản phẩm" class="honnua" autofocus value="<?php if (isset($post['name'])) {echo $post['name'];} else {echo $product['name'];} ?>">
						</td>
					</tr>
					<tr>
						<td>slug</td>
						<td>
							<input type="text" id='slug' name="data[slug]" placeholder="slug (auto)" class="honnua" readonly value="<?php if (isset($post['slug'])) {echo $post['slug'];} else {echo $product['slug'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Giá</td>
						<td>
							<input type="number" name="data[price]" placeholder="giá cả (đ)" class='nuagia' value="<?php if (isset($post['price'])) {echo $post['price'];} else {echo $product['price'];} ?>">
							(vnđ)
						</td>
					</tr>
					<tr>
						<td>màu sắc</td>
						<td>
							<input type="text" name="data[colors]" placeholder="màu sắc" class='gianua' value="<?php if (isset($post['colors'])) {echo $post['colors'];} else {echo $product['colors'];} ?>">
						</td>
					</tr>
					<tr>
						<td>sizes</td>
						<td>
							<input type="text" name="data[sizes]" placeholder="L | M | XL | XXL" class='gianua' value="<?php if (isset($post['sizes'])) {echo $post['sizes'];} else {echo $product['sizes'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Tồn kho</td>
						<td>
							<input type="number" name="data[qty]" placeholder="số lượng" class='nuanon' value="<?php if (isset($post['qty'])) {echo $post['qty'];} else {echo $product['qty'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Thương hiệu</td>
						<td>
							<select name="data[brand_id]">
								<?php foreach ($brands as $brand): ?>
									<option value="<?php echo $brand['id']; ?>" <?php if ((isset($post['brand_id']) && $post['brand_id'] == $brand['id']) || (!isset($post) && $product['brand_id'] == $brand['id'])) {echo 'selected';} ?>><?php echo $brand['name']; ?></option>
								<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Danh mục</td>
						<td>
							<select name="data[product_category_id]">
								<?php foreach ($categories as $category): ?>
									<option value="<?php echo $category['id']; ?>" <?php if ((isset($post['product_category_id']) && $post['product_category_id'] == $category['id']) || (!isset($post) && $product['product_category_id'] == $category['id'])) {echo 'selected';} ?>><?php echo $category['name']; ?></option>
								<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Mô tả</td>
						<td>
							<textarea name="data[description]" placeholder="mô tả" class="text"><?php if (isset($post['description'])) {echo $post['description'];} else {echo $product['description'];} ?></textarea>
						</td>
					</tr>
					<tr>
						<td>Chi tiết</td>
						<td>
							<textarea name="data[content]" placeholder="chi tiết" class="text"><?php if (isset($post['content'])) {echo $post['content'];} else {echo $product['content'];} ?></textarea>
						</td>
					</tr>
					<tr>
						<td>Sao đánh giá</td>
						<td>
							<input type="number" name="data[rate]" placeholder="*sao*" class='motnua' readonly value="<?php if (isset($post['rate'])) {echo $post['rate'];} else {echo $product['rate'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Sản phẩm Mới</td>
						<td>
							<input type="checkbox" name="data[is_new]" class="checkbox" value="1" <?php if ((isset($post['is_new']) && $post['is_new'] == '1') || (!isset($post) && $product['is_new'] == '1')) {echo 'checked';} ?>>
						</td>
					</tr>
					<tr>
						<td>Sản phẩm Khuyến mãi</td>
						<td>
							<input type="checkbox" name="data[is_promotion]" class="checkbox" value="1" <?php if ((isset($post['is_promotion']) && $post['is_promotion'] == '1') || (!isset($post) && $product['is_promotion'] == '1')) {echo 'checked';} ?>>
						</td>
					</tr>
					<tr>
						<td>Sản phẩm Nổi bật</td>
						<td>
							<input type="checkbox" name="data[is_featured]" class="checkbox" value="1" <?php if ((isset($post['is_featured']) && $post['is_featured'] == '1') || (!isset($post) && $product['is_featured'] == '1')) {echo 'checked';} ?>>
						</td>
					</tr>
					<tr>
						<td>Sản phẩm Bán chạy</td>
						<td>
							<input type="checkbox" name="data[is_sale]" class="checkbox" value="1" <?php if ((isset($post['is_sale']) && $post['is_sale'] == '1') || (!isset($post) && $product['is_sale'] == '1')) {echo 'checked';} ?>>
						</td>
					</tr>
					<tr>
						<td>Ngày tạo</td>
						<td>
							<input type="date" name="data[created_at]" placeholder="yyyy-mm-dd" class="gianua" readonly value="<?php echo $product['created_at']; ?>">
						</td>
					</tr>
					<tr>
						<td>Ngày sửa</td>
						<td>
							<input type="date" name="data[updated_at]" placeholder="yyyy-mm-dd" class="gianua" readonly value="<?php echo date('Y-m-d'); ?>">
						</td>
					</tr>
					<tr>
						<td>meta title</td>
						<td>
							<input type="text" name="data[meta_title]" placeholder="meta_title" value="<?php if (isset($post['meta_title'])) {echo $post['meta_title'];} else {echo $product['meta_title'];} ?>">
						</td>
					</tr>
					<tr>
						<td>meta keyword</td>
						<td>
							<input type="text" name="data[meta_keyword]" placeholder="meta_keyword" value="<?php if (isset($post['meta_keyword'])) {echo $post['meta_keyword'];} else {echo $product['meta_keyword'];} ?>">
						</td>
					</tr>
					<tr>
						<td>meta description</td>
						<td>
							<input type="text" name="data[meta_description]" placeholder="meta_description" value="<?php if (isset($post['meta_description'])) {echo $post['meta_description'];} else {echo $product['meta_description'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Trạng thái</td>
						<td>
							<select name="data[status]">
								<option value="1" <?php if ((isset($post['status']) && $post['status'] == '1') || (!isset($post) && $product['status'] == '1')) {echo 'selected';} ?>>Kích hoạt</option>
								<option value="0" <?php if ((isset($post['status']) && $post['status'] == '0') || (!isset($post) && $product['status'] == '0')) {echo 'selected';} ?>>Vô hiệu</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dt-center">
							<input type="submit" name="submit" value="Cập Nhật" class="submit">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</section>
<?php endif; ?>