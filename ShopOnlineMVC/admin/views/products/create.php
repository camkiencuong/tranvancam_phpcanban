<?php 
date_default_timezone_set('Asia/Ho_Chi_Minh');

if (isset($_POST['data'])) {
	$post = $_POST['data'];
}
?>
<?php if (isset($idMax)): ?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<h1>Thêm Sản phẩm</h1>
		</div>
	</div>
	<div class="form-create clear">
		<div class="container">
			<form action="" method="post">
				<table>
					<tr>
						<td>id</td>
						<td>
							<input type="text" name="data[id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php echo $idMax + 1; ?>">
						</td>
					</tr>
					<tr>
						<td>sku</td>
						<td>
							<input type="text" id='sku<?php echo $i; ?>' name="data[sku]" placeholder="sku (auto)" readonly class="gianua">
						</td>
					</tr>
					<tr>
						<td>Tên sản phẩm (*)</td>
						<td>
							<input type="text" onkeyup="ToSlug(this.value)" onchange="ToSlug(this.value)" id='title' name="data[name]" placeholder="tên sản phẩm" class="honnua" autofocus value="<?php if(isset($post) && $post['name'] != '') {echo $post['name'];} ?>">
						</td>
					</tr>
					<tr>
						<td>slug</td>
						<td>
							<input type="text" id='slug' name="data[slug]" placeholder="slug (auto)" class="honnua" readonly>
						</td>
					</tr>
					<tr>
						<td>Giá</td>
						<td>
							<input type="number" name="data[price]" placeholder="giá cả (đ)" class='nuagia'>
							(vnđ)
						</td>
					</tr>
					<tr>
						<td>màu sắc</td>
						<td>
							<input type="text" name="data[colors]" placeholder="màu sắc" class='gianua'>
						</td>
					</tr>
					<tr>
						<td>sizes</td>
						<td>
							<input type="text" name="data[sizes]" placeholder="L | M | XL | XXL" class='gianua'>
						</td>
					</tr>
					<tr>
						<td>Số lượng</td>
						<td>
							<input type="number" name="data[qty]" placeholder="số lượng" class='nuanon'>
						</td>
					</tr>
					<tr>
						<td>Thương hiệu</td>
						<td>
							<select name="data[brand_id]">
								<?php foreach ($brands as $brand): ?>
									<option value="<?php echo $brand['id']; ?>"><?php echo $brand['name']; ?></option>
								<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Danh mục</td>
						<td>
							<select name="data[product_category_id]">
								<?php foreach ($categories as $category): ?>
									<option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
								<?php endforeach; ?>
							</select>
						</td>
					</tr>
					<tr>
						<td>Mô tả</td>
						<td>
							<textarea name="data[description]" placeholder="mô tả" class="textbe"></textarea>
						</td>
					</tr>
					<tr>
						<td>Chi tiết</td>
						<td>
							<textarea name="data[content]" placeholder="chi tiết" class="textbe"></textarea>
						</td>
					</tr>
					<tr>
						<td>Sao đánh giá</td>
						<td>
							<input type="number" name="data[rate]" placeholder="*sao*" class='motnua' value="0" readonly>
						</td>
					</tr>
					<tr>
						<td>Sản phẩm Mới</td>
						<td>
							<input type="checkbox" name="data[is_new]" class="checkbox" value="1">
						</td>
					</tr>
					<tr>
						<td>Sản phẩm Khuyến mãi</td>
						<td>
							<input type="checkbox" name="data[is_promotion]" class="checkbox" value="1">
						</td>
					</tr>
					<tr>
						<td>Sản phẩm Nổi bật</td>
						<td>
							<input type="checkbox" name="data[is_featured]" class="checkbox" value="1">
						</td>
					</tr>
					<tr>
						<td>Sản phẩm Bán chạy</td>
						<td>
							<input type="checkbox" name="data[is_sale]" class="checkbox" value="1">
						</td>
					</tr>
					<tr>
						<td>Ngày tạo</td>
						<td>
							<input type="date" name="data[created_at]" placeholder="yyyy-mm-dd" class="gianua" readonly value="<?php echo date('Y-m-d'); ?>">
						</td>
					</tr>
					<tr>
						<td>Ngày sửa</td>
						<td>
							<input type="date" name="data[updated_at]" placeholder="yyyy-mm-dd" class="gianua" readonly
						</td>
					</tr>
					<tr>
						<td>meta title</td>
						<td>
							<input type="text" name="data[meta_title]" placeholder="meta_title">
						</td>
					</tr>
					<tr>
						<td>meta keyword</td>
						<td>
							<input type="text" name="data[meta_keyword]" placeholder="meta_keyword">
						</td>
					</tr>
					<tr>
						<td>meta description</td>
						<td>
							<input type="text" name="data[meta_description]" placeholder="meta_description">
						</td>
					</tr>
					<tr>
						<td>Trạng thái</td>
						<td>
							<select name="data[status]">
								<option value="1">Kích hoạt</option>
								<option value="0">Vô hiệu</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dt-center">
							<input type="submit" name="submit" value="Thêm Vào" class="submit">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</section>
<?php endif; ?>