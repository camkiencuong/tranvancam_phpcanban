<?php
$menu= isset($_GET['c']) ? $_GET['c'] : 'index';
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Shop Online</title>
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1">
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<body>
	<header class="page-top <?php if (isset($_GET['view'])) echo 'blur'; ?>">
		<div class="menu clear">
			<div class="container">
				<ul>
					<li><a class="<?php if($menu=='index') echo 'active';?>" href="index.php">Tổng quan</a></li>
					<li><a class="<?php if($menu=='productcategory') echo 'active';?>" href="index.php?c=productcategory">Danh mục SP</a></li>
					<li><a class="<?php if($menu=='product') echo 'active';?>" href="index.php?c=product">Sản phẩm</a></li>
					<li><a class="<?php if($menu=='productimage') echo 'active';?>" href="index.php?c=productimage">Hình ảnh SP</a></li>
					<li><a class="<?php if($menu=='productrelate') echo 'active';?>" href="index.php?c=productrelate">SP liên quan</a></li>
					<li><a class="<?php if($menu=='brand') echo 'active';?>" href="index.php?c=brand">Thương hiệu</a></li>
					<li><a class="<?php if($menu=='usergroup') echo 'active';?>" href="index.php?c=usergroup">Nhóm Thành viên</a></li>
					<li><a class="<?php if($menu=='user') echo 'active';?>" href="index.php?c=user">Thành viên</a></li>
					<li><a class="<?php if($menu=='admin') echo 'active';?>" href="index.php?c=admin">Quản trị</a></li>
					<li><a class="<?php if($menu=='order') echo 'active';?>" href="index.php?c=order">Đơn hàng</a></li>
					<li><a class="<?php if($menu=='coupon') echo 'active';?>" href="index.php?c=coupon">Khuyến mãi</a></li>
					<li><a class="<?php if($menu=='review') echo 'active';?>" href="index.php?c=review">Đánh giá</a></li>
					<li><a class="<?php if($menu=='postcategory') echo 'active';?>" href="index.php?c=postcategory">Danh mục tin</a></li>
					<li><a class="<?php if($menu=='post') echo 'active';?>" href="index.php?c=post">Tin tức</a></li>
					<li><a class="<?php if($menu=='comment') echo 'active';?>" href="index.php?c=comment">Bình luận</a></li>
					<li><a class="<?php if($menu=='banner') echo 'active';?>" href="index.php?c=banner">Quảng cáo</a></li>
					<li><a class="<?php if($menu=='page') echo 'active';?>" href="index.php?c=page">Trang tĩnh</a></li>
					<li><a class="<?php if($menu=='province') echo 'active';?>" href="index.php?c=province">Tỉnh/Thành phố</a></li>
					<li><a class="<?php if($menu=='district') echo 'active';?>" href="index.php?c=district">Quận/Huyện</a></li>
					<li><a class="<?php if($menu=='') echo 'active';?>" href="#"></a></li>
				</ul>
			</div>
		</div>
	</header>

<div id="content">
	<div class="message" style="padding-left:5%; color:red;">
		<?php if(isset($_SESSION['flash_msg'])) :?>
			<span><?php echo $_SESSION['flash_msg']; unset($_SESSION['flash_msg']);?></span>
		<?php endif;?>
	</div>
	
<?php require $content; ?>

</div>

<footer>
	<div class="infos">
		<div class="container">
			<address>("-")</address>
		</div>
	</div>
</footer>

<script src="js/script.js"></script>
</body>
</html>