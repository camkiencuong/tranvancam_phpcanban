
<?php if (isset($reviews)): ?>
<section class="show clear">
	<div class="namepage clear">
		<div class="container">
			<h1>Quản lý Đánh giá</h1>
		</div>
	</div>
	<div class="tuychinh clear">
		<div class="container">
			<div class="left">
				<span><big>Tổng: <?php echo $totalRecord; ?></big></span>
				<span class="number">
						Hiển thị: 
						<select onchange="window.location.href=this.value">
							<option value="<?php echo 'index.php'.$queryLimit.'limit=10';?>" <?php if ($limit=='10') {echo 'selected';}?>>10</option>
							<option value="<?php echo 'index.php'.$queryLimit.'limit=20';?>" <?php if ($limit=='20') {echo 'selected';}?>>20</option>
							<option value="<?php echo 'index.php'.$queryLimit.'limit=50';?>" <?php if ($limit=='50') {echo 'selected';}?>>50</option>
							<option value="<?php echo 'index.php'.$queryLimit.'limit=100';?>" <?php if ($limit=='100') {echo 'selected';}?>>100</option>
						</select>
				</span>
			</div>
			<div class="right">
				<span class="orderby">
					Sắp xếp: 
					<select onchange="window.location.href=this.value">
						<option value="<?php echo 'index.php'.$queryOrderby.'col=id&by=ASC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'ASC') {echo 'selected';} ?> >id Tăng</option>
						<option value="<?php echo 'index.php'.$queryOrderby.'col=id&by=DESC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'DESC') {echo 'selected';} ?> >id Giảm</option>
						<option value="<?php echo 'index.php'.$queryOrderby.'col=name&by=ASC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'ASC') {echo 'selected';} ?> >name Tăng</option>
						<option value="<?php echo 'index.php'.$queryOrderby.'col=name&by=DESC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'DESC') {echo 'selected';} ?> >name Giảm</option>
					</select>
				</span>
				<span class="finding">
					<form action="" method="post" accept-charset="utf-8">
						<input id="keyword" type="text" name="keyword" class="form" placeholder="Tìm kiếm nội dung" value="<?php if(isset($_GET['keyword'])) {echo $_GET['keyword'];}?>">
						<input onclick="return testFind()" type="submit" name="finding" value="Tìm kiếm">
					</form>
				</span>
			</div>
		</div>
	</div>
<?php if (count($reviews) > 0): ?>
	<div class="tb-border clear">
		<div class="container">
			<form action="" method="post">
				<table border="1">
					<tr>
						<th colspan="2">Thao tác</th>
						<th>id</th>
						<th>Sản phẩm</th>
						<th>Khách hàng</th>
						<th>Sao</th>
						<th>Nội dung</th>
						<th>Ngày tạo</th>
						<th>Ngày sửa</th>
						<th>Trạng thái</th>
					</tr>
					<?php foreach ($reviews as $review): ?>
						<tr>
							<td class="options"><a href="<?php echo 'index.php?c=review&m=update&id=' . $review['id'];?>">sửa</a></td>
							<td class="options"><a onclick="return confirm('Bạn chắc chắn muốn xóa không?');" href="<?php echo 'index.php?c=review&m=delete&id=' . $review['id'];?>">xóa</a></td>
							<td class='data-show'><?php echo $review['id']; ?></td>
							<td class='data-show'><?php echo $review['product_name']; ?></td>
							<td class='data-show'><?php echo $review['user_name']; ?></td>
							<td class='data-show'><?php echo $review['rate']; ?></td>
							<td class='data-show'><?php echo $review['content']; ?></td>
							<td class='data-show'><?php echo $review['created_at']; ?></td>
							<td class='data-show'><?php echo $review['update_at']; ?></td>
							<td class='data-show'><?php echo status($review['status']); ?></td>
						</tr>
					<?php endforeach; ?>
				</table>
			</form>
		</div>
	</div>
	<div class="paging clear">
		<div class="container">
			<?php echo paging($queryString, $totalRecord, $page, $limit);?>
		</div>
	</div>
<?php else: ?>
	<div class="container dt-center clear">Không có bản ghi!</div>
<?php endif; ?>
</div>
</section>
<?php endif; ?>
