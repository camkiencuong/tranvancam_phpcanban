<?php 
if (isset($_POST['data'])) {
	$post = $_POST['data'];
}
?>

<?php if(isset($review)): ?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<h1>Chỉnh sửa Đánh giá</h1>
		</div>
	</div>
	<div class="form-create clear">
		<div class="container">
			<form action="" method="post">
				<table>
					<tr>
						<td>ID</td>
						<td>
							<input type="text" name="data[id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php if(isset($post['id'])) {echo $post['id'];} else {echo $review['id'];} ?>">
						</td>
					</tr>
					<tr>
						<td>Tên sản phẩm</td>
						<td>
							<input type="text" name="data[product_name]" placeholder="tên sản phẩm" class="nuanua" readonly value="<?php echo $review['product_name']; ?>">
						</td>
					</tr>
					<tr>
						<td>Khách hàng</td>
						<td>
							<input type="text" name="data[user_name]" placeholder="tên khách hàng" readonly value="<?php echo $review['user_name']; ?>">
						</td>
					</tr>
					<tr>
						<td>Nội dung</td>
						<td>
							<textarea type="text" name="data[content]" placeholder="nội dung" class="textbe"><?php if(isset($post['content'])) {echo $post['content'];} else {echo $review['content'];} ?></textarea>
						</td>
					</tr>
					<tr>
						<td>Trạng thái</td>
						<td>
							<select name="data[status]">
								<option value="1" <?php if ((isset($post['status']) && $post['status'] == '1') || (!isset($post) && $review['status'] == '1')) {echo 'selected';} ?>>Kích hoạt</option>
								<option value="0" <?php if ((isset($post['status']) && $post['status'] == '0') || (!isset($post) && $review['status'] == '0')) {echo 'selected';} ?>>Vô hiệu</option>
							</select>
						</td>
					</tr>
					<tr>
						<td colspan="2" class="dt-center">
							<input type="submit" name="submit" value="Cập Nhật" class="submit">
						</td>
					</tr>
				</table>
			</form>
		</div>
	</div>
</section>
<?php endif; ?>