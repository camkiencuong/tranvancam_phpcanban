<?php
session_start();

$controller = isset($_GET['c']) ? $_GET['c'] : 'index';
$method = isset($_GET['m']) ? $_GET['m'] : 'index';

$className = ucfirst($controller) . 'Controller';

define('ROOT_PATH', dirname(__FILE__) . DIRECTORY_SEPARATOR);

define('CONTROLLER_PATH', ROOT_PATH . 'controllers' . DIRECTORY_SEPARATOR);

define('MODEL_PATH', ROOT_PATH . 'models' . DIRECTORY_SEPARATOR);

define('VIEW_PATH', ROOT_PATH . 'views' . DIRECTORY_SEPARATOR);

define('SYSTEM_PATH', ROOT_PATH . 'system' . DIRECTORY_SEPARATOR);

define('HELPER_PATH', ROOT_PATH . 'helpers' . DIRECTORY_SEPARATOR);

require_once ROOT_PATH . 'config.php';
require_once SYSTEM_PATH . 'Database.php';
require_once HELPER_PATH . 'helper.php';

if (file_exists(CONTROLLER_PATH . $className . '.php')) {
	require(CONTROLLER_PATH . $className . '.php');
} else {
	echo "'$className.php' không tồn tại!";
	exit;
}

$showView = new $className;
$showView->$method();

