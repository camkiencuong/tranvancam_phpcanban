<?php

class ProductCategory extends Database {

    public function getCategories($where = '', $limit = 10, $offset = 0, $orderby = '')
    {
        $sql = sprintf("SELECT * FROM product_categories WHERE 1=1 %s %s LIMIT %s OFFSET %s", $where, $orderby, $limit, $offset);

        try {
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi lấy nhiều bản ghi!');                
            }
            return $query->fetch_all(MYSQLI_ASSOC);
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return null;
    }
    
    public function getCategory($where)
    {
        $sql = sprintf("SELECT * FROM product_categories WHERE 1=1 %s LIMIT 1", $where);

        try {
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi lấy 1 bản ghi!');                
            }
            return $query->fetch_assoc();
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return null;
    }
    
    public function totalCategory($where = '')
    {
        $sql = sprintf("SELECT COUNT(*) FROM product_categories WHERE 1=1 %s", $where);
        try {
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi lấy tổng số bản ghi!');                
            }
            $result = $query->fetch_row();
            return $result[0];
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return null;
    }
    
    public function addCategory($data = '')
    {
        $cols = 'id, name, slug, image, description, parent_id, meta_title, meta_keyword, meta_description, status';

        $sql = sprintf("INSERT INTO product_categories (%s) VALUES (%s)", $cols, $data);

        try {
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi thêm mới danh mục!');
            }
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return null;
    }
    
    public function editCategory($set = '', $id = 0)
    {
        $sql = sprintf("UPDATE product_categories SET %s WHERE id=$id", $set, $id);
        
        try {
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi cập nhật danh mục!');
            }
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return null;
    }
    
    public function delCategory($id)
    {
        try {
            $sql = sprintf("DELETE FROM product_images WHERE product_id IN (SELECT id FROM products WHERE product_category_id IN (SELECT id FROM product_categories WHERE id=%s))", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại product_images!');
            }

            $sql = sprintf("DELETE FROM product_relates WHERE product_id IN (SELECT id FROM products WHERE product_category_id IN (SELECT id FROM product_categories WHERE id=%s))", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại product_relates!');
            }

            $sql = sprintf("DELETE FROM reviews WHERE product_id IN (SELECT id FROM products WHERE product_category_id IN (SELECT id FROM product_categories WHERE id=%s))", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại reviews!');
            }

            $sql = sprintf("DELETE FROM order_items WHERE product_id IN (SELECT id FROM products WHERE product_category_id IN (SELECT id FROM product_categories WHERE id=%s))", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại order_items!');
            }

            $sql = sprintf("DELETE FROM products WHERE product_category_id=%s", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại products!');
            }

            $sql = sprintf("DELETE FROM product_categories WHERE id=%s", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại product_categories!');
            }
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return false;
    }

}