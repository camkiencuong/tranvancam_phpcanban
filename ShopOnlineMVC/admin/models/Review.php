<?php

class Review extends Database {

	public function getReviews($where = '', $limit = 10, $offset = 0, $orderby = '')
	{
		$sql = sprintf("SELECT reviews.*, users.fullname AS user_name, products.name AS product_name FROM reviews LEFT JOIN users ON reviews.user_id=users.id LEFT JOIN products ON reviews.product_id=products.id  WHERE 1=1 %s %s LIMIT %s OFFSET %s", $where, $orderby, $limit, $offset);

		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi lấy nhiều bản ghi Review');				
			}
			return $query->fetch_all(MYSQLI_ASSOC);
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}
	
	public function getReview($where = '')
	{
		$sql = sprintf("SELECT reviews.*, users.fullname AS user_name, products.name AS product_name FROM reviews LEFT JOIN users ON reviews.user_id=users.id LEFT JOIN products ON reviews.product_id=products.id  WHERE 1=1 %s LIMIT 1", $where);

		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi lấy 1 bản ghi Review');				
			}
			return $query->fetch_assoc();
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}
	
	public function totalReview($where = '')
	{
		$sql = sprintf("SELECT COUNT(*) FROM reviews WHERE 1=1 %s", $where);

		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi lấy tổng số bản ghi Review');				
			}
			$result = $query->fetch_row();
			return $result[0];
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}
	
	public function editReview($set = '', $id = 0)
	{
		$sql = sprintf("UPDATE reviews SET %s WHERE id=%s", $set, $id);

		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi cập nhật review');
			}
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}
	
	public function delReview($id = 0)
	{
		$sql = sprintf("DELETE FROM reviews WHERE id=%s", $id);

		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi xóa tại reviews!');				
			}
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}
		
		return null;
	}

}