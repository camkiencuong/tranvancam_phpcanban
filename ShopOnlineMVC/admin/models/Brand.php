<?php

class Brand extends Database {

	public function getBrands($where = '', $limit = 10, $offset = 0, $orderby = '')
	{
		$sql = sprintf("SELECT * FROM brands WHERE 1=1 %s %s LIMIT %s OFFSET %s", $where, $orderby, $limit, $offset);

		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi sql!');				
			}
			return $query->fetch_all(MYSQLI_ASSOC);
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}
	
	public function getBrand($where)
	{
		$sql = sprintf("SELECT * FROM brands WHERE 1=1 %s LIMIT 1", $where);

		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi sql!');				
			}
			return $query->fetch_assoc();
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}
	
	public function totalBrand($where = '')
	{
		$sql = sprintf("SELECT COUNT(*) FROM brands WHERE 1=1 %s", $where);
		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi sql!');				
			}
			$result = $query->fetch_row();
			return $result[0];
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}
	
	public function addBrand($data = '')
	{
		$cols = 'id, name, slug, logo, image, description, meta_title, meta_keyword, meta_description, status';

		$sql = sprintf("INSERT INTO brands (%s) VALUES (%s)", $cols, $data);

		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi sql!');
			}
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}
	
	public function editBrand($set = '', $id = 0)
	{
		$sql = sprintf("UPDATE brands SET %s WHERE id=$id", $set, $id);
		
		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi sql!');
			}
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}

		return null;
	}
	
	public function delBrand($id)
	{
		try {
			$sql = sprintf("DELETE FROM product_images WHERE product_id IN (SELECT id FROM products WHERE brand_id IN (SELECT id FROM brands WHERE id=%s))", $id);
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi Xóa tại product_images!');
			}

			$sql = sprintf("DELETE FROM product_relates WHERE product_id IN (SELECT id FROM products WHERE brand_id IN (SELECT id FROM brands WHERE id=%s))", $id);
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi Xóa tại product_relates!');
			}

			$sql = sprintf("DELETE FROM reviews WHERE product_id IN (SELECT id FROM products WHERE brand_id IN (SELECT id FROM brands WHERE id=%s))", $id);
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi Xóa tại reviews!');
			}

			$sql = sprintf("DELETE FROM order_items WHERE product_id IN (SELECT id FROM products WHERE brand_id IN (SELECT id FROM brands WHERE id=%s))", $id);
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi Xóa tại order_items!');
			}

			$sql = sprintf("DELETE FROM products WHERE brand_id=%s", $id);
			$query = $this->_connect->query($sql);
			echo $sql;
			if (!$query) {
				throw new Exception('Lỗi Xóa tại products!');
			}

			$sql = sprintf("DELETE FROM brands WHERE id=%s", $id);
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi Xóa tại brands!');
			}
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}

		return false;
	}
	
}