<?php

class UserGroup extends Database {

	public function getUserGroups($where = '', $limit = 10, $offset = 0, $orderby = '')
	{
		$sql = sprintf("SELECT * FROM user_groups WHERE 1=1 %s %s LIMIT %s OFFSET %s", $where, $orderby, $limit, $offset);
		
		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi lấy nhiều bản ghi User Group!');				
			}
			return $query->fetch_all(MYSQLI_ASSOC);
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}
		
		return null;
	}

	public function getUserGroup($where = '')
	{
		$sql = sprintf("SELECT * FROM user_groups WHERE 1=1 %s LIMIT 1", $where);
		
		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi lấy 1 bản ghi User Group!');				
			}
			return $query->fetch_assoc();
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}
		
		return null;
	}

	public function totalUserGroup($where = '')
	{
		$sql = sprintf("SELECT COUNT(*) FROM user_groups WHERE 1=1 %s", $where);
		
		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi lấy tổng số bản ghi User Group!');				
			}
			$result = $query->fetch_row();
			return $result['0'];
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}
		
		return null;
	}

	public function addUserGroup($value = '')
	{
		$sql = sprintf("INSERT INTO user_groups (id, name, type, amount) VALUES (%s)", $value);
		
		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi thêm mới User Group!');				
			}
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}
		
		return null;
	}

	public function editUserGroup($set = '', $id = '')
	{
		$sql = sprintf("UPDATE user_groups SET %s WHERE id=%s", $set , $id);
		try {
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi cập nhật user group');
			}
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}
		return null;
	}

	public function delUserGroup($id = 0)
	{
		try {

			$sql = sprintf("UPDATE users SET user_group_id=1 WHERE user_group_id=%s", $id);
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi set user group id về mặc định');				
			}

			$sql = sprintf("DELETE FROM user_groups WHERE id=%s", $id);
			$query = $this->_connect->query($sql);
			if (!$query) {
				throw new Exception('Lỗi xóa tại User Group!');				
			}
		}
		catch (Exception $ex) {
			die($ex->getMessage());
		}
		
		return null;
	}

}