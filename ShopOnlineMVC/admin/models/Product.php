<?php

class Product extends Database {

    public function getProducts($where = '', $limit = 10, $offset = 0, $orderBy = '')
    {

        $sql = sprintf("SELECT products.*, product_categories.name as cate_name, brands.name as brand_name FROM products LEFT JOIN product_categories ON products.product_category_id = product_categories.id LEFT JOIN brands ON products.brand_id=brands.id WHERE 1=1 %s %s LIMIT %s OFFSET %s", $where, $orderBy, $limit, $offset);
        try {

            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi lấy nhiều bản ghi products');
            }
            return $query->fetch_all(MYSQLI_ASSOC);
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return null;
    }

    public function getProduct($where = '', $orderBy = '', $offset = 0)
    {

        $sql = sprintf("SELECT products.*, product_categories.name as cate_name, brands.name as brand_name FROM products LEFT JOIN product_categories ON products.product_category_id = product_categories.id LEFT JOIN brands ON products.brand_id=brands.id WHERE 1=1 %s %s LIMIT 1 OFFSET %s", $where, $orderBy, $offset);
        try {

            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi lấy 1 bản ghi product');
            }
            return $query->fetch_assoc();
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return null;
    }

    public function totalProduct($where = '')
    {
        $sql = sprintf("SELECT COUNT(*) FROM products WHERE 1=1 %s", $where);

        try {
            $query =  $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi lấy tổng product');            
            }
            $result = $query->fetch_row();
            return $result[0];
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }       

        return null;
    }

    public function addProduct($data = '')
    {
        $cols = 'id, sku, name, slug, price, colors, sizes, qty, brand_id, product_category_id, description, content, rate, is_new, is_promotion, is_featured, is_sale, created_at, updated_at, meta_title, meta_keyword, meta_description, status';

        $sql = sprintf("INSERT INTO products (%s) VALUES (%s)", $cols, $data);

        $query = $this->_connect->query($sql);
        if (!$query) {
            return 'Thêm lỗi! sql = '.$sql;
        }

        return null;
    }

    public function editProduct($set = '', $id = 0)
    {
        $sql = sprintf("UPDATE products SET %s WHERE id=$id", $set, $id);
        
        try {
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi cập nhật product');
            }
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return null;
    }

    public function delProduct($id = '')
    {
        try {
            $sql = sprintf("DELETE FROM product_images WHERE product_id=%s", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại product_images!');
            }

            $sql = sprintf("DELETE FROM product_relates WHERE product_id=%s", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại product_relates!');
            }

            $sql = sprintf("DELETE FROM reviews WHERE product_id=%s", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại reviews!');
            }

            $sql = sprintf("DELETE FROM order_items WHERE product_id=%s", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại order_items!');
            }

            $sql = sprintf("DELETE FROM products WHERE id=%s", $id);
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi Xóa tại products!');
            }
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return true;
    }

    public function getBrands()
    {
        $sql = "SELECT * FROM brands";

        try {
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi lấy danh sách brands');            
            }
            return $query->fetch_all(MYSQLI_ASSOC);
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }

        return null;
    }

    public function getCategories()
    {
        $sql = "SELECT * FROM product_categories";

        try {
            $query = $this->_connect->query($sql);
            if (!$query) {
                throw new Exception('Lỗi lấy danh sách product category');                
            }
            return $query->fetch_all(MYSQLI_ASSOC);
        }
        catch (Exception $ex) {
            die($ex->getMessage());
        }
        
        return null;
    }

}

