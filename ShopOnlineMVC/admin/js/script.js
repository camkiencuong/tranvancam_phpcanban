
function testDel() {
  var check = 0, checkboxes = document.getElementsByName('checkbox[]');
  for(var i in checkboxes) {
    if (checkboxes[i].checked == true) {
      ++check;
    }
  }
  if (check == 0) {
    alert('Bạn chưa chọn mục nào!');
    return false;
  } else {
    return confirm('Bạn chắc chắn muốn xóa không?');
  }
}
function testEdit() {
  var check = 0, checkboxes = document.getElementsByName('checkbox[]');
  for(var i in checkboxes) {
    if (checkboxes[i].checked == true) {
      ++check;
    }
  }
  if (check == 0) {
    alert('Bạn chưa chọn mục nào!');
    return false;
  } else {
    return true;
  }
}


function selectAll(source) {
  //function cho ô check
  checkboxes = document.getElementsByName('checkbox[]');
  for(var i in checkboxes)
    checkboxes[i].checked = source.checked;
}

function checkAll(){
  // function cho nút bấm
  var checkboxes = document.getElementsByName('checkbox[]');
  var button = document.getElementById('toggle');

  if(button.value == 'SelectAll'){
    for (var i in checkboxes){
      checkboxes[i].checked = 'FALSE';
    }
    button.value = 'UnSelectAll';
  }else{
    for (var i in checkboxes){
      checkboxes[i].checked = '';
    }
    button.value = 'SelectAll';
  }
}
function ChangeToSlug(value, num)
{
  var title, slug, id = 'slug'+num,
  slugs = document.getElementById(id);
  //Lấy text từ thẻ input title 
  title =  value;
  //Đổi chữ hoa thành chữ thường
  slug = title.toLowerCase();

  //Đổi ký tự có dấu thành không dấu
  slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
  slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
  slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
  slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
  slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
  slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
  slug = slug.replace(/đ/gi, 'd');
  //Xóa các ký tự đặt biệt
  slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
  //Đổi khoảng trắng thành ký tự gạch ngang
  slug = slug.replace(/ /gi, "-");
  //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
  //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
  slug = slug.replace(/\-\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-/gi, '-');
  slug = slug.replace(/\-\-/gi, '-');
  //Xóa các ký tự gạch ngang ở đầu và cuối
  slug = '@' + slug + '@';
  slug = slug.replace(/\@\-|\-\@|\@/gi, '');
  //In slug ra textbox có id “slug”
  slugs.value = slug;
}

function ToSlug()
{
  var title, slug;

  //Lấy text từ thẻ input title 
  title = document.getElementById("title").value;

  //Đổi chữ hoa thành chữ thường
  slug = title.toLowerCase();

  //Đổi ký tự có dấu thành không dấu
  slug = slug.replace(/á|à|ả|ạ|ã|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ/gi, 'a');
  slug = slug.replace(/é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ/gi, 'e');
  slug = slug.replace(/i|í|ì|ỉ|ĩ|ị/gi, 'i');
  slug = slug.replace(/ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ/gi, 'o');
  slug = slug.replace(/ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự/gi, 'u');
  slug = slug.replace(/ý|ỳ|ỷ|ỹ|ỵ/gi, 'y');
  slug = slug.replace(/đ/gi, 'd');
  //Xóa các ký tự đặt biệt
  slug = slug.replace(/\`|\~|\!|\@|\#|\||\$|\%|\^|\&|\*|\(|\)|\+|\=|\,|\.|\/|\?|\>|\<|\'|\"|\:|\;|_/gi, '');
  //Đổi khoảng trắng thành ký tự gạch ngang
  slug = slug.replace(/ /gi, "-");
  //Đổi nhiều ký tự gạch ngang liên tiếp thành 1 ký tự gạch ngang
  //Phòng trường hợp người nhập vào quá nhiều ký tự trắng
  slug = slug.replace(/\-\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-\-/gi, '-');
  slug = slug.replace(/\-\-\-/gi, '-');
  slug = slug.replace(/\-\-/gi, '-');
  //Xóa các ký tự gạch ngang ở đầu và cuối
  slug = '@' + slug + '@';
  slug = slug.replace(/\@\-|\-\@|\@/gi, '');
  //In slug ra textbox có id “slug”
  document.getElementById('slug').value = slug;
}

function testform(total) {
  var title, slug, idt, ids
  for (var i = 0; i < total; i++) {
    idt = 'title'+i;
    title = document.getElementById(idt);
    if (title.value == '') {
      alert('Vui lòng nhập tên ở dòng '+(i+1));
      title.select();
      return false;
    }
    for (var j = 0; j < i; j++) {
      ids2 = 'slug'+j;
      slug2 = document.getElementById(ids2).value;
      if (slug.value == slug2) {
        alert('slug dòng '+(i+1)+' bị trùng với dòng '+(j+1)+'. Hãy sửa lại tên ở dòng '+(i+1));
        title.select();
        return false;
      }
    }
  }
  return true;
}
      
function testFind() {
  var key = document.getElementById('keyword').value;
  if (key == '') {
    alert('Nhập từ khóa muốn tìm!');
    return false;
  }
  return true;
}

function testBack() {
  var nhap = document.getElementById('title0').value;
  if (nhap=='') {
    return true;
  } else {
    return confirm('Trở về sẽ mất dữ liệu đã sửa! Bạn có muốn trở về không?');
  }
}














