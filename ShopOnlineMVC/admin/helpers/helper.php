<?php

function view($view = '', $data = [])
{

	foreach ($data as $key => $items) {
		$$key = $items;
	}

	$view = @str_replace('.', '/', $view);
	$content = VIEW_PATH . $view . '.php';

	if (!file_exists($content)) {
		$content = VIEW_PATH . 'layout/error.php';
	}
	require VIEW_PATH . 'layout/layout.php'; 

}

function paging($queryString = '', $totalRecord = 1, $page = 1, $limit = 10)
{
	if ($totalRecord==null || $totalRecord<=$limit) {
		return null;
	}

	$totalPage = ceil($totalRecord/$limit);
	$offset = ($page-1)*$limit;
	$baseURL = 'index.php';
	$active = '';
	
	$html = '<ul>';
// tạo nút prev
	$html .= sprintf("<li><a class='move' href='%s%spage=%d'>prev</a></li>", $baseURL, $queryString, $page-1);
// tạo các page
	for ($i=1; $i <= $totalPage; $i++) { 
		if ($page==$i) {$active='acti';}
		$html .= sprintf("<li><a href='%s%spage=%d' class='page %s'>%d</a></li>", $baseURL, $queryString, $i, $active, $i);
		$active='';
	}
// tạo nút next
	$html .= sprintf("<li><a class='move' href='%s%spage=%d'>next</a></li>", $baseURL, $queryString, $page+1);
	$html .= '</ul>';
	return $html;
}

function queryString($get='',$unset='')
{
	if (count($get)==0) {
		return null;
	}

	if (is_array($unset)) {
		foreach ($unset as $key) {
			unset($get[$key]);
		}
	} else {
		unset($get[$unset]);
	}

	$queryString = http_build_query($get);

	return (($queryString == '') ? '?' : '?'.$queryString.'&');
}

function to_slug($str) 
{
	$str = trim(mb_strtolower($str));
	$str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
	$str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
	$str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
	$str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
	$str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
	$str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
	$str = preg_replace('/(đ)/', 'd', $str);
	$str = preg_replace('/[^a-z0-9-\s]/', '', $str);
	$str = preg_replace('/([\s]+)/', '-', $str);
	return $str;
}

function create_sku($category_id='',$brand_id='',$product_id='')
{
	if ($category_id=='' || $brand_id=='' || $product_id=='') {
		return random_int(100000000, 1000000000);
	}
	$sku = '100'.$category_id.'00'.$brand_id.'00'.$product_id;
	return $sku;
}

function status($value = '')
{
	if ($value == '1') {
		return 'Kích hoạt';
	} elseif ($value == '0') {
		return 'Vô hiệu';	
	} else {
		return '-';
	}
}

function isYes($value = '')
{
	if ($value == '1') {
		return 'yes';
	} else {
		return '-';
	}
}


