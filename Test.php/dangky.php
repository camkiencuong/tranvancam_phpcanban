<?php
session_start();
$error = [];
if (isset($_POST['submit'])) {
	if ($_POST['username'] == '') {
		$error[] = "Vui lòng điền Tên Đăng Nhập!";
	} elseif ($_POST['username'] != 'CamPro') {
		$error[] = "Đây là tài khoản mặc định.<br>Vui lòng không chỉnh sửa!";
	} else {
		$_SESSION['user'] = ['username' => $_POST['username']];
		header('Location: test.php');
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Đăng Ký</title>
	<link rel="stylesheet" type="text/css" href="css/style-dk.css">
</head>
<body>
	<div class="container">
		<div class="header">
			<div class="nameshow">
				<h2>Đăng Ký</h2>
				<h1>Trắc Nghiệm Thông Minh</h1>
			</div>
		</div>
		<form action="" method="POST">
			<table>
				<tr>
					<td colspan="2">
						<?php if (count($error)>0) :?>
							<?php for ($i=0; $i < count($error); $i++) :?>
								<div style="color:red"><?php echo $error[$i]; ?></div>
							<?php endfor; ?>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td>Tên Đăng Nhập: </td>
					<td><input class="nhap" type="text" name="username" placeholder=" Tên đăng nhập" value="CamPro"></td>
				</tr>
				<tr>
					<td>Mật Khẩu: </td>
					<td><input class="nhap" type="password" name="pass" placeholder=" Mật khẩu" value="12345678"></td>
				</tr>
				<tr>
					<td>Email: </td>
					<td><input class="nhap" type="text" name="Email" placeholder=" Email" value="abc@gmail.com"></td>
				</tr>
				<tr>
					<td>Tên Đầy Đủ: </td>
					<td><input class="nhap" type="text" name="fullname" placeholder=" Tên đầy đủ" value="Mr.Cam"></td>
				</tr>
				<tr>
					<td>Giới Tính: </td>
					<td>
						<input type="radio" name="gender" checked> Nam  <input type="radio" name="gender"> Nữ
					</td>
				</tr>
				<tr>
					<td colspan="2" class="dangnhap"><button type="submit" name="submit">Đăng Ký</button></td>
				</tr>
				<tr>
					<td colspan="2">

					</td>
				</tr>
			</table>
		</form>
	</div>

</body>
</html>