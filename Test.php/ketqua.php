<?php
	session_start();
	if (!isset($_SESSION['user']['ketqua'])) {
		header('Location: test.php');
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Kết Quả</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<div class="ketqua clear">
		<div>Số câu đúng: <big><?php echo $_SESSION['user']['ketqua'];?>/8</h4></big></div>
		<div>
			Kết Quả: 
			<?php if ($_SESSION['user']['ketqua'] >= 6) :?>
				<span style='color:lime;'>
					<?php echo "Đạt"; ?>
				</span>
				<div><a href="index.php">Làm Lại</a></div>
			<?php else :?>
				<span style='color:red;'>
					<?php echo "Bị Loại"; ?>
				</span>
				<div><a href="logout.php">Chấp Nhận Bị Loại</a></div>
			<?php endif; ?>
		</div>
	</div>
</body>
</html>
	
