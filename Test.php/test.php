<?php
session_start();
// input: mảng câu hỏi $questions, mảng câu trả lời $options, mảng đáp án $answers
// process:
// output: câu trả lời đúng tô màu xanh, câu trả lời sai tô màu đỏ và hiện câu trả lời đúng

	//Dap an
$answers = [
	['question_id' => 10, 'option' => 'C'],
	['question_id' => 20, 'option' => 'A'],
	['question_id' => 30, 'option' => 'C'],
	['question_id' => 40, 'option' => 'B'],
	['question_id' => 50, 'option' => 'C'],
	['question_id' => 60, 'option' => 'C'],
	['question_id' => 70, 'option' => 'D'],
	['question_id' => 80, 'option' => 'D']
];

	//Cac cau hoi
$questions = [
	[
		'id' => 10,
		'question' => 'Có một đàn vịt, cho biết 1 con đi trước thì có 2 con đi sau, 1 con đi sau thì có 2 con đi trước, 1 con đi giữa thì có 2 con đi 2 bên. Hỏi đàn vịt đó có mấy con?'
	],
	[
		'id' => 20,
		'question' => 'Làm thế nào để qua được câu này?'
	],
	[
		'id' => 30,
		'question' => 'Sở thú bị cháy, con gì chạy ra đầu tiên?'
	],
	[
		'id' => 40,
		'question' => 'Bệnh gì bác sỹ bó tay?'
	],
	[
		'id' => 50,
		'question' => 'Ở Việt Nam, rồng bay ở đâu và đáp ở đâu?'
	],
	[
		'id' => 60,
		'question' => 'Tay cầm cục thịt nắn nắn, tay vỗ mông là đang làm gì?'
	],
	[
		'id' => 70,
		'question' => 'Con gấu trúc ao ước gì mà không bao giờ được?'
	],
	[
		'id' => 80,
		'question' => 'Có 1 đàn chim đậu trên cành, người thợ săn bắn cái rằm. Hỏi chết mấy con?'
	]
];
	//Cac phuong an tra loi
$options = [
	[
		'question_id' => 60,
		'options' => ['A' => 'Nướng thịt', 'B' => 'Thái Thịt', 'C' => 'Cho con Bú', 'D' => 'Đấu vật'] 
	],
	[
		'question_id' => 10,
		'options' => ['A' => 1, 'B' => 2, 'C' => 3, 'D' => 4] 
	],       
	[
		'question_id' => 20,
		'options' => ['A' => 'Bỏ cuộc', 'B' => 'Cho tôi qua đi mà', 'C' => 'Không thể qua', 'D' => 'Câu này khó quá'] 
	],
	[
		'question_id' => 30,
		'options' => ['A' => 'Con chim', 'B' => 'Con rắn', 'C' => 'Con người','D' => 'Con tê giác'] 
	],
	[
		'question_id' => 40,
		'options' => ['A' => 'HIV', 'B' => 'Gãy tay', 'C' => 'Siđa', 'D' => 'Bệnh sĩ'] 
	],
	[
		'question_id' => 50,
		'options' => ['A' => 'Hà Nội và Long An', 'B' => 'Hà nội và Quảng Ninh', 'C' => 'Thăng Long và Hạ long', 'D' => 'Quảng Ninh và Long An'] 
	],
	[
		'question_id' => 70,
		'options' => ['A' => 'Ăn Kẹo', 'B' => 'Uống cocacola', 'C' => 'Đá bóng', 'D' => 'Chụp hình'] 
	],
	[
		'question_id' => 80,
		'options' => ['A' => 1,'B' => 2,'C' => 14,'D' => 15] 
	]
];

$idData = $ketqua = [];
if (!isset($_SESSION['user']['username'])) {
	header('location: index.php');
}
if (!isset($_SESSION['user']['avatar'])) {
	header('location: upload.php');
}
$image = getimagesize($_SESSION['user']['avatar']);
if (!is_array($image)) {
	header('location: upload.php');
}
if (count($questions) > 0) {
	for ($i=0; $i < count($questions); $i++) { 
		$idData[] = $questions[$i]['id'];
	}
}
if (isset($_SESSION['user']['ketqua']) && $_SESSION['user']['ketqua'] < 6){
	header('location: ketqua.php');
}

$demYes = 0;
if (isset($_POST['submit'])) {
	for ($i=0; $i < count($idData); $i++) { 
		if (!isset($_POST['qs'][$idData[$i]])) {
			$ketqua[$idData[$i]] = 'nocheck';
		} else {
			foreach ($answers as $as) {
				if ($as['question_id'] == $idData[$i] && $as['option'] == $_POST['qs'][$idData[$i]]) {
					$ketqua[$idData[$i]] = 'yes';
					++$demYes;
				} elseif ($as['question_id'] == $idData[$i] && $as['option'] != $_POST['qs'][$idData[$i]]) {
					$ketqua[$idData[$i]] = ['no' => $as['option']];
				}
			}
		}
	}
	$_SESSION['user']['ketqua'] = $demYes;
	if ($demYes > 0) {
		header('Location: ketqua.php');
	}
}


?>

<!DOCTYPE html>
<html>
<head>
	<title>Thông minh</title>
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="css/owl.carousel.min.css">
	<link rel="stylesheet" href="css/owl.theme.default.min.css">

	<script src="js/jquery.min.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script src="js/highlight.js"></script>
	<script src="js/app.js"></script>

</head>
<body>
	<div class="container">
		<div class="header">
			<div class="photo">
				<img src="http://sohanews.sohacdn.com/thumb_w/660/2013/sam3-3de05-crop1383728744029p.jpg">
			</div>
			<div class="nameshow">
				<h2>Chào mừng bạn đến với chương trình:</h2>
				<h1>Trắc Nghiệm Thông Minh</h1>
				<div class="userlogin">
				<div class="hellouser">
					Xin Chào, <big><b><?php echo $_SESSION['user']['username'];?></b></big>
				</div>
			</div>
			</div>
			<div class="userlogin">
				<div class="avatar"><img src="<?php echo $_SESSION['user']['avatar'];?>"></div>
				<div>
					<button><a href="upload.php">Thay Avatar</a></button>
					<a href="logout.php">Đăng Xuất</a>					
				</div>
			</div>
		</div>

		<form action="" method="post">
					<div class="row showcauhoi clear">
				<div class="owl-carousel owl-theme qsas">

					<!-- Kiểm tra có dữ liệu hay không -->
					<?php if (count($idData) > 0) :?>

						<!-- hiển thị câu hỏi theo danh sách id lấy được-->
						<?php for ($i=0; $i < count($idData); $i++) :?>
							<div class="item" data-hash="<?php echo 'cauhoi'.$i; ?>">

								<!-- lặp trong mảng $questions tìm câu hỏi giống id đã lấy -->
								<?php foreach ($questions as $qs) :?>
									<?php if ($qs['id'] == $idData[$i]) :?>

										<!-- hiển thị câu hỏi có cùng id -->
										<h4>
											<span class="cauhoi">
												<?php echo 'Câu hỏi ' . ($i+1) . ': '; ?>
											</span>
											<?php echo $qs['question'] . "<br>"; ?>
										</h4>
									<?php endif; ?>
								<?php endforeach; ?>

								<!-- hiển thị các lựa chọn (option) theo id đã lấy -->
								<!-- lặp qua mảng $options tìm phần tử có cùng id -->
								<?php foreach ($options as $ot) :?>
									<?php if ($ot['question_id'] == $idData[$i]) :?>

										<!-- lựa chọn A -->
										<div class="<?php if (isset($_POST['submit'])) {if ($ketqua[$idData[$i]] == 'yes' && $_POST['qs'][$idData[$i]] == 'A') {echo "dung";} elseif ($ketqua[$idData[$i]]['no'] == 'A') {echo "dung";} elseif ($_POST['qs'][$idData[$i]] == 'A') {echo 'sai';}} ?>">

											<!-- hiển thị nút radio cho A -->
											<input type="radio" name="qs[<?php echo $idData[$i]; ?>]" class="dapan " value="A" <?php if (isset($_POST['qs'][$idData[$i]]) && $_POST['qs'][$idData[$i]] == 'A') echo "checked"; ?>>

											<!-- hiển thị nội dung lựa chọn A -->
											<?php echo 'A. '.$ot['options']['A']; ?>
											<br>
										</div>

										<!-- lựa chọn B -->
										<div class="<?php if (isset($_POST['submit'])) {if ($ketqua[$idData[$i]] == 'yes' && $_POST['qs'][$idData[$i]] == 'B') {echo "dung";} elseif ($ketqua[$idData[$i]]['no'] == 'B') {echo "dung";} elseif ($_POST['qs'][$idData[$i]] == 'B') {echo 'sai';}} ?>">

											<!-- hiển thị nút radio cho B -->
											<input type="radio" name="qs[<?php echo $idData[$i]; ?>]" class="dapan " value="B" <?php if (isset($_POST['qs'][$idData[$i]]) && $_POST['qs'][$idData[$i]] == 'B') echo "checked"; ?>>

											<!-- hiển thị nội dung lựa chọn B -->
											<?php echo 'B. '.$ot['options']['B']; ?>
											<br>
										</div>

										<!-- lựa chọn C -->
										<div class="<?php if (isset($_POST['submit'])) {if ($ketqua[$idData[$i]] == 'yes' && $_POST['qs'][$idData[$i]] == 'C') {echo "dung";} elseif ($ketqua[$idData[$i]]['no'] == 'C') {echo "dung";} elseif ($_POST['qs'][$idData[$i]] == 'C') {echo 'sai';}} ?>">

											<!-- hiển thị nút radio cho C -->
											<input type="radio" name="qs[<?php echo $idData[$i]; ?>]" class="dapan " value="C" <?php if (isset($_POST['qs'][$idData[$i]]) && $_POST['qs'][$idData[$i]] == 'C') echo "checked"; ?>>

											<!-- hiển thị nội dung lựa chọn C -->
											<?php echo 'C. '.$ot['options']['C']; ?>
											<br>
										</div>

										<!-- lựa chọn D -->
										<div class="<?php if (isset($_POST['submit'])) {if ($ketqua[$idData[$i]] == 'yes' && $_POST['qs'][$idData[$i]] == 'D') {echo "dung";} elseif ($ketqua[$idData[$i]]['no'] == 'D') {echo "dung";} elseif ($_POST['qs'][$idData[$i]] == 'D') {echo 'sai';}} ?>">

											<!-- hiển thị nút radio cho D -->
											<input type="radio" name="qs[<?php echo $idData[$i]; ?>]" class="dapan " value="D" <?php if (isset($_POST['qs'][$idData[$i]]) && $_POST['qs'][$idData[$i]] == 'D') echo "checked"; ?>>

											<!-- hiển thị nội dung lựa chọn D -->
											<?php echo 'D. '.$ot['options']['D']; ?>
											<br>
										</div>

										<!-- hiển thị nhắc nhở -->
										<div class="nhacnho">
											<?php if (isset($_POST['submit'])) :?>
												<?php if ($ketqua[$idData[$i]] == 'nocheck') :?>
													<p class="chuachon">Vui lòng chọn 1 đáp án. OK?</p>
												<?php endif; ?>
											<?php endif; ?>
										</div>

									<?php endif; ?>
								<?php endforeach; ?>
							</div>
						<?php endfor; ?>
					<?php endif; ?>
				</div>
			</div>
			<!-- hiển thị các nút bấm vào câu hỏi muốn đến -->
			<div class="listcauhoi clear">
				<?php for ($i=0; $i < count($idData) ; $i++) :?>
					<a class="nutcauhoi" href="#cauhoi<?php echo $i; ?>">Câu hỏi <?php echo $i+1; ?></a>
				<?php endfor; ?>
			</div>

			<!-- nút submit xem kết quả -->
			<div class="nutsubmit clear">
				<input type="submit" name="submit" value="XEM KẾT QUẢ" class="xemkq">
			</div>
		</form>


		<script>
			$(document).ready(function() {
				$('.owl-carousel').owlCarousel({
					nav: true,
					items: 1,
					loop: false,
					center: true,
					margin: 10,
					callbacks: true,
					URLhashListener: true,
					autoplayHoverPause: true,
					startPosition: 'cauhoi1'
				});
			})
		</script>

	</div>
</body>
</html>