<?php
session_start();
if (isset($_SESSION['user']['username'])) {
	header('location: test.php');
}

$error = [];
if (isset($_POST['submit'])) {
	if ($_POST['username'] == '') {
		$error[] = "Vui lòng điền Tên Đăng Nhập!";
	} elseif ($_POST['username'] != 'CamPro') {
		$error[] = "Tên Đăng Nhập không tồn tại. Vui lòng đăng ký tài khoản";
	} else {
		$_SESSION['user']['username'] = $_POST['username'];
		if (isset($_POST['save'])) {
			setcookie("TenDangNhap", $_POST['username'], time()+86400, "/");
		}
		header('Location: test.php');
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Đăng Nhập</title>
	<link rel="stylesheet" type="text/css" href="css/styleindex.css">

</head>
<body>
	<div class="container">
		<div class="header">
			<div class="nameshow">
				<h2>Đăng Nhập</h2>
				<h1>Trắc Nghiệm Thông Minh</h1>
			</div>
		</div>
		<form action="" method="POST">
			<table>
				<tr>
					<td colspan="2">
						<?php if (count($error)>0) :?>
							<?php for ($i=0; $i < count($error); $i++) :?>
								<div style="color:red"><?php echo $error[$i]; ?></div>
							<?php endfor; ?>
						<?php endif; ?>
					</td>
				</tr>
				<tr>
					<td>Tên Đăng Nhập: </td>
					<td><input class="nhap" type="text" name="username" placeholder=" Tên đăng nhập" value="<?php if (isset($_COOKIE['TenDangNhap'])) echo $_COOKIE['TenDangNhap'];?>"></td>
				</tr>
				<tr>
					<td>Mật Khẩu: </td>
					<td><input class="nhap" type="password" name="pass" placeholder=" Mật khẩu"></td>
				</tr>
				<tr>
					<td colspan="2">
						<input type="checkbox" name="save" value="ok"> Lưu tên đăng nhập vào cookie để tự động đăng nhập
					</td>
				</tr>
				<tr>
					<td colspan="2" class="dangnhap"><button type="submit" name="submit">Đăng Nhập</button></td>
				</tr>
				<tr>
					<td><a href="#">Quên mật khẩu?</a></td>
				</tr>
				<tr>
					<td colspan="2">
						Chưa có tài khoản? 
						<a href="dangky.php">Đăng Ký Tài Khoản</a>
					</td>
				</tr>
			</table>
		</form>
	</div>

</body>
</html>