<?php
	session_start();
	if (isset($_SESSION['user'])) {
		unset($_SESSION['user']);
	} else {
		session_destroy();
	}
	header("location: login.php");
?>