<?php

function getAll($field='*',$table='',$where='',$limit=10,$offset=0,$join='',$orderby='')
{
	global $db;
	if ($table=='') {
		return null;
	}
	$sql = sprintf("SELECT %s FROM %s %s WHERE 1=1 %s %s LIMIT %d OFFSET %d", $field, $table, $join, $where, $orderby, $limit, $offset);
	$query = $db->query($sql);

	if ($query) {
		return $query->fetch_all(MYSQLI_ASSOC);
	}
	return null;
}

function getOne($field='*',$table='',$where='',$join='',$orderby='')
{
	global $db;
	if ($table=='') {
		return null;
	}
	$sql = sprintf("SELECT %s FROM %s %s WHERE 1=1 %s %s", $field, $table, $join, $where, $orderby);
	$query = $db->query($sql);

	if ($query) {
		return $query->fetch_assoc();
	}
	return null;
}

function totalRecord($table='',$where='')
{
	global $db;
	if ($table=='') {
		return null;
	}
	$sql = sprintf("SELECT COUNT(*) FROM %s WHERE 1=1 %s", $table, $where);
	$query = $db->query($sql);
	$result = $query->fetch_row();
	if ($query) {
		return $result[0];
	}
	return null;
}

function paging($baseURL='',$params='',$totalRecord=1,$page=1,$limit=10)
{
	$totalPage = ceil($totalRecord/$limit);
	if ($totalPage==1) {return null;}
	$offset = ($page-1)*$limit;
	unset($params['page']);
	$queryString = http_build_query($params);
	$active = '';
	
	$html = '<ul>';
// tạo nút prev
	if ($queryString != '' && $page>1) {
		$html .= sprintf("<li><a class='move' href='%s?%s&page=%d'>prev</a></li>", $baseURL, $queryString, $page-1);
	}
	if ($queryString == '' && $page>1) {
		$html .= sprintf("<li><a class='move' href='%s?page=%d'>prev</a></li>", $baseURL, $page-1);
	}
// tạo các page
	for ($i=1; $i <= $totalPage; $i++) { 
		if ($page==$i) {$active='acti';}
		if ($queryString != '') {
			$html .= sprintf("<li><a href='%s?%s&page=%d' class='page %s'>%d</a></li>", $baseURL, $queryString, $i, $active, $i);
		} else {
			$html .= sprintf("<li><a href='%s?page=%d' class='page %s'>%d</a></li>", $baseURL, $i, $active, $i);
		}
		$active='';
	}
// tạo nút next
	if ($queryString != '' && $page < $totalPage) {
		$html .= sprintf("<li><a class='move' href='%s?%s&page=%d'>next</a></li>", $baseURL, $queryString, $page+1);
	}
	if ($queryString == '' && $page < $totalPage) {
		$html .= sprintf("<li><a class='move' href='%s?page=%d'>next</a></li>", $baseURL, $page+1);
	}

	$html .= '</ul>';
	return $html;
}

function link_sapxep($params='')
{
	unset($params['cot']);
	unset($params['kieu']);
	
	$queryString = http_build_query($params);
	if ($queryString != '') {
		$link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[SCRIPT_NAME]".'?'.$queryString.'&';
	} else {
		$link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[SCRIPT_NAME]".'?';
	}
	return $link;
}





