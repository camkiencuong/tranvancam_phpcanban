<?php
session_start();
if (!isset($_SESSION['user'])) {
	header('location: login.php');
}
require('connect.php');
$type_login = 'student';
if (isset($_SESSION['user']['is_admin'])) {
	$type_login = 'admin';
} else {
	$masv = $_SESSION['user']['masv'];
}
$error=[];
$isCreated = 0;
$cur=@array_pop(explode('/',$_SERVER['SCRIPT_NAME']));
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>My School</title>
	<link href="http://mta.edu.vn//Portals/0/favicon.ico" rel="shortcut icon">
	<link rel="stylesheet" type="text/css" href="css/style.css">

</head>
<body>
	<header>
		<div class="container">
			<div class="logo">
				<a href="index.php"><img src="img/logo.png"></a>
			</div>
			<div class="sitename">
				<h1>My School HVKTQS</h1>
			</div>
			<div class="user">
				<p class="name">hello, <b><?php if (isset($_SESSION['user']['hoten'])) {echo $_SESSION['user']['hoten'];} if (isset($_SESSION['user']['is_admin'])) {echo 'Admin, '.$_SESSION['user']['fullname'];}?></b></p>
				<div class="logout"><a href="logout.php">Đăng xuất</a></div>
			</div>
			<div class="menu clearfix">
				<ul id="nav">
					<?php if($type_login=='admin'): ?>
					<li><a href="index.php" class="<?php if($cur=='index.php') echo 'active';?>">Trang Chủ</a></li>
					<?php endif; ?>
					<li><a href="khoa.php" class="<?php if($cur=='khoa.php') echo 'active';?>">Khoa</a></li>
					<li><a href="monhoc.php" class="<?php if($cur=='monhoc.php') echo 'active';?>">Môn Học</a></li>
					<li><a href="sinhvien.php" class="<?php if($cur=='sinhvien.php') echo 'active';?>">Sinh Viên</a></li>
					<li><a href="ketqua_diem.php" class="<?php if($cur=='ketqua_diem.php') echo 'active';?>">Bảng Điểm</a></li>
				</ul>
			</div>
		</div>
	</header>