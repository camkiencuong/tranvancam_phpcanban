<?php
require('connect.php');
require("header.php");

// lay nhieu ban ghi
$sql = "SELECT ketqua.diem, sinhvien.hoten, sinhvien.masv, mon_hoc.mamonhoc, mon_hoc.tenmonhoc FROM ketqua INNER JOIN sinhvien ON ketqua.masv=sinhvien.masv INNER JOIN mon_hoc ON ketqua.mamonhoc=mon_hoc.mamonhoc WHERE 1=1";
if($type_login!='admin') {
	$sql .= " AND ketqua.masv='{$masv}'";
}

// tìm kiếm
if (isset($_GET['masv']) && $_GET['masv'] != '') {
	$sql .= " AND ketqua.masv LIKE '%".$_GET['masv']."%'";
}
if (isset($_GET['hoten']) && $_GET['hoten'] != '') {
	$sql .= " AND hoten LIKE '%".$_GET['hoten']."%'";
}
if (isset($_GET['mamonhoc']) && $_GET['mamonhoc'] != '') {
	$sql .= " AND ketqua.mamonhoc LIKE '%".$_GET['mamonhoc']."%'";
}
if (isset($_GET['tenmonhoc']) && $_GET['tenmonhoc'] != '') {
	$sql .= " AND tenmonhoc LIKE '%".$_GET['tenmonhoc']."%'";
}
if (isset($_GET['diem']) && $_GET['diem'] != '') {
	$sql .= " AND diem LIKE '%".$_GET['diem']."%'";
}
// xắp sếp
if (isset($_GET['orderby']) && $_GET['orderby'] != '') {
	$cot = @array_shift(explode('-', $_GET['orderby']));
	$kieu = @array_pop(explode('-', $_GET['orderby']));
	$sql .= " ORDER BY $cot $kieu";
}

// phân trang
$query = $db->query($sql);
$count_kq = $query->num_rows;
$limit = 15;
$offset = 0;
$page = 1;
$total_page = ceil($count_kq/$limit);
$actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
if (isset($_GET['page'])) {
	$page = $_GET['page'];
	$offset = ($page-1)*$limit;
}
$sql .= " LIMIT $limit OFFSET $offset";
$query = $db->query($sql);
$bangdiem = $query->fetch_all(MYSQLI_ASSOC);

?>
<section>
	<div class="container danhsach">
		<h2 class="title">Danh sách điểm</h2>
		<div class="menu-small">
			<span class="sapxep">
				Sắp xếp ngay lập tức: 
				<select onchange="window.location.href=this.value" name="orderby" class="selecter1">
						<option value="">-- chọn --</option>
					<option value="http://phpcoban.demo/tranvancam_phpcanban/school/ketqua_diem.php?orderby=diem-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='diem-ASC') {echo 'selected';}?>>Điểm tăng</option>
					<option value="http://phpcoban.demo/tranvancam_phpcanban/school/ketqua_diem.php?orderby=diem-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='diem-DESC') {echo 'selected';}?>>Điểm giảm</option>
					<option value="http://phpcoban.demo/tranvancam_phpcanban/school/ketqua_diem.php?orderby=masv-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='masv-ASC') {echo 'selected';}?>>Mã SV tăng</option>
					<option value="http://phpcoban.demo/tranvancam_phpcanban/school/ketqua_diem.php?orderby=masv-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='masv-DESC') {echo 'selected';}?>>Mã SV giảm</option>
					<option value="http://phpcoban.demo/tranvancam_phpcanban/school/ketqua_diem.php?orderby=hoten-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='hoten-ASC') {echo 'selected';}?>>Tên SV tăng</option>
					<option value="http://phpcoban.demo/tranvancam_phpcanban/school/ketqua_diem.php?orderby=hoten-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='hoten-DESC') {echo 'selected';}?>>Tên SV giảm</option>
					<option value="http://phpcoban.demo/tranvancam_phpcanban/school/ketqua_diem.php?orderby=mamonhoc-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='mamonhoc-ASC') {echo 'selected';}?>>Mã môn tăng</option>
					<option value="http://phpcoban.demo/tranvancam_phpcanban/school/ketqua_diem.php?orderby=mamonhoc-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='mamonhoc-DESC') {echo 'selected';}?>>Mã môn giảm</option>
					<option value="http://phpcoban.demo/tranvancam_phpcanban/school/ketqua_diem.php?orderby=tenmonhoc-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='tenmonhoc-ASC') {echo 'selected';}?>>Tên môn tăng</option>
					<option value="http://phpcoban.demo/tranvancam_phpcanban/school/ketqua_diem.php?orderby=tenmonhoc-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='tenmonhoc-DESC') {echo 'selected';}?>>Tên môn giảm</option>
				</select>
			</span>
			<span class="message" style="padding-left:10%;">
				<?php if(isset($_SESSION['flash_message_ok'])) :?>
					<span style="color:blue;"><?php echo $_SESSION['flash_message_ok'];?></span>
				<?php endif; unset($_SESSION['flash_message_ok']);?>
				<?php if(isset($_SESSION['flash_message'])) :?>
					<span style="color:red;"><?php echo $_SESSION['flash_message'];?></span>
				<?php endif; unset($_SESSION['flash_message']);?>
			</span>
		</div>
		<div class="tuychon clearfix cach-top">
			<form action="" method="get">
				<span class="sapxep">
					Sắp xếp theo: 
					<select name="orderby" class="selecter1">
						<option value="">-- chọn --</option>
						<option value="diem-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='diem-ASC') {echo 'selected';}?>>Điểm tăng</option>
						<option value="diem-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='diem-DESC') {echo 'selected';}?>>Điểm giảm</option>
						<option value="masv-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='masv-ASC') {echo 'selected';}?>>Mã SV tăng</option>
						<option value="masv-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='masv-DESC') {echo 'selected';}?>>Mã SV giảm</option>
						<option value="hoten-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='hoten-ASC') {echo 'selected';}?>>Tên SV tăng</option>
						<option value="hoten-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='hoten-DESC') {echo 'selected';}?>>Tên SV giảm</option>
						<option value="mamonhoc-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='mamonhoc-ASC') {echo 'selected';}?>>Mã môn tăng</option>
						<option value="mamonhoc-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='mamonhoc-DESC') {echo 'selected';}?>>Mã môn giảm</option>
						<option value="tenmonhoc-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='tenmonhoc-ASC') {echo 'selected';}?>>Tên môn tăng</option>
						<option value="tenmonhoc-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='tenmonhoc-DESC') {echo 'selected';}?>>Tên môn giảm</option>
					</select>
					<input type="submit" name="sapxep" value="Sắp xếp" class="submit">
				</span>
				<span class="timkiem">
					Tìm kiếm: 
					<input type="text" name="diem" class="tukhoa" placeholder="điểm" value="<?php if (isset($_GET['diem']) && $_GET['diem']!='') {echo $_GET['diem'];}?>">
					<input type="text" name="masv" class="tukhoa" placeholder="mã sinh viên" value="<?php if (isset($_GET['masv']) && $_GET['masv']!='') {echo $_GET['masv'];}?>">
					<input type="text" name="hoten" class="tukhoa" placeholder="tên sinh viên" value="<?php if (isset($_GET['hoten']) && $_GET['hoten']!='') {echo $_GET['hoten'];}?>">
					<input type="text" name="mamonhoc" class="tukhoa" placeholder="mã môn" value="<?php if (isset($_GET['mamonhoc']) && $_GET['mamonhoc']!='') {echo $_GET['mamonhoc'];}?>">
					<input type="text" name="tenmonhoc" class="tukhoa" placeholder="tên môn" value="<?php if (isset($_GET['tenmonhoc']) && $_GET['tenmonhoc']!='') {echo $_GET['tenmonhoc'];}?>">
					<input type="submit" name="timkiem" class="submit" value="Tìm kiếm">
				</span>
			</form>
		</div>
		<div class="table-center clearfix">
			<table>
				<thead>
					<tr>
						<th>Điểm</th>
						<th>Tên sinh viên</th>
						<th>Mã sinh viên</th>
						<th>Tên môn học</th>
						<th>Mã môn</th>
						<?php if ($type_login=="admin"): ?>
							<th colspan="2">Tùy chọn</th>
						<?php endif; ?>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach ($bangdiem as $diem) :
						?>
						<tr>
							<td class="center"><?php echo $diem['diem'];?></td>
							<td><?php echo $diem['hoten'];?></td>
							<td class="center"><?php echo $diem['masv'];?></td>
							<td><?php echo $diem['tenmonhoc'];?></td>
							<td class="center"><?php echo $diem['mamonhoc'];?></td>
							<?php if ($type_login=="admin"): ?>
								<td class="tacvu"><a href="ketqua_update.php?masv=<?php echo $diem['masv'];?>">sửa điểm</a></td>
								<td class="tacvu"><a onclick="return confirm('Xác nhận xóa?');" href="delete.php?madiem=<?php echo $diem['masv'];?>&mamon=<?php echo $diem['mamonhoc'];?>">xóa điểm</a></td>
							<?php endif; ?>
						</tr>
						<?php
					endforeach;
					?>
				</tbody>
			</table>
		</div>
		<?php if ($total_page > 1): ?>
			<div class="phantrang full-center clearfix">
				<a class="move <?php if($page=='1') {echo 'vohieu';} else {echo 'page';}?>" href="<?php if(($page-1)>0) {echo $actual_link.'&page='.($page-1);} else {echo '#';}?>">prev</a>
				<?php for ($i=1; $i <= $total_page; $i++): ?>
					<a class="page <?php if($page==$i) echo 'active_page';?>" href="<?php echo $actual_link.'&page='.$i;?>"><?php echo $i;?></a>
				<?php endfor; ?>
				<a class="move <?php if($page==$total_page) {echo 'vohieu';} else {echo 'page';}?>" href="<?php if(($page+1)<=$total_page) {echo $actual_link.'&page='.($page+1);} else {echo '#';}?>">next</a>
			</div>
		<?php endif; ?>
	</div>
</section>


<?php
include('footer.php');
?>