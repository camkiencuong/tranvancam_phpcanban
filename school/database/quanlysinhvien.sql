-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 03, 2018 at 05:12 PM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `quanlysinhvien`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(20) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(16) NOT NULL,
  `status` varchar(10) NOT NULL,
  `fullname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `status`, `fullname`) VALUES
(1, 'campro', '87654321', '1', 'Cam Pro'),
(2, 'camkc', '87654321', '0', 'Cam Block');

-- --------------------------------------------------------

--
-- Table structure for table `ketqua`
--

CREATE TABLE `ketqua` (
  `diem` float NOT NULL,
  `masv` varchar(50) NOT NULL,
  `mamonhoc` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `ketqua`
--

INSERT INTO `ketqua` (`diem`, `masv`, `mamonhoc`) VALUES
(8, 'sv0156', 'MH01'),
(9, 'sv0156', 'MH02'),
(7, 'sv0098', 'MH02'),
(8, 'sv0226', 'MH03'),
(6, 'sv0375', 'MH05'),
(7, 'sv1112', 'MH04'),
(9, 'sv0851', 'MH04'),
(7, 'sv1070', 'MH04'),
(5, 'sv0846', 'MH05'),
(8, 'sv0094', 'MH03'),
(5, 'sv0846', 'MH03'),
(6, 'sv0850', 'MH02'),
(1, 'sv0094', 'MH01'),
(6, 'sv0094', 'MH02'),
(7, 'sv0094', 'MH04'),
(5, 'sv0094', 'MH05'),
(9, 'sv0098', 'MH01'),
(8, 'sv0098', 'MH03'),
(7, 'sv0098', 'MH04'),
(9, 'sv0098', 'MH05'),
(4, 'sv0156', 'MH03'),
(1, 'sv0156', 'MH04'),
(5, 'sv0156', 'MH05'),
(6, 'sv0226', 'MH01'),
(5, 'sv0226', 'MH02'),
(9, 'sv0226', 'MH04'),
(7, 'sv0226', 'MH05'),
(9, 'sv1035', 'MH01'),
(8, 'sv1035', 'MH02'),
(5, 'sv1035', 'MH03'),
(3, 'sv1035', 'MH04'),
(7, 'sv1035', 'MH05'),
(9, 'sv1682', 'MH01'),
(6, 'sv1682', 'MH02'),
(8, 'sv1682', 'MH03'),
(4, 'sv1682', 'MH04'),
(3, 'sv1682', 'MH05'),
(7, 'sv1049', 'MH01'),
(8, 'sv1049', 'MH02'),
(9, 'sv1049', 'MH03'),
(7, 'sv1049', 'MH04'),
(6, 'sv1049', 'MH05'),
(1, 'sv1722', 'MH01'),
(2, 'sv1722', 'MH02'),
(5, 'sv1722', 'MH03'),
(3, 'sv1722', 'MH04'),
(5, 'sv1722', 'MH05'),
(8, 'sv1680', 'MH01'),
(4, 'sv1680', 'MH02'),
(5, 'sv1680', 'MH03'),
(6, 'sv1680', 'MH04'),
(2, 'sv1680', 'MH05'),
(6, 'sv0252', 'MH01'),
(5, 'sv0252', 'MH02'),
(8, 'sv0252', 'MH03'),
(4, 'sv0252', 'MH04'),
(2, 'sv0252', 'MH05'),
(6, 'sv0375', 'MH01'),
(5, 'sv0375', 'MH02'),
(8, 'sv0375', 'MH03'),
(7, 'sv0375', 'MH04'),
(8, 'sv0843', 'MH01'),
(9, 'sv0843', 'MH02'),
(5, 'sv0843', 'MH03'),
(1, 'sv0843', 'MH04'),
(2, 'sv0843', 'MH05'),
(3, 'sv0845', 'MH01'),
(5, 'sv0845', 'MH02'),
(6, 'sv0845', 'MH03'),
(5, 'sv0845', 'MH04'),
(8, 'sv0845', 'MH05'),
(7, 'sv0846', 'MH01'),
(8, 'sv0846', 'MH02'),
(9, 'sv0846', 'MH04'),
(3, 'sv0850', 'MH01'),
(4, 'sv0850', 'MH03'),
(7, 'sv0850', 'MH04'),
(8, 'sv0850', 'MH05'),
(8, 'sv0851', 'MH01'),
(5, 'sv0851', 'MH02'),
(6, 'sv0851', 'MH03'),
(4, 'sv0851', 'MH05'),
(7, 'sv0853', 'MH01'),
(8, 'sv0853', 'MH02'),
(9, 'sv0853', 'MH03'),
(4, 'sv0853', 'MH04'),
(3, 'sv0853', 'MH05'),
(7, 'sv1032', 'MH01'),
(4, 'sv1032', 'MH02'),
(6, 'sv1032', 'MH03'),
(8, 'sv1032', 'MH04'),
(2, 'sv1032', 'MH05'),
(1, 'sv1043', 'MH01'),
(2, 'sv1043', 'MH02'),
(3, 'sv1043', 'MH03'),
(8, 'sv1043', 'MH04'),
(9, 'sv1043', 'MH05'),
(7, 'sv1054', 'MH01'),
(4, 'sv1054', 'MH02'),
(5, 'sv1054', 'MH03'),
(8, 'sv1054', 'MH04'),
(2, 'sv1054', 'MH05');

-- --------------------------------------------------------

--
-- Table structure for table `khoa`
--

CREATE TABLE `khoa` (
  `makhoa` varchar(20) NOT NULL,
  `ten_khoa` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `khoa`
--

INSERT INTO `khoa` (`makhoa`, `ten_khoa`) VALUES
('k01', 'Khoa Cơ khí'),
('k02', 'Khoa Công nghệ thông tin'),
('k03', 'Khoa Hàng không Vũ trụ'),
('k04', 'Khoa Kỹ thuật điều khiển'),
('k05', 'Khoa Vô tuyến điện tử'),
('k06', 'khoa 50 mâm'),
('k07', 'khoa chống lầy'),
('k08', 'khoa gì cũng đk');

-- --------------------------------------------------------

--
-- Table structure for table `mon_hoc`
--

CREATE TABLE `mon_hoc` (
  `mamonhoc` varchar(20) NOT NULL,
  `tenmonhoc` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `mon_hoc`
--

INSERT INTO `mon_hoc` (`mamonhoc`, `tenmonhoc`) VALUES
('MH01', 'Lập trình đa phương tiện'),
('MH02', 'Các hệ thống phân tán'),
('MH03', 'Phân tích Malwares'),
('MH04', 'Quản lý dự án phần mềm'),
('MH05', 'Đạo đức nghề nghiệp');

-- --------------------------------------------------------

--
-- Table structure for table `sinhvien`
--

CREATE TABLE `sinhvien` (
  `masv` varchar(50) NOT NULL,
  `hoten` varchar(100) NOT NULL,
  `ngaysinh` date DEFAULT NULL,
  `gioitinh` tinyint(4) NOT NULL,
  `diachi` varchar(100) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `makhoa` varchar(20) NOT NULL,
  `password` varchar(16) NOT NULL,
  `status` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sinhvien`
--

INSERT INTO `sinhvien` (`masv`, `hoten`, `ngaysinh`, `gioitinh`, `diachi`, `email`, `makhoa`, `password`, `status`) VALUES
('sv0094', 'hoàng bách', '2018-03-07', 1, 'quảng trị', 'aijfi@gmail.com', 'k04', '12345678', '1'),
('sv0098', 'Trần Văn Came', '2018-03-02', 1, 'Phú xuyên', 'camtry@gmail.com', 'k04', '12345678', '1'),
('sv0156', 'nguyễn anh tuấn', '2018-03-31', 1, 'Bắc ninh', 'ilkkipahs@gmail.com', 'k01', '12345678', '0'),
('sv0226', 'Mai hoàng yến', '2018-03-09', 0, 'vĩnh phúc', 'guhijpo@gmail.com', 'k03', '12345678', '1'),
('sv0252', 'nguyễn quang đại', '2018-03-08', 1, 'hà nội', 'kajsdi@gmail.com', 'k01', '12345678', '1'),
('sv0375', 'Cô giáo thảo', '2018-03-05', 0, 'bắc giang', 'jiasdkfnk@gmail.com', 'k05', '12345678', '0'),
('sv0843', 'Mai hoàng yến', '2018-03-09', 0, 'vĩnh phúc', 'guhijpo@gmail.com', 'k03', '12345678', '1'),
('sv0845', 'Tống xuân trường', '2018-03-20', 1, 'hà nội', 'hkjhagu@gmail.com', 'k04', '12345678', '1'),
('sv0846', 'Cô giáo thảo', '2018-03-05', 0, 'bắc giang', 'jiasdkfnk@gmail.com', 'k05', '12345678', '0'),
('sv0848', 'nguyễn anh duy', '2018-03-19', 1, 'hưng yên', 'yghonhu@gmail.com', 'k05', '12345678', '1'),
('sv0850', 'phạm mỹ duyên', '2018-03-16', 0, 'hà nam', 'gbjhhihh@gmail.com', 'k02', '12345678', '1'),
('sv0851', 'vũ thị giang', '2018-03-22', 0, 'quảng ninh', 'ignv@gmail.com', 'k05', '12345678', '1'),
('sv0852', 'phan đông dương', '2018-03-20', 1, 'lý sơn', 'jaif@gmail.com', 'k05', '12345678', '1'),
('sv0853', 'lê thị hiền', '2018-03-20', 0, 'kiến xương', 'uuyygfv@gmail.com', 'k04', '12345678', '1'),
('sv0854', 'hà đình đức', '2018-03-11', 1, 'tàu khựa', 'tfhgb@gmail.com', 'k02', '12345678', '1'),
('sv0856', 'Trần anh dũng', '2018-03-06', 1, 'lạng sơn', 'aidfaihdf@gmail.com', 'k05', '12345678', '1'),
('sv0858', 'nguyễn vắn đạt', '2018-03-14', 1, 'hà nội', 'aghj@gmail.com', 'k05', '12345678', '1'),
('sv0859', 'nguyễn hải phòng', '2018-03-19', 1, 'nam dương', 'lhgug@gmail.com', 'k01', '12345678', '1'),
('sv0860', 'nguyễn bá huy', '2018-03-17', 1, 'ha noi', 'aghtjuyjgh@gmail.com', 'k01', '12345678', '1'),
('sv0862', 'nguyễn hải phòng', '2018-03-19', 1, 'nam dương', 'lhgug@gmail.com', 'k01', '12345678', '1'),
('sv1029', 'chu văn dũng', '2018-03-02', 1, 'hà nam', 'iubj@gmail.com', 'k05', '12345678', '1'),
('sv1030', 'nguyễn anh tuấn', '2018-03-31', 1, 'Bắc ninh', 'ilkkipahs@gmail.com', 'k01', '12345678', '1'),
('sv1031', 'vũ tông thủy', '2018-03-05', 1, 'thanh hóa', 'oiasfn@gmail.com', 'k02', '12345678', '0'),
('sv1032', 'phạm vũ thảo my', '2018-03-01', 0, 'thái bình', 'aidfha@gmail.com', 'k02', '12345678', '1'),
('sv1035', 'nguyễn bá huy', '2018-03-17', 1, 'ha noi', 'aghtjuyjgh@gmail.com', 'k01', '12345678', '1'),
('sv1038', 'bùi văn hiển', '2018-03-12', 1, 'hà nam ninh', 'kalsjfdiaj@gmail.com', 'k01', '12345678', '1'),
('sv1039', 'hoàng bách', '2018-03-07', 1, 'quảng trị', 'aijfi@gmail.com', 'k04', '12345678', '1'),
('sv1040', 'nguyễn vắn đạt', '2018-03-14', 1, 'hà nội', 'aghj@gmail.com', 'k05', '12345678', '1'),
('sv1042', 'nguyễn hải phòng', '2018-03-19', 1, 'nam dương', 'lhgug@gmail.com', 'k01', '12345678', '1'),
('sv1043', 'lê thị hiền', '2018-03-20', 0, 'kiến xương', 'uuyygfv@gmail.com', 'k04', '12345678', '1'),
('sv1044', 'nguyễn quang đại', '2018-03-08', 1, 'hà nội', 'kajsdi@gmail.com', 'k01', '12345678', '1'),
('sv1045', 'hoàng công danh', '2018-03-15', 1, 'bắc ninh', 'yutydtsdgf@gmail.com', 'k03', '12345678', '1'),
('sv1047', 'chu văn dũng', '2018-03-02', 1, 'hà nam', 'iubj@gmail.com', 'k05', '12345678', '1'),
('sv1048', 'chu văn dũng', '2018-03-02', 1, 'hà nam', 'iubj@gmail.com', 'k05', '12345678', '1'),
('sv1049', 'phạm mỹ duyên', '2018-03-16', 0, 'hà nam', 'gbjhhihh@gmail.com', 'k02', '12345678', '1'),
('sv1051', 'Tống xuân trường', '2018-03-20', 1, 'hà nội', 'hkjhagu@gmail.com', 'k04', '12345678', '1'),
('sv1053', 'đào duy lượng', '2018-03-13', 1, 'hải phòng', 'ihj@gmail.com', 'k04', '12345678', '1'),
('sv1054', 'trịnh hải kiều', '2018-03-29', 0, 'xuân mai', 'aidhsf@gmail.com', 'k04', '12345678', '1'),
('sv1070', 'Trần anh dũng', '2018-03-06', 1, 'lạng sơn', 'aidfaihdf@gmail.com', 'k05', '12345678', '1'),
('sv1112', 'nguyễn quang đại', '2018-03-08', 1, 'hà nội', 'kajsdi@gmail.com', 'k01', '12345678', '1'),
('sv1360', 'bùi văn hiển', '2018-03-12', 1, 'hà nam ninh', 'kalsjfdiaj@gmail.com', 'k01', '12345678', '1'),
('sv1625', 'đào duy lượng', '2018-03-13', 1, 'hải phòng', 'ihj@gmail.com', 'k04', '12345678', '1'),
('sv1629', 'hoàng công danh', '2018-03-15', 1, 'bắc ninh', 'yutydtsdgf@gmail.com', 'k03', '12345678', '1'),
('sv1634', 'hoàng công danh', '2018-03-15', 1, 'bắc ninh', 'yutydtsdgf@gmail.com', 'k03', '12345678', '1'),
('sv1680', 'trịnh hải kiều', '2018-03-29', 0, 'xuân mai', 'aidhsf@gmail.com', 'k04', '12345678', '1'),
('sv1682', 'phạm vũ thảo my', '2018-03-01', 0, 'thái bình', 'aidfha@gmail.com', 'k02', '12345678', '1'),
('sv1684', 'nguyễn anh tuấn', '2018-03-31', 1, 'Bắc ninh', 'ilkkipahs@gmail.com', 'k01', '12345678', '1'),
('sv1721', 'nguyễn anh tuấn', '2018-03-31', 1, 'Bắc ninh', 'ilkkipahs@gmail.com', 'k01', '12345678', '1'),
('sv1722', 'vũ tông thủy', '2018-03-05', 1, 'thanh hóa', 'oiasfn@gmail.com', 'k02', '12345678', '0'),
('sv1723', 'nguyễn bá huy', '2018-03-17', 1, 'ha noi', 'aghtjuyjgh@gmail.com', 'k01', '12345678', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ketqua`
--
ALTER TABLE `ketqua`
  ADD KEY `masv` (`masv`),
  ADD KEY `mamonhoc` (`mamonhoc`);

--
-- Indexes for table `khoa`
--
ALTER TABLE `khoa`
  ADD PRIMARY KEY (`makhoa`);

--
-- Indexes for table `mon_hoc`
--
ALTER TABLE `mon_hoc`
  ADD PRIMARY KEY (`mamonhoc`);

--
-- Indexes for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD PRIMARY KEY (`masv`),
  ADD KEY `makhoa` (`makhoa`),
  ADD KEY `hoten` (`hoten`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `ketqua`
--
ALTER TABLE `ketqua`
  ADD CONSTRAINT `ketqua_ibfk_1` FOREIGN KEY (`masv`) REFERENCES `sinhvien` (`masv`),
  ADD CONSTRAINT `ketqua_ibfk_2` FOREIGN KEY (`mamonhoc`) REFERENCES `mon_hoc` (`mamonhoc`);

--
-- Constraints for table `sinhvien`
--
ALTER TABLE `sinhvien`
  ADD CONSTRAINT `sinhvien_ibfk_1` FOREIGN KEY (`makhoa`) REFERENCES `khoa` (`makhoa`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
