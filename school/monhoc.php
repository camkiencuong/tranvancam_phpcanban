<?php
require('connect.php');
require("header.php");

$sql = "SELECT * FROM mon_hoc WHERE 1=1";

// tìm kiếm
if (isset($_GET['mamonhoc']) && $_GET['mamonhoc'] != '') {
	$sql .= " AND mamonhoc LIKE '%".$_GET['mamonhoc']."%'";
}
if (isset($_GET['tenmonhoc']) && $_GET['tenmonhoc'] != '') {
	$sql .= " AND tenmonhoc LIKE '%".$_GET['tenmonhoc']."%'";
}
// xắp sếp
if (isset($_GET['orderby']) && $_GET['orderby'] != '') {
	$cot = @array_shift(explode('-', $_GET['orderby']));
	$kieu = @array_pop(explode('-', $_GET['orderby']));
	$sql .= " ORDER BY $cot $kieu";
}

$query = $db->query($sql);
$result = $query->fetch_all(MYSQLI_ASSOC);

?>
<section>
	<div class="container danhsach">
		<h2 class="title">Danh sách môn học</h2>
		<?php if($type_login=='admin'): ?>
		<div class="menu-small">
			<a href="monhoc_create.php">Thêm môn học</a>
			<span class="message" style="padding-left:10%;">
				<?php if(isset($_SESSION['flash_message_ok'])) :?>
					<span style="color:blue;"><?php echo $_SESSION['flash_message_ok'];?></span>
				<?php endif; unset($_SESSION['flash_message_ok']);?>
				<?php if(isset($_SESSION['flash_message'])) :?>
					<span style="color:red;"><?php echo $_SESSION['flash_message'];?></span>
				<?php endif; unset($_SESSION['flash_message']);?>
			</span>
		</div>
		<?php endif; ?>
		<div class="tuychon clearfix cach-top">
			<form action="" method="get">
				<span class="sapxep">
					Sắp xếp theo: 
					<select name="orderby" class="selecter1">
						<option value="mamonhoc-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='mamonhoc-ASC') {echo 'selected';}?>>Mã môn tăng</option>
						<option value="mamonhoc-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='mamonhoc-DESC') {echo 'selected';}?>>Mã môn giảm</option>
						<option value="tenmonhoc-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='tenmonhoc-ASC') {echo 'selected';}?>>Tên môn tăng</option>
						<option value="tenmonhoc-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='tenmonhoc-DESC') {echo 'selected';}?>>Tên môn giảm</option>
					</select>
					<input type="submit" name="sapxep" value="Sắp xếp" class="submit">
				</span>
				<span class="timkiem">
					Tìm kiếm: 
					<input type="text" name="mamonhoc" class="tukhoa" placeholder="mã môn" value="<?php if (isset($_GET['mamonhoc']) && $_GET['mamonhoc']!='') {echo $_GET['mamonhoc'];}?>">
					<input type="text" name="tenmonhoc" class="tukhoa" placeholder="tên môn" value="<?php if (isset($_GET['tenmonhoc']) && $_GET['tenmonhoc']!='') {echo $_GET['tenmonhoc'];}?>">
					<input type="submit" name="timkiem" class="submit" value="Tìm kiếm">
				</span>
			</form>
		</div>
		<div class="table-center clearfix">
			<table>
				<thead>
					<tr>
						<th>STT</th>
						<th>Mã môn học</th>
						<th>Tên môn học</th>
						<?php if($type_login=='admin'): ?>
						<th colspan="2">Tùy chọn</th>
						<?php endif; ?>
					</tr>
				</thead>
				<tbody>
					<?php 
					$stt=0;
					if (count($result) > 0) :
						foreach ($result as $sv) :
							?>
							<tr>
								<td class="center"><?php echo ++$stt;?></td>
								<td class="center"><?php echo $sv['mamonhoc'];?></td>
								<td><?php echo $sv['tenmonhoc'];?></td>
								<?php if($type_login=='admin'): ?>
								<td class="tacvu"><a href="monhoc_edit.php?mamonhoc=<?php echo $sv['mamonhoc'];?>">sửa</a></td>
								<td class="tacvu"><a onclick="return confirm('Xác nhận xóa?');" href="delete.php?mamonhoc=<?php echo $sv['mamonhoc'];?>">xóa</a></td>
								<?php endif; ?>
							</tr>
							<?php
						endforeach;
					endif; 
					?>
				</tbody>
			</table>
		</div>
	</div>
</section>

<?php
include('footer.php');
?>