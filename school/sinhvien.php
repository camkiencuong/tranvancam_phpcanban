<?php
require('connect.php');
require('header.php');
require('functions.php');

$join = '';
$orderby = '';
// không phải admin chỉ hiện thông tin của 1 sv
$where = ($type_login!='admin' ? " AND masv='".$masv."'" :'');

// tìm kiếm
if (isset($_GET['masv']) && $_GET['masv'] != '') {
	$where .= " AND masv LIKE '%".$_GET['masv']."%'";
}
if (isset($_GET['hoten']) && $_GET['hoten'] != '') {
	$where .= " AND hoten LIKE '%".$_GET['hoten']."%'";
}
if (isset($_GET['ngaysinh']) && $_GET['ngaysinh'] != '') {
	$where .= " AND ngaysinh LIKE '%".$_GET['ngaysinh']."%'";
}
if (isset($_GET['email']) && $_GET['email'] != '') {
	$where .= " AND email LIKE '%".$_GET['email']."%'";
}
if (isset($_GET['gioitinh']) && $_GET['gioitinh'] != '') {
	$where .= " AND gioitinh LIKE '".$_GET['gioitinh']."'";
}
if (isset($_GET['status']) && $_GET['status'] != '') {
	$where .= " AND status LIKE '".$_GET['status']."'";
}
// xắp sếp
if (isset($_GET['cot']) && $_GET['cot'] != '' && isset($_GET['kieu']) && $_GET['kieu'] != '') {
	$cot = $_GET['cot'];
	$kieu = $_GET['kieu'];
	$orderby .= " ORDER BY $cot $kieu";
}

$page = (isset($_GET['page']) ? $_GET['page'] : 1);
$params = $_GET;
$limit = 5;
$offset = ($page-1)*$limit;
$baseURL = 'sinhvien.php';
$table = 'sinhvien';
$totalRecord = totalRecord($table,$where);

$sinhvien = getAll('*',$table,$where,$limit,$offset,$join,$orderby);
$link = link_sapxep($params);

// xóa theo checkbox
if(isset($_POST['delete']) || isset($_POST['active']) || isset($_POST['block'])) {
	if (!isset($_POST['check'])) {
		$_SESSION['flash_message'] = 'Bạn chưa chọn mục nào!';
	} else {
		$_SESSION['multi_del'] = $_POST['check'];
		header('location: delete.php');
	}
}

?>
<section>
	<div class="container danhsach">
		<h2 class="title">Thông tin Sinh viên</h2>
		<?php if($type_login=='admin'): ?>
			<div class="menu-small">
				<a href="sinhvien_create.php">Thêm sinh viên</a>
				<span class="message" style="padding-left:10%;">
					<?php if(isset($_SESSION['flash_message_ok'])) :?>
						<span style="color:blue;"><?php echo $_SESSION['flash_message_ok'];?></span>
					<?php endif; unset($_SESSION['flash_message_ok']);?>
					<?php if(isset($_SESSION['flash_message'])) :?>
						<span style="color:red;"><?php echo $_SESSION['flash_message'];?></span>
					<?php endif; unset($_SESSION['flash_message']);?>
				</span>
			</div>
		<?php endif; ?>
		<div class="tuychon clearfix cach-top">
			<?php if($type_login=='admin'):?>
				<span class="sapxep">
					Sắp xếp theo: 
					<select onchange="window.location.href=this.value" class="selecter1">
						<option value="<?php echo $link.'cot=masv&kieu=ASC';?>" <?php if(isset($_GET['cot']) && $_GET['cot']=='masv' && $_GET['kieu']=='ASC') {echo 'selected';} ?>>Mã SV tăng</option>
						<option value="<?php echo $link.'cot=masv&kieu=DESC';?>" <?php if(isset($_GET['cot']) && $_GET['cot']=='masv' && $_GET['kieu']=='DESC') {echo 'selected';} ?>>Mã SV giảm</option>

						<option value="<?php echo $link.'cot=hoten&kieu=ASC';?>" <?php if(isset($_GET['cot']) && $_GET['cot']=='hoten' && $_GET['kieu']=='ASC') {echo 'selected';} ?>>Tên SV tăng</option>
						<option value="<?php echo $link.'cot=hoten&kieu=DESC';?>" <?php if(isset($_GET['cot']) && $_GET['cot']=='hoten' && $_GET['kieu']=='DESC') {echo 'selected';} ?>>Tên SV giảm</option>

						<option value="<?php echo $link.'cot=ngaysinh&kieu=ASC';?>" <?php if(isset($_GET['cot']) && $_GET['cot']=='ngaysinh' && $_GET['kieu']=='ASC') {echo 'selected';} ?>>Ngày sinh tăng</option>
						<option value="<?php echo $link.'cot=ngaysinh&kieu=DESC';?>" <?php if(isset($_GET['cot']) && $_GET['cot']=='ngaysinh' && $_GET['kieu']=='DESC') {echo 'selected';} ?>>Ngày sinh giảm</option>

						<option value="<?php echo $link.'cot=makhoa&kieu=ASC';?>" <?php if(isset($_GET['cot']) && $_GET['cot']=='makhoa' && $_GET['kieu']=='ASC') {echo 'selected';} ?>>Mã khoa tăng</option>
						<option value="<?php echo $link.'cot=makhoa&kieu=DESC';?>" <?php if(isset($_GET['cot']) && $_GET['cot']=='makhoa' && $_GET['kieu']=='DESC') {echo 'selected';} ?>>Mã khoa giảm</option>
					</select>
				</span>
			<form action="" method="get">
				<span class="timkiem">
					Tìm kiếm: 
					<input type="text" name="masv" class="tukhoa" placeholder="mã sinh viên" value="<?php if (isset($_GET['masv']) && $_GET['masv']!='') {echo $_GET['masv'];}?>">
					<input type="text" name="hoten" class="tukhoa" placeholder="tên sinh viên" value="<?php if (isset($_GET['hoten']) && $_GET['hoten']!='') {echo $_GET['hoten'];}?>">
					<input type="text" name="ngaysinh" class="tukhoa" placeholder="yyyy-mm-dd" value="<?php if (isset($_GET['ngaysinh']) && $_GET['ngaysinh']!='') {echo $_GET['ngaysinh'];}?>">
					<input type="text" name="email" class="tukhoa" placeholder="email" value="<?php if (isset($_GET['email']) && $_GET['email']!='') {echo $_GET['email'];}?>">
					<select name="gioitinh" class="selecter">
						<option value="">giới tính</option>
						<option value="1" <?php if (isset($_GET['gioitinh']) && $_GET['gioitinh']=='1') {echo 'selected';}?>>nam</option>
						<option value="0" <?php if (isset($_GET['gioitinh']) && $_GET['gioitinh']=='0') {echo 'selected';}?>>nữ</option>
					</select>
					<select name="status" class="selecter">
						<option value="">trạng thái</option>
						<option value="1" <?php if (isset($_GET['status']) && $_GET['status']=='1') {echo 'selected';}?>>Active</option>
						<option value="0" <?php if (isset($_GET['status']) && $_GET['status']=='0') {echo 'selected';}?>>block</option>
					</select>
					<input type="submit" name="timkiem" class="submit" value="Tìm kiếm">
				</span>
			</form>
			<?php endif; ?>
		</div>
		<div class="table-center clearfix">
			<?php if($type_login=='admin'): ?>
				<form action="" method="post">
					<div class="menu-task">
						<input type="submit" name="active" value="kích hoạt">
						<input type="submit" name="block" value="vô hiệu">
						<input type="submit" name="delete" value="Xóa" onclick="return confirm('Bạn có muốn xóa các mục đã chọn?')">
					</div>
				<?php endif; ?>
				<table>
					<thead>
						<tr>
							<?php if($type_login=='admin'):?>
								<th>
									<input type="submit" name="checkall" value="All"><br>
									<input type="submit" name="uncheckall" value="UnAll">
								</th>
							<th>STT</th>
							<?php endif;?>
							<th>Mã SV</th>
							<th>Tên sinh viên</th>
							<th>Giới tính</th>
							<th>Ngày sinh</th>
							<th>Địa chỉ</th>
							<th>Email</th>
							<th>Khoa</th>
							<?php if($type_login=='admin'): ?>
								<th>Mật khẩu</th>
								<th>Trạng thái</th>
								<th colspan="3">Tùy chọn</th>
							<?php endif; ?>
						</tr>
					</thead>
					<tbody>
						<?php $stt=$offset; foreach ($sinhvien as $sv): ?>
							<tr>
								<?php if($type_login=='admin'): ?>
								<td class="center"><input class='checkbox' type="checkbox" name="check[<?php echo $sv['masv'];?>]" value="<?php echo $sv['masv'];?>" <?php if(isset($_POST['checkall'])) echo 'checked';if(isset($_POST['uncheckall'])) echo '';?>></td>
								<td class="center"><?php echo ++$stt; ?></td>
								<?php endif;?>

								<td class="center"><?php echo $sv['masv'];?></td>
								<td><?php echo $sv['hoten'];?></td>
								<td class="center"><?php if ($sv['gioitinh']=="1") echo "nam"; else echo "nữ";?></td>
								<td><?php echo $sv['ngaysinh'];?></td>
								<td><?php echo $sv['diachi'];?></td>
								<td><?php echo 'email';?></td>
								<td class="center"><?php echo $sv['makhoa'];?></td>
								<?php if($type_login=='admin'): ?>
									<td><?php echo $sv['password'];?></td>
									<?php if ($sv['status']=='1'): ?>
										<td style="color:green;"><?php echo 'Active';?></td> 
									<?php else: ?>
										<td style="color:red;"><?php echo 'block';?></td>
									<?php endif;?>
									<td class="tacvu"><a href="ketqua_update.php?sv&masv=<?php echo $sv['masv'];?>">Điểm</a></td>
									<td class="tacvu"><a href="sinhvien_edit.php?masv=<?php echo $sv['masv'];?>">sửa</a></td>
									<td class="tacvu"><a href="delete.php?masv=<?php echo $sv['masv'];?>" onClick="return confirm('Xác nhận xóa?')">xóa</a></td>
								<?php endif; ?>
							</tr>
							<?php
						endforeach;
						?>
					</tbody>
				</table>
				<?php if($type_login=='admin'):?></form><?php endif;?>
			</div>

			<div class="phantrang full-center clearfix">
				<?php echo paging($baseURL,$params,$totalRecord,$page,$limit);?>
			</div>
		</div>
	</section>


	<?php
	include('footer.php');
	?>

