<?php
require('connect.php');
require("header.php");

$sql = "SELECT * FROM khoa WHERE 1=1";

// tìm kiếm
if (isset($_GET['makhoa']) && $_GET['makhoa'] != '') {
	$sql .= " AND makhoa LIKE '%".$_GET['makhoa']."%'";
}
if (isset($_GET['ten_khoa']) && $_GET['ten_khoa'] != '') {
	$sql .= " AND ten_khoa LIKE '%".$_GET['ten_khoa']."%'";
}
// xắp sếp
if (isset($_GET['orderby']) && $_GET['orderby'] != '') {
	$cot = @array_shift(explode('-', $_GET['orderby']));
	$kieu = @array_pop(explode('-', $_GET['orderby']));
	$sql .= " ORDER BY $cot $kieu";
}

$query = $db->query($sql);
$result = $query->fetch_all(MYSQLI_ASSOC);

?>
<section>
	<div class="container danhsach">
		<h2 class="title">Danh sách khoa</h2>
		<?php if($type_login=='admin'): ?>
		<div class="menu-small">
			<a href="khoa_create.php">Thêm khoa</a>
			<span class="message" style="padding-left:10%;">
				<?php if(isset($_SESSION['flash_message_ok'])) :?>
					<span style="color:blue;"><?php echo $_SESSION['flash_message_ok'];?></span>
				<?php endif; unset($_SESSION['flash_message_ok']);?>
				<?php if(isset($_SESSION['flash_message'])) :?>
					<span style="color:red;"><?php echo $_SESSION['flash_message'];?></span>
				<?php endif; unset($_SESSION['flash_message']);?>
			</span>
		</div>
		<?php endif; ?>
		<div class="tuychon clearfix cach-top">
			<form action="" method="get">
				<span class="sapxep">
					Sắp xếp theo: 
					<select name="orderby" class="selecter1">
						<option value="makhoa-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='makhoa-ASC') {echo 'selected';}?>>Mã khoa tăng</option>
						<option value="makhoa-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='makhoa-DESC') {echo 'selected';}?>>Mã khoa giảm</option>
						<option value="ten_khoa-ASC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='ten_khoa-ASC') {echo 'selected';}?>>Tên khoa tăng</option>
						<option value="ten_khoa-DESC" <?php if (isset($_GET['orderby']) && $_GET['orderby']=='ten_khoa-DESC') {echo 'selected';}?>>Tên khoa giảm</option>
					</select>
					<input type="submit" name="sapxep" value="Sắp xếp" class="submit">
				</span>
				<span class="timkiem">
					Tìm kiếm: 
					<input type="text" name="makhoa" class="tukhoa" placeholder="mã khoa" value="<?php if (isset($_GET['makhoa']) && $_GET['makhoa']!='') {echo $_GET['makhoa'];}?>">
					<input type="text" name="ten_khoa" class="tukhoa" placeholder="tên khoa" value="<?php if (isset($_GET['ten_khoa']) && $_GET['ten_khoa']!='') {echo $_GET['ten_khoa'];}?>">
					<input type="submit" name="timkiem" class="submit" value="Tìm kiếm">
				</span>
			</form>
		</div>
		<div class="table-center clearfix">
			<table>
				<thead>
					<tr>
						<th>STT</th>
						<th>Mã khoa</th>
						<th>Tên khoa</th>
						<?php if($type_login=='admin'): ?>
							<th colspan="2">Tùy chọn</th>
						<?php endif; ?>
					</tr>
				</thead>
				<tbody>
					<?php 
					$stt=0;
					if (count($result) > 0) :
						foreach ($result as $sv) :
							?>
							<tr>
								<td class="center"><?php echo ++$stt;?></td>
								<td class="center"><?php echo $sv['makhoa'];?></td>
								<td><?php echo $sv['ten_khoa'];?></td>
								<?php if ($type_login=='admin'): ?>
									<td class="tacvu"><a href="khoa_edit.php?makhoa=<?php echo $sv['makhoa'];?>">sửa</a></td>
									<td class="tacvu"><a onclick="return confirm('Xác nhận xóa?');" href="delete.php?makhoa=<?php echo $sv['makhoa'];?>">xóa</a></td>
								<?php endif; ?>
							</tr>
							<?php
						endforeach;
					endif; 
					?>
				</tbody>
			</table>
		</div>
	</div>
</section>

<?php
include('footer.php');
?>