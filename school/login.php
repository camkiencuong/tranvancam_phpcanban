<?php
session_start();
if (isset($_SESSION['user'])) {
	header('location: index.php');
}
require('connect.php');
$error=[];
$sql='';
if(isset($_POST['submit'])) {
	if (isset($_POST['masv']) && trim($_POST['masv']=='')) {
		$error[] = 'Vui lòng nhập mã sinh viên!';
	}
	if (isset($_POST['password']) && trim($_POST['password']=='')) {
		$error[] = 'Vui lòng nhập mật khẩu!';
	} elseif (strlen(trim($_POST['password'])) < 6) {
		$error[] = 'Mật khẩu phải từ 6 ký tự trở lên!';
	}
	if (count($error) == 0) {
		$sql .= 'SELECT * FROM sinhvien WHERE masv="'.trim($_POST['masv']).'"';
		$query = $db->query($sql);
		$user = $query->fetch_assoc();
		if (is_null($user)) {
			$error[] = 'Mã sinh viên không tồn tại!';
		} elseif ($user['password'] != trim($_POST['password'])) {
			$error[] = 'Mật khẩu không đúng!';
		} elseif ($user['status']=='0') {
			$error[] = 'Tài khoản đã bị khóa!';
		} else {
			$_SESSION['user'] = $user;
			$_SESSION['flash_message_ok'] = 'Đăng nhập thành công!';
			header('location: ketqua_diem.php?cur=5');
			exit;
		}
	}
}
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>My School</title>
	<link href="http://mta.edu.vn//Portals/0/favicon.ico" rel="shortcut icon">
	<link rel="stylesheet" type="text/css" href="css/style.css">
</head>
<body>
	<header>
		<div class="container">
			<div class="logo">
				<a href="index.php"><img src="img/logo.png"></a>
			</div>
			<div class="sitename">
				<h1>My School HVKTQS</h1>
			</div>
			<div class="menu clearfix">
				<ul>
					<li></li>
				</ul>
			</div>
		</div>
	</header>
	<section class="container themmoi clearfix">
		<div><h2 class="title">Đăng Nhập</h2></div>
		<div class="form full-center">
			<form action="" method="post">
				<?php if (count($error) > 0):
				for ($i=0; $i < count($error); $i++): ?>
				<p style="color:red;"><?php echo $error[$i]; ?></p>
			<?php endfor;
			endif; ?>
			<table>
				<tr>
					<td>Mã sinh viên</td>
					<td class="chuoi"><input type="text" name="masv" placeholder="mã sinh viên" value="<?php if(isset($_POST['masv']) && $_POST['masv'] != '') echo trim($_POST['masv']); ?>"></td>
				</tr>
				<tr>
					<td>Mật khẩu</td>
					<td class="chuoi"><input type="password" name="password" placeholder="password" value="<?php if(isset($_POST['password']) && $_POST['password'] != '') echo trim($_POST['password']); ?>"></td>
				</tr>
				<tr>
					<td></td>
					<td>
						<input type="submit" name="submit" class="submit" value="Đăng Nhập">
					</td>
				</tr>
			</table>
		</form>
	</div>
</section>
<?php include('footer.php');?>
