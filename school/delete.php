<?php
session_start();
if (!isset($_SESSION['user'])) {
	header('location: login.php');
}
if (!isset($_SESSION['user']['is_admin'])) {
	header('location: index.php');
}
require('connect.php');

// xóa nhiều sinh viên
if (isset($_SESSION['multi_del'])) {
	// xóa kết quả của các sinh viên đã chọn
	$sql_diem = 'DELETE FROM ketqua WHERE 1=0';
	$sql_sv = 'DELETE FROM sinhvien WHERE 1=0';
	$multi_del = $_SESSION['multi_del'];
	foreach ($multi_del as $del) {
		$sql_diem .= ' OR masv="'.$del.'"';
		$sql_sv .= ' OR masv="'.$del.'"';
	}
	$query = $db->query($sql_diem);
	if (!$query) {
		$_SESSION['flash_message'] = 'Không thể xóa điểm của các sinh viên!';
		header('location: sinhvien.php');
	}
	$query = $db->query($sql_sv);
	if (!$query) {
		$_SESSION['flash_message'] = 'Không thể xóa các sinh viên!';
		header('location: sinhvien.php');
	} else {
		$_SESSION['flash_message_ok'] = 'Xóa nhiều sinh viên thành công!';
		header('location: sinhvien.php');
	}
	exit;
}

// xóa 1 sinh viên
if (isset($_GET['masv'])) {
	$masv = $_GET['masv'];
	$sql = "SELECT * FROM sinhvien WHERE masv='{$masv}'";
	$query = $db->query($sql);
	$sinhvien = $query->fetch_assoc();
	if (is_null($sinhvien)) {
		$_SESSION['flash_message'] = 'Sinh viên không tồn tại';
		header('location: sinhvien.php');
	} else {
		$sql = "DELETE FROM ketqua WHERE masv='{$masv}'";
		$query = $db->query($sql);
		if (!$query) {
			$_SESSION['flash_message'] = "Lỗi, không thể xóa điểm $masv!";
			header("location: sinhvien.php");
		}
		$sql = "DELETE FROM sinhvien WHERE masv='{$masv}'";
		$query = $db->query($sql);
		if ($query) {
			$_SESSION['flash_message_ok'] = "Xóa sinh viên $masv thành công!";
			header('location: sinhvien.php');
		} else {
			$_SESSION['flash_message'] = "Lỗi, không thể xóa sinh viên $masv!";
			header("location: sinhvien.php");
		}
	}
	exit;
}

// xóa khoa
if (isset($_GET['makhoa'])) {
	$makhoa = $_GET['makhoa'];
	$sql = "SELECT * FROM khoa WHERE makhoa='{$makhoa}'";
	$query = $db->query($sql);
	$khoa = $query->fetch_assoc();
	if (is_null($khoa)) {
		$_SESSION['flash_message'] = 'Khoa không tồn tại';
		header('location: khoa.php');
	} else {
		$sql = "DELETE FROM ketqua WHERE masv IN (SELECT masv FROM khoa WHERE makhoa='{$makhoa}')";
		$query = $db->query($sql);
		if (!$query) {
			$_SESSION['flash_message'] = "Lỗi, không thể xóa điểm của SV trong khoa $makhoa!";
			header("location: khoa.php");
		}
		$sql = "DELETE FROM sinhvien WHERE makhoa='{$makhoa}'";
		$query = $db->query($sql);
		if (!$query) {
			$_SESSION['flash_message'] = "Lỗi, không thể xóa SV trong khoa $makhoa!";
			header("location: khoa.php");
		}
		$sql = "DELETE FROM khoa WHERE makhoa='{$makhoa}'";
		$query = $db->query($sql);
		if ($query) {
			$_SESSION['flash_message_ok'] = "Xóa khoa $makhoa thành công!";
			header('location: khoa.php');
		} else {
			$_SESSION['flash_message'] = "Lỗi, không thể xóa sinh viên $makhoa!";
			header("location: khoa.php");
		}
	}
	exit;
}

// xóa môn học
if (isset($_GET['mamonhoc'])) {
	$mamonhoc = $_GET['mamonhoc'];
	$sql = "SELECT * FROM mon_hoc WHERE mamonhoc='{$mamonhoc}'";
	$query = $db->query($sql);
	$monhoc = $query->fetch_assoc();
	if (is_null($monhoc)) {
		$_SESSION['flash_message'] = 'Môn học không tồn tại';
		header('location: monhoc.php');
	} else {
		$sql = "DELETE FROM ketqua WHERE mamonhoc='{$mamonhoc}'";
		$query = $db->query($sql);
		if (!$query) {
			$_SESSION['flash_message'] = "Lỗi, không thể xóa điểm của môn $mamonhoc!";
			header("location: monhoc.php");
		}
		$sql = "DELETE FROM mon_hoc WHERE mamonhoc='{$mamonhoc}'";
		$query = $db->query($sql);
		if ($query) {
			$_SESSION['flash_message_ok'] = "Xóa môn học $mamonhoc thành công!";
			header('location: monhoc.php');
		} else {
			$_SESSION['flash_message'] = "Lỗi, không thể xóa môn $mamonhoc!";
			header("location: monhoc.php");
		}
	}
	exit;
}

// xóa điểm 
if (isset($_GET['madiem']) && isset($_GET['mamon'])) {
	$masv = $_GET['madiem'];
	$mamonhoc = $_GET['mamon'];
	$sql = "SELECT * FROM ketqua WHERE masv='{$masv}' AND mamonhoc='{$mamonhoc}'";
	$query = $db->query($sql);
	$ketqua = $query->fetch_assoc();
	if (is_null($ketqua)) {
		$_SESSION['flash_message'] = 'Điểm không tồn tại';
		header('location: ketqua_diem.php');
	} else {
		$sql = "DELETE FROM ketqua WHERE masv='{$masv}' AND mamonhoc='{$mamonhoc}'";
		$query = $db->query($sql);
		if ($query) {
			$_SESSION['flash_message_ok'] = "Xóa điểm $masv & $mamonhoc thành công!";
			header('location: ketqua_diem.php');
		} else {
			$_SESSION['flash_message'] = "Lỗi, không thể xóa điểm $masv & $mamonhoc!";
			header("location: ketqua_diem.php");
		}
	}
	exit;
}

// không xóa gì
header('location: index.php');
?>


