<?php 
require('connect.php');
require('header.php');

if (!isset($_SESSION['user']['is_admin'])) {
	header('location: ketqua_diem.php?cur=5');
}
?>
<section class="content clearfix">
	<div class="container danhsach">
		<h2 class="title">Tổng quan</h2>
		<div>
			<span class="message">
				<?php if(isset($_SESSION['flash_message_ok'])) :?>
					<span style="color:blue;"><?php echo $_SESSION['flash_message_ok'];?></span>
				<?php endif; unset($_SESSION['flash_message_ok']);?>
				<?php if(isset($_SESSION['flash_message'])) :?>
					<span style="color:red;"><?php echo $_SESSION['flash_message'];?></span>
				<?php endif; unset($_SESSION['flash_message']);?>
			</span>
		</div>
		<div class="table-center clearfix">
			<table>
				<thead>
					<tr class="tieude">
						<th><a href="khoa.php">Tổng khoa</a></th>
						<th><a href="monhoc.php">Tổng môn học</a></th>
						<th><a href="sinhvien.php">Tổng sinh viên</a></th>
						<th><a href="ketqua_diem.php">Bảng điểm</a></th>
						<th><a href="#">Admins</a></th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td class="center"><?php
						// Đếm số khoa
						$sql = "SELECT COUNT(*) FROM khoa";
						$query = $db->query($sql);
						$result = $query->fetch_row();
						echo $result[0];
						?></td>
						<td class="center"><?php
						// Đếm số môn học
						$sql = "SELECT COUNT(*) FROM mon_hoc";
						$query = $db->query($sql);
						$result = $query->fetch_row();
						echo $result[0];
						?></td>
						<td class="center"><?php
						// Đếm số sinh viên
						$sql = "SELECT COUNT(*) FROM sinhvien";
						$query = $db->query($sql);
						$result = $query->fetch_row();
						echo $result[0];
						?></td>
						<td class="center"><?php
						// Đếm số điểm
						$sql = "SELECT COUNT(*) FROM ketqua";
						$query = $db->query($sql);
						$result = $query->fetch_row();
						echo $result[0];
						?></td>
						<td class="center"><?php
						$sql = 'SELECT COUNT(*) FROM admins';
						$query = $db->query($sql);
						$result = $query->fetch_row();
						echo $result[0];
						?></td>
					</tr>
				</tbody>
			</table>
		</div>
	</div>
</section>

<?php
include('footer.php');
?>

