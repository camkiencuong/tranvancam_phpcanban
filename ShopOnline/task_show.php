<?php

$page = (isset($_GET['page']) ? $_GET['page'] : 1);
$where = $join = $orderby = '';
$error = [];

$limit = (isset($_GET['limit']) ? $_GET['limit'] : 10);
$offset = ($page-1)*$limit;

if (isset($_GET['id_del']) && $_GET['id_del'] != '') {
	$id_get =  (int)$_GET['id_del'];
	$testId = $object->index('*',"AND id=$id_get",10,0,'','',0);
	if (is_null($testId)) {
		$_SESSION['flash_msg'] = 'ID không tồn tại!';
		header("location: $baseURL"); exit;
	}
	$_SESSION['flash_msg'] = $object->delete([$id_get]);
	header("location: $baseURL"); exit;
}

if (isset($_POST['multi_del'])) {
	if (!isset($_POST['checkbox'])) {
		$error[] = 'Bạn chưa chọn mục nào';
	} else {
		$multi_del = $_POST['checkbox'];
		$_SESSION['flash_msg'] = $object->delete($multi_del);
		header("location: $baseURL"); exit;
	}
}


if (isset($_POST['multi_edit'])) {
	if (!isset($_POST['checkbox'])) {
		$error[] = 'Bạn chưa chọn mục nào';
	} else {
		$id_array = $_POST['checkbox'];
		$dieukien = ' AND id in ('.implode(',', $id_array).')';
		$_SESSION['id_edit'] = $object->index('*',$dieukien,$limit);
		header("location: $baseURL?act=edit");exit;
	}
}

if (isset($_POST['active'])) {
	if (!isset($_POST['checkbox'])) {
		$error[] = 'Bạn chưa chọn mục nào';
	} else {
		$id_array = $_POST['checkbox'];
		for ($i=0; $i < count($id_array); $i++) { 
			$dieukien[] = 'id="'.$id_array[$i].'"';
			$status[] = 'status=1';
			$error = $object->update($status,$dieukien);
		}
	}
}

if (isset($_POST['block'])) {
	if (!isset($_POST['checkbox'])) {
		$error[] = 'Bạn chưa chọn mục nào';
	} else {
		$id_array = $_POST['checkbox'];
		for ($i=0; $i < count($id_array); $i++) { 
			$dieukien[] = 'id="'.$id_array[$i].'"';
			$status[] = 'status=0';
			$error = $object->update($status,$dieukien);
		}
	}
}
// bảng product_categories
if (isset($_POST['parent'])) {
	if (!isset($_POST['checkbox'])) {
		$error[] = 'Bạn chưa chọn mục nào';
	} else {
		$id_array = $_POST['checkbox'];
		if (in_array($_POST['parent_id'], $id_array)) {
			$error[] = 'Không thể chọn danh mục cha trong các mục đã chọn';
		} else {
			for ($i=0; $i < count($id_array); $i++) { 
				$dieukien[] = 'id="'.$id_array[$i].'"';
				$sets[] = "parent_id='".$_POST['parent_id']."'";
				$error = $object->update($sets,$dieukien);
			}
		}
	}
}
// bảng product_categories
if (isset($_POST['product_img'])) {
	if (!isset($_POST['checkbox'])) {
		$error[] = 'Bạn chưa chọn mục nào';
	} else {
		$id_array = $_POST['checkbox'];
		$dieukien = ' AND id in ('.implode(',', $id_array).')';
		$_SESSION['id_edit'] = $object->index('*',$dieukien,$limit);
		header("location: product_images.php?act=edit");exit;
	}
}
// bảng products
if (isset($_POST['brand'])) {
	if (!isset($_POST['checkbox'])) {
		$error[] = 'Bạn chưa chọn mục nào';
	} else {
		$id_array = $_POST['checkbox'];
		for ($i=0; $i < count($id_array); $i++) { 
			$dieukien[] = 'id="'.$id_array[$i].'"';
			$sets[] = "brand_id='".$_POST['brand_id']."'";
			$error = $object->update($sets,$dieukien);
		}
	}
}
// bảng products
if (isset($_POST['category'])) {
	if (!isset($_POST['checkbox'])) {
		$error[] = 'Bạn chưa chọn mục nào';
	} else {
		$id_array = $_POST['checkbox'];
		for ($i=0; $i < count($id_array); $i++) { 
			$dieukien[] = 'id="'.$id_array[$i].'"';
			$sets[] = "product_category_id='".$_POST['product_category_id']."'";
			$error = $object->update($sets,$dieukien);
		}
	}
}

if (isset($_GET['find'])) {
	$key = $_GET['key'];
	for ($i=0; $i < count($tableCols); $i++) { 
		$where .= ($i==0 ? " AND $tableCols[$i] LIKE '%$key%'" : " OR $tableCols[$i] LIKE '%$key%'");
	}
}
if (isset($_GET['col']) && isset($_GET['by'])) {
	$col = $_GET['col']; $by = $_GET['by'];
	$orderby = " ORDER BY $col $by";
}
$param = $_GET;
$queryLimit = queryString($param,['limit','page']);
$queryBy = queryString($param,['col','by']);
$queryImg = queryString($param,['view','id','img']);
$queryString = queryString($param,'page');