<?php
session_start();
require('connect.php');
require('functions.php');
require('page_header.php');

?>

<section class="tongquan">
	<div class="namepage clear">
		<div class="container">
			<h1>Tổng Quan Shop</h1>
		</div>
	</div>
	<div class="shop-show">
		<div class="container">
			<table>
				<tr>
					<td class="title"><a href="product_categories.php">Danh mục SP</a></td>
					<td class="sl"><?php $table='product_categories';echo totalRecord($table); ?></td>
					<td></td>
					<td class="title"><a href="products.php">Sản phẩm</a></td>
					<td class="sl"><?php $table='products';echo totalRecord($table); ?></td>
				</tr>
				<tr>
					<td class="title"><a href="product_images.php">Hình ảnh SP</a></td>
					<td class="sl"><?php $table='product_images';echo totalRecord($table); ?></td>
					<td></td>
					<td class="title"><a href="product_relates.php">SP trên quan</a></td>
					<td class="sl"><?php $table='product_relates';echo totalRecord($table); ?></td>
				</tr>
				<tr>
					<td class="title"><a href="brands.php">Thương hiệu</a></td>
					<td class="sl"><?php $table='brands';echo totalRecord($table); ?></td>
					<td></td>
					<td class="title"><a href="user_groups.php">Nhóm Thành viên</a></td>
					<td class="sl"><?php $table='user_groups';echo totalRecord($table); ?></td>
				</tr>
				<tr>
					<td class="title"><a href="users.php">Thành viên</a></td>
					<td class="sl"><?php $table='users';echo totalRecord($table); ?></td>
					<td></td>
					<td class="title"><a href="admins.php">Quản trị</a></td>
					<td class="sl"><?php $table='admins';echo totalRecord($table); ?></td>
				</tr>
				<tr>
					<td class="title"><a href="orders.php">Đơn hàng</a></td>
					<td class="sl"><?php $table='orders';echo totalRecord($table); ?></td>
					<td></td>
					<td class="title"><a href="coupons.php">Khuyến mãi</a></td>
					<td class="sl"><?php $table='coupons';echo totalRecord($table); ?></td>
				</tr>
				<tr>
					<td class="title"><a href="reviews.php">Đánh giá</a></td>
					<td class="sl"><?php $table='reviews';echo totalRecord($table); ?></td>
					<td></td>
					<td class="title"><a href="post_categories.php">Danh mục tin</a></td>
					<td class="sl"><?php $table='post_categories';echo totalRecord($table); ?></td>
				</tr>
				<tr>
					<td class="title"><a href="posts.php">Tin tức</a></td>
					<td class="sl"><?php $table='posts';echo totalRecord($table); ?></td>
					<td></td>
					<td class="title"><a href="comments.php">Bình luận</a></td>
					<td class="sl"><?php $table='comments';echo totalRecord($table); ?></td>
				</tr>
				<tr>
					<td class="title"><a href="banners.php">Quảng cáo</a></td>
					<td class="sl"><?php $table='banners';echo totalRecord($table); ?></td>
					<td></td>
					<td class="title"><a href="pages.php">Trang tĩnh</a></td>
					<td class="sl"><?php $table='pages';echo totalRecord($table); ?></td>
				</tr>
				<tr>
					<td class="title"><a href="provinces.php">Tỉnh/Thành phố</a></td>
					<td class="sl"><?php $table='provinces';echo totalRecord($table); ?></td>
					<td></td>
					<td class="title"><a href="districts.php">Quận/Huyện</a></td>
					<td class="sl"><?php $table='districts';echo totalRecord($table); ?></td>
				</tr>
			</table>
		</div>
	</div>
</section>
<?php
require('page_footer.php');
?>