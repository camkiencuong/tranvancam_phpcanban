<?php

class ProductCategory {
	
	private $table = 'product_categories';
	public $id;
	public $name;
	public $slug;
	public $image;
	public $description;
	public $parent_id;
	public $meta_title;
	public $meta_keyword;
	public $meta_description;
	public $status;

	public function index($field='*',$where='',$limit=100,$offset=0,$join='',$orderby='',$all='1')
	{
		global $db;
		$sql = sprintf("SELECT %s FROM %s %s WHERE 1=1 %s %s LIMIT %d OFFSET %d ", $field, $this->table, $join, $where, $orderby, $limit, $offset);
		$query = $db->query($sql);
		if (!$query) {
			return "sql lỗi! $sql";
		}
		return $all==1 ? $query->fetch_all(MYSQLI_ASSOC) : $query->fetch_assoc();
	}

	public function create($values=[])
	{
		global $db;
		$cols = 'id, name, slug, image, description, parent_id, meta_title, meta_keyword, meta_description, status';
		//$values = "$id'','$name','$slug','$image','$description','$parent_id','$meta_title','$meta_keyword','$meta_description','$status'";
		$error = [];
		for ($i=0; $i < count($values); $i++) { 
			$sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $this->table, $cols, $values[$i]);
			$query = $db->query($sql);
			if (!$query) {$error[] = 'Thêm lỗi! '.$sql;}
		}
		return count($error)==0 ? null : $error;
	}

	public function update($set=[], $where=[])
	{
		global $db;
		$error = [];
		if (count($set)!=count($where)) {return 'Lỗi nạp dữ liệu';}

		for ($i=0; $i < count($set); $i++) { 
			$sql = sprintf("UPDATE %s SET %s WHERE %s", $this->table, $set[$i], $where[$i]);
			$query = $db->query($sql);
			if (!$query) {$error[] = 'Cập Nhật lỗi! '.$sql;}
		}
		return count($error)==0 ? null : $error;
		
	}
	
	
	public function delete($id_array=[])
	{
		global $db;
		$relate = ['product_images', 'product_relates', 'reviews', 'order_items'];
		$id_string = implode(', ', $id_array);

		// xóa ở các bảng liên quan với sản phẩm
		for ($i=0; $i < count($relate); $i++) { 
			$sql = sprintf("DELETE FROM %s WHERE product_id in (SELECT id FROM products WHERE product_category_id in (%s))", $relate[$i], $id_string);
			$query = $db->query($sql);
			if (!$query) {return "Xóa lỗi ở $relate[$i]! $sql";}
		}
		// xóa ở bảng sản phẩm
		$sql = sprintf("DELETE FROM products WHERE product_category_id in (%s)", $id_string);
		$query = $db->query($sql);
		if (!$query) {return "Xóa lỗi ở products! $sql";}
		
		// xóa ở bảng danh mục
		$sql = sprintf("DELETE FROM %s WHERE id in (%s)", $this->table, $id_string);
		$query = $db->query($sql);
		
		return $query ? 'Xóa Thành công' : "Xóa lỗi ở product_categories! $sql";
	}

	public function status($status='0')
	{
		if ($status == 0) {return 'Block';}
		if ($status == 1) {return 'Active';}
		return '-';
	}

	public function parent_id($parent_id='0')
	{
		$where = "AND id=$parent_id";
		$name = $this->index('name',$where,1,0,'','',0);
		return count($name)=='0' ? 'không' : $name['name'];
	}
	
	public function totalRecord($where='')
	{
		global $db;
		$sql = sprintf("SELECT COUNT(*) FROM %s WHERE 1=1 %s", $this->table, $where);
		$query = $db->query($sql);
		if (!$query) {
			return "sql lỗi! $sql";
		}
		$result = $query->fetch_row();
		return is_null($result) ? 0 : $result[0];
	}

}

