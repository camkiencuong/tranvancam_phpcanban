<?php

class ProductImage {
	
	private $table = 'product_images';
	public $id;
	public $product_id;
	public $image;
	public $is_featured;

	public function index($field='*',$where='',$limit=100,$offset=0,$join='',$orderby='',$all='1')
	{
		global $db;
		$sql = sprintf("SELECT %s FROM %s %s WHERE 1=1 %s %s LIMIT %d OFFSET %d ", $field, $this->table, $join, $where, $orderby, $limit, $offset);
		$query = $db->query($sql);
		if (!$query) {
			return "sql lỗi! $sql";
		}
		return $all==1 ? $query->fetch_all(MYSQLI_ASSOC) : $query->fetch_assoc();
	}

	public function create($values=[])
	{
		global $db;
		$cols = 'id, product_id, image, is_featured';
		$error = [];
		for ($i=0; $i < count($values); $i++) { 
			$sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $this->table, $cols, $values[$i]);
			$query = $db->query($sql);
			if (!$query) {$error[] = 'Thêm lỗi! '.$sql;}
		}
		return count($error)==0 ? null : $error;
	}

	public function update($set=[], $where=[])
	{
		global $db;
		$error = [];

		if (count($set)!=count($where)) {return 'Lỗi nạp dữ liệu';}

		for ($i=0; $i < count($set); $i++) { 
			$sql = sprintf("UPDATE %s SET %s WHERE %s", $this->table, $set[$i], $where[$i]);
			$query = $db->query($sql);
			if (!$query) {$error[] = 'Cập Nhật lỗi! '.$sql;}
		}
		return count($error)==0 ? null : $error;
		
	}
	
	public function delete($id_array=[])
	{
		global $db;
		$id_string = implode(', ', $id_array);
		$sql = sprintf("DELETE FROM product_images WHERE id in (%s)", $id_string);
		$query = $db->query($sql);		
		return $query ? 'Xóa Thành công' : "Xóa lỗi ở product_images! $sql";
	}

	public function totalRecord($where='')
	{
		global $db;
		$sql = sprintf("SELECT COUNT(*) FROM %s WHERE 1=1 %s", $this->table, $where);
		$query = $db->query($sql);
		if (!$query) {
			return "sql lỗi! $sql";
		}
		$result = $query->fetch_row();
		return is_null($result) ? 0 : $result[0];
	}

	public function isYes($is='0')
	{
		return $is=='1' ? 'yes' : 'no';
	}

	public function getData($field='*',$table,$where='',$all='1')
	{
		global $db;
		$sql = sprintf("SELECT %s FROM %s WHERE 1=1 %s", $field, $table, $where);
		$query = $db->query($sql);
		if (!$query) {
			return "sql lỗi! $sql";
		}
		return $all==1 ? $query->fetch_all(MYSQLI_ASSOC) : $query->fetch_assoc();
	}

	public function productName($product_id='0')
	{
		$where = "AND id=$product_id";
		$table = 'products';
		$name = $this->getData('name',$table,$where,0);
		if (!isset($name['name'])) {
			return "lỗi function!";
		}
		return $product_id=='0' ? 'trống' : $name['name'];
	}

	public function brandName($brand_id='0')
	{
		$where = "AND id=$brand_id";
		$table = 'brands';
		$name = $this->getData('name',$table,$where,0);
		if (!isset($name['name'])) {
			return "lỗi function!";
		}
		return $brand_id=='0' ? 'trống' : $name['name'];
	}
	
	public function categoryName($category_id='0')
	{
		$where = "AND id=$category_id";
		$table = 'product_categories';
		$name = $this->getData('name',$table,$where,0);
		if (!isset($name['name'])) {
			return "lỗi function!";
		}
		return $category_id=='0' ? 'trống' : $name['name'];
	}

}

