<?php

class User {
	
	private $table = 'Users';
	public $id;
	public $fullname;
	public $gender;
	public $birthdate;
	public $email;
	public $phone;
	public $province_id;
	public $district_id;
	public $address;
	public $password;
	public $created_at;
	public $update_at;
	public $user_group_id;
	public $status;

	public function index($field='*',$where='',$limit=100,$offset=0,$join='',$orderby='',$all='1')
	{
		global $db;
		$sql = sprintf("SELECT %s FROM %s %s WHERE 1=1 %s %s LIMIT %d OFFSET %d ", $field, $this->table, $join, $where, $orderby, $limit, $offset);
		$query = $db->query($sql);
		if (!$query) {
			return "sql lỗi! $sql";
		}
		return $all==1 ? $query->fetch_all(MYSQLI_ASSOC) : $query->fetch_assoc();
	}

	public function create($values=[])
	{
		global $db;
		$cols = 'id, fullname, gender, birthdate, email, phone, province_id, district_id, address, password, created_at, update_at, user_group_id, status';
		$error = [];
		for ($i=0; $i < count($values); $i++) { 
			$sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $this->table, $cols, $values[$i]);
			$query = $db->query($sql);
			if (!$query) {$error[] = 'Thêm lỗi! '.$sql;}
		}
		return count($error)==0 ? null : $error;
	}

	public function update($set=[], $where=[])
	{
		global $db;
		$error = [];

		if (count($set)!=count($where)) {return 'Lỗi nạp dữ liệu';}

		for ($i=0; $i < count($set); $i++) { 
			$sql = sprintf("UPDATE %s SET %s WHERE %s", $this->table, $set[$i], $where[$i]);
			$query = $db->query($sql);
			if (!$query) {$error[] = 'Cập Nhật lỗi! '.$sql;}
		}
		return count($error)==0 ? null : $error;
		
	}
	
	public function delete($id_array=[])
	{
		global $db;
		$relate = ['product_images', 'product_relates', 'reviews', 'order_items'];
		$id_string = implode(', ', $id_array);

		// xóa ở các bảng liên quan với sản phẩm
		for ($i=0; $i < count($relate); $i++) { 
			$sql = sprintf("DELETE FROM %s WHERE product_id in (%s)", $relate[$i], $id_string);
			$query = $db->query($sql);
			if (!$query) {return "Xóa lỗi ở $relate[$i]! $sql";}
		}
		// xóa ở bảng sản phẩm
		$sql = sprintf("DELETE FROM products WHERE id in (%s)", $id_string);
		$query = $db->query($sql);		
		return $query ? 'Xóa Thành công' : "Xóa lỗi ở products! $sql";
	}

	public function status($status='0')
	{
		if ($status == 0) {return 'Block';}
		if ($status == 1) {return 'Active';}
		return '-';
	}

	public function totalRecord($where='')
	{
		global $db;
		$sql = sprintf("SELECT COUNT(*) FROM %s WHERE 1=1 %s", $this->table, $where);
		$query = $db->query($sql);
		if (!$query) {
			return "sql lỗi! $sql";
		}
		$result = $query->fetch_row();
		return is_null($result) ? 0 : $result[0];
	}

	public function getData($field='*',$table,$where='',$all='1')
	{
		global $db;
		$sql = sprintf("SELECT %s FROM %s WHERE 1=1 %s", $field, $table, $where);
		$query = $db->query($sql);
		if (!$query) {
			return "sql lỗi! $sql";
		}
		return $all==1 ? $query->fetch_all(MYSQLI_ASSOC) : $query->fetch_assoc();
	}

	public function isYes($is='0')
	{
		return $is=='1' ? 'yes' : '-';
	}

}

