<?php

class UserGroup {
	
	private $table = 'user_groups';
	public $id;
	public $name;
	public $slug;
	public $logo;
	public $image;
	public $description;
	public $meta_title;
	public $meta_keyword;
	public $meta_description;
	public $status;
	public function index($field='*',$where='',$limit=100,$offset=0,$join='',$orderby='',$all='1')
	{
		global $db;
		$sql = sprintf("SELECT %s FROM %s %s WHERE 1=1 %s %s LIMIT %d OFFSET %d ", $field, $this->table, $join, $where, $orderby, $limit, $offset);
		$query = $db->query($sql);
		if (!$query) {
			return "sql lỗi! $sql";
		}
		return $all==1 ? $query->fetch_all(MYSQLI_ASSOC) : $query->fetch_assoc();
	}

	public function create($values=[])
	{
		global $db;
		$cols = 'id, name, type, amount';
		$error = [];
		for ($i=0; $i < count($values); $i++) { 
			$sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $this->table, $cols, $values[$i]);
			$query = $db->query($sql);
			if (!$query) {$error[] = 'Thêm lỗi! '.$sql;}
		}
		return count($error)==0 ? null : $error;
	}

	public function update($set=[], $where=[])
	{
		global $db;
		$error = [];
		if (count($set)!=count($where)) {return 'Lỗi nạp dữ liệu';}

		for ($i=0; $i < count($set); $i++) { 
			$sql = sprintf("UPDATE %s SET %s WHERE %s", $this->table, $set[$i], $where[$i]);
			$query = $db->query($sql);
			if (!$query) {$error[] = 'Cập Nhật lỗi! '.$sql;}
		}
		return count($error)==0 ? null : $error;
		
	}
	
	
	public function delete($id_array=[])
	{
		global $db;
		$relate = ['comments', 'orders', 'reviews'];
		$id_string = implode(', ', $id_array);

		// xóa ở các bảng liên quan với user
		for ($i=0; $i < count($relate); $i++) { 
			$sql = sprintf("DELETE FROM %s WHERE product_id in (SELECT id FROM products WHERE user_id in (%s))", $relate[$i], $id_string);
			$query = $db->query($sql);
			if (!$query) {return "Xóa lỗi ở $relate[$i]! $sql";}
		}
		// xóa ở bảng user
		$sql = sprintf("DELETE FROM products WHERE user_id in (%s)", $id_string);
		$query = $db->query($sql);
		if (!$query) {return "Xóa lỗi ở products! $sql";}
		
		// xóa ở bảng nhóm
		$sql = sprintf("DELETE FROM %s WHERE id in (%s)", $this->table, $id_string);
		$query = $db->query($sql);
		
		return $query ? 'Xóa Thành công!' : "Xóa lỗi ở $this->table! $sql";
	}

	public function status($status='0')
	{
		if ($status == 0) {return 'Block';}
		if ($status == 1) {return 'Active';}
		return '-';
	}

	public function totalRecord($where='')
	{
		global $db;
		$sql = sprintf("SELECT COUNT(*) FROM %s WHERE 1=1 %s", $this->table, $where);
		$query = $db->query($sql);
		if (!$query) {
			return "sql lỗi! $sql";
		}
		$result = $query->fetch_row();
		return is_null($result) ? 0 : $result[0];
	}

}

