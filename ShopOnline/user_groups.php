<?php 
session_start();
ob_start();
require('connect.php');
require('functions.php');
require('page_header.php');
require('class/UserGroup.php');

$object = new UserGroup();
$baseURL = 'user_groups.php';
$tableCols = ['id','name','type','amount'];

require('task_show.php');

if (!isset($_GET['act'])):
	$totalRecord = $object->totalRecord($where);
	$nhomUser = $object->index('*',$where,$limit,$offset,$join,$orderby);

?>

<!-- start show -->
<section class="content clear">
	<div class="namepage clear">
		<div class="container">
			<h1>Quản lý Nhóm Thành Viên</h1>
		</div>
	</div>
	<div class="menubar clear">
		<div class="container">
			<ul>
				<li><a href="<?php echo $baseURL.'?act=add'; ?>">Thêm Nhóm</a></li>
				<li class="message" style="padding-left:5%;">
					<?php if(isset($_SESSION['flash_msg'])) :?>
						<span><?php echo $_SESSION['flash_msg'];?></span>
						<?php unset($_SESSION['flash_msg']); endif;?>
						<?php if(count($error)>0):
						for ($i=0; $i < count($error); $i++) :?>
						<span><?php echo $error[$i]."<br>";?></span>
					<?php endfor; endif;?>
				</li>
			</ul>
		</div>
	</div>
	<div class="content object-show tb-border">
		<div class=" container number clear">
			Số dòng hiển thị: 
			<select onchange="window.location.href=this.value">
				<option value="<?php echo $baseURL.$queryLimit.'limit=10';?>" <?php if ($limit=='10') {echo 'selected';}?>>10</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=20';?>" <?php if ($limit=='20') {echo 'selected';}?>>20</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=50';?>" <?php if ($limit=='50') {echo 'selected';}?>>50</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=100';?>" <?php if ($limit=='100') {echo 'selected';}?>>100</option>
			</select>
		</div>
		<div class="container">
			<span><big>Tổng: <?php echo $totalRecord; ?></big></span>
			<div class="tuychinh">
				<span class="orderby">
					Sắp xếp: 
					<select onchange="window.location.href=this.value">
						<option value="<?php echo $baseURL.$queryBy.'col=id&by=ASC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'ASC') {echo 'selected';} ?> >id Tăng</option>
						<option value="<?php echo $baseURL.$queryBy.'col=id&by=DESC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'DESC') {echo 'selected';} ?> >id Giảm</option>
						<option value="<?php echo $baseURL.$queryBy.'col=name&by=ASC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'ASC') {echo 'selected';} ?> >name Tăng</option>
						<option value="<?php echo $baseURL.$queryBy.'col=name&by=DESC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'DESC') {echo 'selected';} ?> >name Giảm</option>
					</select>
				</span>
				<span class="finding">
					<form action="" method="get">
						<input id="key" type="text" name="key" class="form" placeholder="từ khóa" value="<?php if(isset($_GET['key'])) {echo $_GET['key'];}?>">
						<input onclick="return testFind();" type="submit" name="find" value="Tìm Kiếm" class="button">
					</form>
				</span>
			</div>
			<?php if ($totalRecord == 0):?>
				<div class="container dt-center clear">Không có dữ liệu để hiển thị!</div>
			<?php else: ?>
				<div class="form-table clear tb-center">
					<form action="" method="post">
						<table border="1">
							<tr>
								<th colspan="3">Thao tác</th>
								<th>id</th>
								<th>Tên Nhóm</th>
								<th>Kiểu</th>
								<th>Số lượng</th>
							</tr>
							<?php for ($i=0; $i < count($nhomUser); $i++) :?>
								<tr>
									<td class="options"><input type="checkbox" name="checkbox[]" value="<?php echo $nhomUser[$i]['id'];?>" class="checkbox" <?php if(isset($_POST['checkall'])) {echo 'checked';} ?>></td>
									<td class="options"><a href="<?php echo $baseURL.'?act=edit&id='.$nhomUser[$i]['id'];?>">sửa</a></td>
									<td class="options"><a onclick="return confirm('Bạn chắc chắn muốn xóa không?');" href="<?php echo $baseURL.'?id_del='.$nhomUser[$i]['id'];?>">xóa</a></td>
									<td class="data-show"><?php echo $nhomUser[$i]['id']; ?></td>
									<td class="data-show"><?php echo $nhomUser[$i]['name']; ?></td>
									<td class="data-show"><?php echo $nhomUser[$i]['type']; ?></td>
									<td class="data-show"><?php echo $nhomUser[$i]['amount']; ?></td>
								</tr>
							<?php endfor; ?>
								<tr>
									<td colspan="7">
									<div class="menubot clear">
										<input type="checkbox" id="selectall" onClick="selectAll(this)" class="checkbox">
										<button type="button" id="toggle" value="SelectAll" onClick="checkAll()">Chọn Tất</button>
										<input onclick="return testDel()" type="submit" name="multi_del" value="Xóa">
										<input onclick="return testEdit()" type="submit" name="multi_edit" value="Sửa">
								</div>
									</td>
								</tr>
						</table>
				</form>
			</div>
			<div class="paging clear">
				<div class="container">
					<?php echo paging($baseURL,$queryString,$totalRecord,$page,$limit);?>
				</div>
			</div>
		<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>
<!-- end show -->


<!-- start create  -->
<?php if (isset($_GET['act'])):

require('task_action.php');

if (isset($_POST['addnow'])) {
	$data = $_POST['data'];
	for ($i=0; $i < count($data); $i++) { 
		$id = trim($data[$i]['id']);
		$name = trim($data[$i]['name']);
		$type = $data[$i]['type'];
		$amount = trim($data[$i]['amount']);

		if ($name == '') {
			$error[] = "'Tên nhóm' id=$id không được trống!";
			continue;
		}
		$str = [];
		foreach ($tableCols as $col) {
			$str[] = $$col;
		}
		$value_array[] = "'".implode("','", $str)."'";
	}
	if (count($error)==0) {
		$error[] = $object->create($value_array);
		if ($error[0] == '') {
			$_SESSION['flash_msg'] = 'Thêm Thành Công!';
			header("location: $baseURL");exit;
		}
	}
}
if (isset($_POST['editnow'])) {
	$data = $_POST['data'];
	for ($i=0; $i < count($data); $i++) { 
		$id = trim($data[$i]['id']);
		$name = trim($data[$i]['name']);
		$type = $data[$i]['type'];
		$amount = trim($data[$i]['amount']);

		$id_out = $edit_array[$i]['id'];
		$where = ' AND id='.$id.' AND id<>'.$id_out;
		$testid = $object->index('*',$where);
		if (count($testid)>0) {
			$error[] = 'lỗi id='.$id.' bị trùng!';
			continue;
		}
		if ($name == '') {
			$error[] = "'Tên Thương hiệu' id=$id không được trống!";
			continue;
		}
		$id_array[] = 'id='.$id;
		$str = [];
		foreach ($tableCols as $col) {
			$str[] = "$col='".$$col."'";
		}
		$set_array[] = implode(", ", $str);
	}
	if (count($error) == 0) {
		$error[] = $object->update($set_array,$id_array);
		if ($error[0] == '') {
			$_SESSION['flash_msg'] = 'Sửa Thành Công!';
			header("location: $baseURL");exit;
		}
	}
	
}
?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<?php if ($_GET['act']=='add'):?>
				<h1>Thêm Nhóm</h1>
			<?php endif; ?>
			<?php if ($_GET['act']=='edit'):?>
				<h1>Sửa Nhóm</h1>
			<?php endif; ?>
		</div>
	</div>
	<div class="container" >
		<div class="message" style="padding-left:5%">
			<?php if(isset($_SESSION['flash_msg'])) :?>
				<span><?php echo $_SESSION['flash_msg'];?></span>
			<?php unset($_SESSION['flash_msg']); endif;?>
			<?php if(count($error)>0) :
				for ($i=0; $i < count($error); $i++) :?>
				<span><?php echo $error[$i]."<br>";?></span>
			<?php endfor; endif;?>
		</div>
	</div>
	<div class="back">
		<div class="container">
			<a onclick="return testBack()" href="<?php echo $baseURL;?>">Trở Về</a>
		</div>
	</div>
	<?php if ($_GET['act']=='add'):?>
		<div class="number clear">
			<div class="container">
				Số lượng dòng: 
				<select onchange="window.location.href=this.value">
					<option value="<?php echo $baseURL.'?act=add&num=1'; ?>" <?php if ($num=='1') {echo 'selected';}?>>1</option>
					<option value="<?php echo $baseURL.'?act=add&num=2'; ?>" <?php if ($num=='2') {echo 'selected';}?>>2</option>
					<option value="<?php echo $baseURL.'?act=add&num=5'; ?>" <?php if ($num=='5') {echo 'selected';}?>>5</option>
					<option value="<?php echo $baseURL.'?act=add&num=10'; ?>" <?php if ($num=='10') {echo 'selected';}?>>10</option>
					<option value="<?php echo $baseURL.'?act=add&num=20'; ?>" <?php if ($num=='20') {echo 'selected';}?>>20</option>
				</select>
			</div>
		</div>
	<?php endif;?>
	<div class="form-create clear">
		<div class="full-width">
			<form action="" method="post" enctype="multipart/form-data">
				<table>
					<tr>
						<th>id</th>
						<th>Tên Nhóm</th>
						<th>Kiểu</th>
						<th>Số lượng</th>
					</tr>
					<?php for ($i=0; $i < $num; $i++):?>
						<tr>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php if(isset($edit_array)) {echo $edit_array[$i]['id'];} else {echo ++$idMax;} ?>">
							</td>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][name]" placeholder="tên nhóm" autofocus value="<?php if(isset($data)) {echo $data[$i]['name'];} elseif(isset($edit_array)) {echo $edit_array[$i]['name'];} ?>">
							</td>
						<td>
							<select name="data[<?php echo $i; ?>][type]">
								<option value="rate" <?php if(isset($data) && $data[$i]['type'] == 'rate' || isset($edit_array) && $edit_array[$i]['type'] == 'rate') {echo 'selected';} ?>>Phần trăm</option>
								<option value="price" <?php if(isset($data) && $data[$i]['type'] == 'price' || isset($edit_array) && $edit_array[$i]['type'] == 'price') {echo 'selected';} ?>>Giá trị</option>
							</select>
						</td>
							<td>
								<input type="number" name="data[<?php echo $i; ?>][amount]" placeholder="số lượng" autofocus value="<?php if(isset($data)) {echo $data[$i]['amount'];} elseif(isset($edit_array)) {echo $edit_array[$i]['amount'];} ?>" class="nuagia">
							</td>
					</tr>
				<?php endfor;?>
			</table>
			<div class="container button">
				<?php if ($_GET['act']=='add'):?>
					<input onclick="return testform(<?php echo $num; ?>)" type="submit" name="addnow" value="Thêm Vào" class="submit">
				<?php endif; ?>
				<?php if ($_GET['act']=='edit'):?>
					<input onclick="return testform(<?php echo $num; ?>)" type="submit" name="editnow" value="Cập Nhật" class="submit">
				<?php endif; ?>
			</div>
		</form>
	</div>
</div>
</section>
<?php endif; ?>
<!-- end create  -->

<script type="text/javascript" src="js/script.js"></script>
<?php
require('page_footer.php');
?>