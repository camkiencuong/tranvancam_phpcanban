<?php
$menu=@array_pop(explode('/',$_SERVER['SCRIPT_NAME']));
?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Shop Online</title>
	<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1">
	<link rel="stylesheet" type="text/css" href="css/stylesheet.css">
</head>
<body>
	<header class="page-top <?php if (isset($_GET['view'])) echo 'blur'; ?>">
		<div class="header-top">
			<div class="container">
				<div class="logo">
					<a href="index.php"><img src="https://www.google.com/images/branding/googlelogo/1x/googlelogo_color_272x92dp.png" title="Shop Online" alt="logo Shop Online"></a>
				</div>
				<div class="sitename">
					<h2>Quản lý Cửa Hàng</h2>
					<h2 class="title">Mua sắm thả ga, <span class="more">Tha về quà khủng</span></h2>
				</div>
				<div class="info">
					<h2>Cam's Shop</h2>
				</div>
			</div>
		</div>
		<div class="menu clear">
			<div class="container">
				<ul>
					<li><a class="<?php if($menu=='index.php') echo 'active';?>" href="index.php">Tổng quan</a></li>
					<li><a class="<?php if($menu=='product_categories.php') echo 'active';?>" href="product_categories.php">Danh mục SP</a></li>
					<li><a class="<?php if($menu=='products.php') echo 'active';?>" href="products.php">Sản phẩm</a></li>
					<li><a class="<?php if($menu=='product_images.php') echo 'active';?>" href="product_images.php">Hình ảnh SP</a></li>
					<li><a class="<?php if($menu=='product_relates.php') echo 'active';?>" href="product_relates.php">SP liên quan</a></li>
					<li><a class="<?php if($menu=='brands.php') echo 'active';?>" href="brands.php">Thương hiệu</a></li>
					<li><a class="<?php if($menu=='user_groups.php') echo 'active';?>" href="user_groups.php">Nhóm Thành viên</a></li>
					<li><a class="<?php if($menu=='users.php') echo 'active';?>" href="users.php">Thành viên</a></li>
					<li><a class="<?php if($menu=='admins.php') echo 'active';?>" href="admins.php">Quản trị</a></li>
					<li><a class="<?php if($menu=='orders.php') echo 'active';?>" href="orders.php">Đơn hàng</a></li>
					<li><a class="<?php if($menu=='coupons.php') echo 'active';?>" href="coupons.php">Khuyến mãi</a></li>
					<li><a class="<?php if($menu=='reviews.php') echo 'active';?>" href="reviews.php">Đánh giá</a></li>
					<li><a class="<?php if($menu=='post_categories.php') echo 'active';?>" href="post_categories.php">Danh mục tin</a></li>
					<li><a class="<?php if($menu=='posts.php') echo 'active';?>" href="posts.php">Tin tức</a></li>
					<li><a class="<?php if($menu=='comments.php') echo 'active';?>" href="comments.php">Bình luận</a></li>
					<li><a class="<?php if($menu=='banners.php') echo 'active';?>" href="banners.php">Quảng cáo</a></li>
					<li><a class="<?php if($menu=='pages.php') echo 'active';?>" href="pages.php">Trang tĩnh</a></li>
					<li><a class="<?php if($menu=='provinces.php') echo 'active';?>" href="provinces.php">Tỉnh/Thành phố</a></li>
					<li><a class="<?php if($menu=='districts.php') echo 'active';?>" href="districts.php">Quận/Huyện</a></li>
					<li><a class="<?php if($menu=='') echo 'active';?>" href="#"></a></li>
				</ul>
			</div>
		</div>
	</header>