-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2018 at 10:26 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `shoponline`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `role` int(11) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `username`, `password`, `fullname`, `role`, `status`) VALUES
(1, 'campro', '87654321', 'TVCam', 1, 1),
(2, 'thaomy', '87654321', 'Thao My', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `banners`
--

CREATE TABLE `banners` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `name`, `slug`, `logo`, `image`, `description`, `meta_title`, `meta_keyword`, `meta_description`, `status`) VALUES
(1, 'Everest', 'everest', 'img/brands/everest/logo.jpg', 'img/brands/everest/image.png', 'Everest là một thương hiệu giày thuộc Công ty Trách nhiệm hữu hạn Thời trang Everest. Công ty được thành lập vào tháng 4/2016 với trụ sở tại Thành phố Hồ Chí Minh. Với đội ngũ nhân viên và nhà thiết kế tận tâm và luôn tạo ra các mẫu mã mới và chất lượng tốt, vì thế luôn làm hài lòng mọi người tiêu dùng.', 'everest', 'everest', 'everest', 1),
(2, 'Adidas', 'adidas', 'img/brands/adidas/logo.png', 'img/brands/adidas/image.png', 'Adidas ltd AG là một nhà sản xuất dụng cụ thể thao của Đức, một thành viên của Adidas Group, bao gồm cả công ty dụng cụ thể thao Reebok, công ty golf Taylormade, công ty sản xuất bóng golf Maxfli và Adidas golf.', 'adidas', 'adidas', 'adidas', 1),
(3, 'Nike', 'nike', 'img/brands/logo/logo.png', 'img/brands/logo/image.jpg', 'Nike, Inc. là nhà cung cấp quần áo và dụng cụ thể thao thương mại công cộng lớn có trụ sở tại Hoa Kỳ. Đầu não của công ty đặt tại Beaverton, gần vùng đô thị Portland của Oregon.', 'nike', 'nike', 'nike', 1),
(4, 'VFASHION', 'vfashion', 'img/brands/vfashion/logo.png', 'img/brands/vfashion/image.png', 'vfashion', 'vfashion', 'vfashion', 'vfashion', 1),
(5, 'Việt Tiến', 'viet-tien', 'img/brands/viet-tien/logo.png', 'img/brands/viet-tien/image.png', 'Việt Tiến', 'Việt Tiến', 'Việt Tiến', 'Việt Tiến', 1),
(6, 'Ewin', 'ewin', 'img/brands/ewin/logo.png', 'img/brands/ewin/image.png', 'ewin', 'ewin', 'ewin', 'ewin', 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `fullname` varchar(255) NOT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coupons`
--

CREATE TABLE `coupons` (
  `id` int(11) NOT NULL,
  `code` int(11) NOT NULL,
  `start_date` date NOT NULL,
  `end_date` date NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `type` tinyint(4) NOT NULL,
  `uses_per_user` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(11) NOT NULL,
  `province_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `province_id`, `name`, `code`) VALUES
(1, 1, ' Quận Ba Đình', 1),
(2, 1, ' Quận Hoàn Kiếm', 2),
(3, 1, ' Quận Tây Hồ', 3),
(4, 1, ' Quận Long Biên', 4),
(5, 1, ' Quận Cầu Giấy', 5),
(6, 1, ' Quận Đống Đa', 6),
(7, 1, ' Quận Hai Bà Trưng', 7),
(8, 1, ' Quận Hoàng Mai', 8),
(9, 1, ' Quận Thanh Xuân', 9),
(16, 1, ' Huyện Sóc Sơn', 16),
(17, 1, ' Huyện Đông Anh', 17),
(18, 1, ' Huyện Gia Lâm', 18),
(19, 1, ' Quận Nam Từ Liêm', 19),
(20, 1, ' Huyện Thanh Trì', 20),
(21, 1, ' Quận Bắc Từ Liêm', 21),
(24, 2, ' Thành phố Hà Giang', 24),
(26, 2, ' Huyện Đồng Văn', 26),
(27, 2, ' Huyện Mèo Vạc', 27),
(28, 2, ' Huyện Yên Minh', 28),
(29, 2, ' Huyện Quản Bạ', 29),
(30, 2, ' Huyện Vị Xuyên', 30),
(31, 2, ' Huyện Bắc Mê', 31),
(32, 2, ' Huyện Hoàng Su Phì', 32),
(33, 2, ' Huyện Xín Mần', 33),
(34, 2, ' Huyện Bắc Quang', 34),
(35, 2, ' Huyện Quang Bình', 35),
(40, 4, ' Thành phố Cao Bằng', 40),
(42, 4, ' Huyện Bảo Lâm', 42),
(43, 4, ' Huyện Bảo Lạc', 43),
(44, 4, ' Huyện Thông Nông', 44),
(45, 4, ' Huyện Hà Quảng', 45),
(46, 4, ' Huyện Trà Lĩnh', 46),
(47, 4, ' Huyện Trùng Khánh', 47),
(48, 4, ' Huyện Hạ Lang', 48),
(49, 4, ' Huyện Quảng Uyên', 49),
(50, 4, ' Huyện Phục Hoà', 50),
(51, 4, ' Huyện Hoà An', 51),
(52, 4, ' Huyện Nguyên Bình', 52),
(53, 4, ' Huyện Thạch An', 53),
(58, 6, ' Thành Phố Bắc Kạn', 58),
(60, 6, ' Huyện Pác Nặm', 60),
(61, 6, ' Huyện Ba Bể', 61),
(62, 6, ' Huyện Ngân Sơn', 62),
(63, 6, ' Huyện Bạch Thông', 63),
(64, 6, ' Huyện Chợ Đồn', 64),
(65, 6, ' Huyện Chợ Mới', 65),
(66, 6, ' Huyện Na Rì', 66),
(70, 8, ' Thành phố Tuyên Quang', 70),
(71, 8, ' Huyện Lâm Bình', 71),
(72, 8, ' Huyện Nà Hang', 72),
(73, 8, ' Huyện Chiêm Hóa', 73),
(74, 8, ' Huyện Hàm Yên', 74),
(75, 8, ' Huyện Yên Sơn', 75),
(76, 8, ' Huyện Sơn Dương', 76),
(80, 10, ' Thành phố Lào Cai', 80),
(82, 10, ' Huyện Bát Xát', 82),
(83, 10, ' Huyện Mường Khương', 83),
(84, 10, ' Huyện Si Ma Cai', 84),
(85, 10, ' Huyện Bắc Hà', 85),
(86, 10, ' Huyện Bảo Thắng', 86),
(87, 10, ' Huyện Bảo Yên', 87),
(88, 10, ' Huyện Sa Pa', 88),
(89, 10, ' Huyện Văn Bàn', 89),
(94, 11, ' Thành phố Điện Biên Phủ', 94),
(95, 11, ' Thị Xã Mường Lay', 95),
(96, 11, ' Huyện Mường Nhé', 96),
(97, 11, ' Huyện Mường Chà', 97),
(98, 11, ' Huyện Tủa Chùa', 98),
(99, 11, ' Huyện Tuần Giáo', 99),
(100, 11, ' Huyện Điện Biên', 100),
(101, 11, ' Huyện Điện Biên Đông', 101),
(102, 11, ' Huyện Mường Ảng', 102),
(103, 11, ' Huyện Nậm Pồ', 103),
(105, 12, ' Thành phố Lai Châu', 105),
(106, 12, ' Huyện Tam Đường', 106),
(107, 12, ' Huyện Mường Tè', 107),
(108, 12, ' Huyện Sìn Hồ', 108),
(109, 12, ' Huyện Phong Thổ', 109),
(110, 12, ' Huyện Than Uyên', 110),
(111, 12, ' Huyện Tân Uyên', 111),
(112, 12, ' Huyện Nậm Nhùn', 112),
(116, 14, ' Thành phố Sơn La', 116),
(118, 14, ' Huyện Quỳnh Nhai', 118),
(119, 14, ' Huyện Thuận Châu', 119),
(120, 14, ' Huyện Mường La', 120),
(121, 14, ' Huyện Bắc Yên', 121),
(122, 14, ' Huyện Phù Yên', 122),
(123, 14, ' Huyện Mộc Châu', 123),
(124, 14, ' Huyện Yên Châu', 124),
(125, 14, ' Huyện Mai Sơn', 125),
(126, 14, ' Huyện Sông Mã', 126),
(127, 14, ' Huyện Sốp Cộp', 127),
(128, 14, ' Huyện Vân Hồ', 128),
(132, 15, ' Thành phố Yên Bái', 132),
(133, 15, ' Thị xã Nghĩa Lộ', 133),
(135, 15, ' Huyện Lục Yên', 135),
(136, 15, ' Huyện Văn Yên', 136),
(137, 15, ' Huyện Mù Căng Chải', 137),
(138, 15, ' Huyện Trấn Yên', 138),
(139, 15, ' Huyện Trạm Tấu', 139),
(140, 15, ' Huyện Văn Chấn', 140),
(141, 15, ' Huyện Yên Bình', 141),
(148, 17, ' Thành phố Hòa Bình', 148),
(150, 17, ' Huyện Đà Bắc', 150),
(151, 17, ' Huyện Kỳ Sơn', 151),
(152, 17, ' Huyện Lương Sơn', 152),
(153, 17, ' Huyện Kim Bôi', 153),
(154, 17, ' Huyện Cao Phong', 154),
(155, 17, ' Huyện Tân Lạc', 155),
(156, 17, ' Huyện Mai Châu', 156),
(157, 17, ' Huyện Lạc Sơn', 157),
(158, 17, ' Huyện Yên Thủy', 158),
(159, 17, ' Huyện Lạc Thủy', 159),
(164, 19, ' Thành phố Thái Nguyên', 164),
(165, 19, ' Thành phố Sông Công', 165),
(167, 19, ' Huyện Định Hóa', 167),
(168, 19, ' Huyện Phú Lương', 168),
(169, 19, ' Huyện Đồng Hỷ', 169),
(170, 19, ' Huyện Võ Nhai', 170),
(171, 19, ' Huyện Đại Từ', 171),
(172, 19, ' Thị xã Phổ Yên', 172),
(173, 19, ' Huyện Phú Bình', 173),
(178, 20, ' Thành phố Lạng Sơn', 178),
(180, 20, ' Huyện Tràng Định', 180),
(181, 20, ' Huyện Bình Gia', 181),
(182, 20, ' Huyện Văn Lãng', 182),
(183, 20, ' Huyện Cao Lộc', 183),
(184, 20, ' Huyện Văn Quan', 184),
(185, 20, ' Huyện Bắc Sơn', 185),
(186, 20, ' Huyện Hữu Lũng', 186),
(187, 20, ' Huyện Chi Lăng', 187),
(188, 20, ' Huyện Lộc Bình', 188),
(189, 20, ' Huyện Đình Lập', 189),
(193, 22, ' Thành phố Hạ Long', 193),
(194, 22, ' Thành phố Móng Cái', 194),
(195, 22, ' Thành phố Cẩm Phả', 195),
(196, 22, ' Thành phố Uông Bí', 196),
(198, 22, ' Huyện Bình Liêu', 198),
(199, 22, ' Huyện Tiên Yên', 199),
(200, 22, ' Huyện Đầm Hà', 200),
(201, 22, ' Huyện Hải Hà', 201),
(202, 22, ' Huyện Ba Chẽ', 202),
(203, 22, ' Huyện Vân Đồn', 203),
(204, 22, ' Huyện Hoành Bồ', 204),
(205, 22, ' Thị xã Đông Triều', 205),
(206, 22, ' Thị xã Quảng Yên', 206),
(207, 22, ' Huyện Cô Tô', 207),
(213, 24, ' Thành phố Bắc Giang', 213),
(215, 24, ' Huyện Yên Thế', 215),
(216, 24, ' Huyện Tân Yên', 216),
(217, 24, ' Huyện Lạng Giang', 217),
(218, 24, ' Huyện Lục Nam', 218),
(219, 24, ' Huyện Lục Ngạn', 219),
(220, 24, ' Huyện Sơn Động', 220),
(221, 24, ' Huyện Yên Dũng', 221),
(222, 24, ' Huyện Việt Yên', 222),
(223, 24, ' Huyện Hiệp Hòa', 223),
(227, 25, ' Thành phố Việt Trì', 227),
(228, 25, ' Thị xã Phú Thọ', 228),
(230, 25, ' Huyện Đoan Hùng', 230),
(231, 25, ' Huyện Hạ Hoà', 231),
(232, 25, ' Huyện Thanh Ba', 232),
(233, 25, ' Huyện Phù Ninh', 233),
(234, 25, ' Huyện Yên Lập', 234),
(235, 25, ' Huyện Cẩm Khê', 235),
(236, 25, ' Huyện Tam Nông', 236),
(237, 25, ' Huyện Lâm Thao', 237),
(238, 25, ' Huyện Thanh Sơn', 238),
(239, 25, ' Huyện Thanh Thuỷ', 239),
(240, 25, ' Huyện Tân Sơn', 240),
(243, 26, ' Thành phố Vĩnh Yên', 243),
(244, 26, ' Thị xã Phúc Yên', 244),
(246, 26, ' Huyện Lập Thạch', 246),
(247, 26, ' Huyện Tam Dương', 247),
(248, 26, ' Huyện Tam Đảo', 248),
(249, 26, ' Huyện Bình Xuyên', 249),
(250, 1, ' Huyện Mê Linh', 250),
(251, 26, ' Huyện Yên Lạc', 251),
(252, 26, ' Huyện Vĩnh Tường', 252),
(253, 26, ' Huyện Sông Lô', 253),
(256, 27, ' Thành phố Bắc Ninh', 256),
(258, 27, ' Huyện Yên Phong', 258),
(259, 27, ' Huyện Quế Võ', 259),
(260, 27, ' Huyện Tiên Du', 260),
(261, 27, ' Thị xã Từ Sơn', 261),
(262, 27, ' Huyện Thuận Thành', 262),
(263, 27, ' Huyện Gia Bình', 263),
(264, 27, ' Huyện Lương Tài', 264),
(268, 1, ' Quận Hà Đông', 268),
(269, 1, ' Thị xã Sơn Tây', 269),
(271, 1, ' Huyện Ba Vì', 271),
(272, 1, ' Huyện Phúc Thọ', 272),
(273, 1, ' Huyện Đan Phượng', 273),
(274, 1, ' Huyện Hoài Đức', 274),
(275, 1, ' Huyện Quốc Oai', 275),
(276, 1, ' Huyện Thạch Thất', 276),
(277, 1, ' Huyện Chương Mỹ', 277),
(278, 1, ' Huyện Thanh Oai', 278),
(279, 1, ' Huyện Thường Tín', 279),
(280, 1, ' Huyện Phú Xuyên', 280),
(281, 1, ' Huyện Ứng Hòa', 281),
(282, 1, ' Huyện Mỹ Đức', 282),
(288, 30, ' Thành phố Hải Dương', 288),
(290, 30, ' Thị xã Chí Linh', 290),
(291, 30, ' Huyện Nam Sách', 291),
(292, 30, ' Huyện Kinh Môn', 292),
(293, 30, ' Huyện Kim Thành', 293),
(294, 30, ' Huyện Thanh Hà', 294),
(295, 30, ' Huyện Cẩm Giàng', 295),
(296, 30, ' Huyện Bình Giang', 296),
(297, 30, ' Huyện Gia Lộc', 297),
(298, 30, ' Huyện Tứ Kỳ', 298),
(299, 30, ' Huyện Ninh Giang', 299),
(300, 30, ' Huyện Thanh Miện', 300),
(303, 31, ' Quận Hồng Bàng', 303),
(304, 31, ' Quận Ngô Quyền', 304),
(305, 31, ' Quận Lê Chân', 305),
(306, 31, ' Quận Hải An', 306),
(307, 31, ' Quận Kiến An', 307),
(308, 31, ' Quận Đồ Sơn', 308),
(309, 31, ' Quận Dương Kinh', 309),
(311, 31, ' Huyện Thuỷ Nguyên', 311),
(312, 31, ' Huyện An Dương', 312),
(313, 31, ' Huyện An Lão', 313),
(314, 31, ' Huyện Kiến Thuỵ', 314),
(315, 31, ' Huyện Tiên Lãng', 315),
(316, 31, ' Huyện Vĩnh Bảo', 316),
(317, 31, ' Huyện Cát Hải', 317),
(318, 31, ' Huyện Bạch Long Vĩ', 318),
(323, 33, ' Thành phố Hưng Yên', 323),
(325, 33, ' Huyện Văn Lâm', 325),
(326, 33, ' Huyện Văn Giang', 326),
(327, 33, ' Huyện Yên Mỹ', 327),
(328, 33, ' Huyện Mỹ Hào', 328),
(329, 33, ' Huyện Ân Thi', 329),
(330, 33, ' Huyện Khoái Châu', 330),
(331, 33, ' Huyện Kim Động', 331),
(332, 33, ' Huyện Tiên Lữ', 332),
(333, 33, ' Huyện Phù Cừ', 333),
(336, 34, ' Thành phố Thái Bình', 336),
(338, 34, ' Huyện Quỳnh Phụ', 338),
(339, 34, ' Huyện Hưng Hà', 339),
(340, 34, ' Huyện Đông Hưng', 340),
(341, 34, ' Huyện Thái Thụy', 341),
(342, 34, ' Huyện Tiền Hải', 342),
(343, 34, ' Huyện Kiến Xương', 343),
(344, 34, ' Huyện Vũ Thư', 344),
(347, 35, ' Thành phố Phủ Lý', 347),
(349, 35, ' Huyện Duy Tiên', 349),
(350, 35, ' Huyện Kim Bảng', 350),
(351, 35, ' Huyện Thanh Liêm', 351),
(352, 35, ' Huyện Bình Lục', 352),
(353, 35, ' Huyện Lý Nhân', 353),
(356, 36, ' Thành phố Nam Định', 356),
(358, 36, ' Huyện Mỹ Lộc', 358),
(359, 36, ' Huyện Vụ Bản', 359),
(360, 36, ' Huyện Ý Yên', 360),
(361, 36, ' Huyện Nghĩa Hưng', 361),
(362, 36, ' Huyện Nam Trực', 362),
(363, 36, ' Huyện Trực Ninh', 363),
(364, 36, ' Huyện Xuân Trường', 364),
(365, 36, ' Huyện Giao Thủy', 365),
(366, 36, ' Huyện Hải Hậu', 366),
(369, 37, ' Thành phố Ninh Bình', 369),
(370, 37, ' Thành phố Tam Điệp', 370),
(372, 37, ' Huyện Nho Quan', 372),
(373, 37, ' Huyện Gia Viễn', 373),
(374, 37, ' Huyện Hoa Lư', 374),
(375, 37, ' Huyện Yên Khánh', 375),
(376, 37, ' Huyện Kim Sơn', 376),
(377, 37, ' Huyện Yên Mô', 377),
(380, 38, ' Thành phố Thanh Hóa', 380),
(381, 38, ' Thị xã Bỉm Sơn', 381),
(382, 38, ' Thành phố Sầm Sơn', 382),
(384, 38, ' Huyện Mường Lát', 384),
(385, 38, ' Huyện Quan Hóa', 385),
(386, 38, ' Huyện Bá Thước', 386),
(387, 38, ' Huyện Quan Sơn', 387),
(388, 38, ' Huyện Lang Chánh', 388),
(389, 38, ' Huyện Ngọc Lặc', 389),
(390, 38, ' Huyện Cẩm Thủy', 390),
(391, 38, ' Huyện Thạch Thành', 391),
(392, 38, ' Huyện Hà Trung', 392),
(393, 38, ' Huyện Vĩnh Lộc', 393),
(394, 38, ' Huyện Yên Định', 394),
(395, 38, ' Huyện Thọ Xuân', 395),
(396, 38, ' Huyện Thường Xuân', 396),
(397, 38, ' Huyện Triệu Sơn', 397),
(398, 38, ' Huyện Thiệu Hóa', 398),
(399, 38, ' Huyện Hoằng Hóa', 399),
(400, 38, ' Huyện Hậu Lộc', 400),
(401, 38, ' Huyện Nga Sơn', 401),
(402, 38, ' Huyện Như Xuân', 402),
(403, 38, ' Huyện Như Thanh', 403),
(404, 38, ' Huyện Nông Cống', 404),
(405, 38, ' Huyện Đông Sơn', 405),
(406, 38, ' Huyện Quảng Xương', 406),
(407, 38, ' Huyện Tĩnh Gia', 407),
(412, 40, ' Thành phố Vinh', 412),
(413, 40, ' Thị xã Cửa Lò', 413),
(414, 40, ' Thị xã Thái Hoà', 414),
(415, 40, ' Huyện Quế Phong', 415),
(416, 40, ' Huyện Quỳ Châu', 416),
(417, 40, ' Huyện Kỳ Sơn', 417),
(418, 40, ' Huyện Tương Dương', 418),
(419, 40, ' Huyện Nghĩa Đàn', 419),
(420, 40, ' Huyện Quỳ Hợp', 420),
(421, 40, ' Huyện Quỳnh Lưu', 421),
(422, 40, ' Huyện Con Cuông', 422),
(423, 40, ' Huyện Tân Kỳ', 423),
(424, 40, ' Huyện Anh Sơn', 424),
(425, 40, ' Huyện Diễn Châu', 425),
(426, 40, ' Huyện Yên Thành', 426),
(427, 40, ' Huyện Đô Lương', 427),
(428, 40, ' Huyện Thanh Chương', 428),
(429, 40, ' Huyện Nghi Lộc', 429),
(430, 40, ' Huyện Nam Đàn', 430),
(431, 40, ' Huyện Hưng Nguyên', 431),
(432, 40, ' Thị xã Hoàng Mai', 432),
(436, 42, ' Thành phố Hà Tĩnh', 436),
(437, 42, ' Thị xã Hồng Lĩnh', 437),
(439, 42, ' Huyện Hương Sơn', 439),
(440, 42, ' Huyện Đức Thọ', 440),
(441, 42, ' Huyện Vũ Quang', 441),
(442, 42, ' Huyện Nghi Xuân', 442),
(443, 42, ' Huyện Can Lộc', 443),
(444, 42, ' Huyện Hương Khê', 444),
(445, 42, ' Huyện Thạch Hà', 445),
(446, 42, ' Huyện Cẩm Xuyên', 446),
(447, 42, ' Huyện Kỳ Anh', 447),
(448, 42, ' Huyện Lộc Hà', 448),
(449, 42, ' Thị xã Kỳ Anh', 449),
(450, 44, ' Thành Phố Đồng Hới', 450),
(452, 44, ' Huyện Minh Hóa', 452),
(453, 44, ' Huyện Tuyên Hóa', 453),
(454, 44, ' Huyện Quảng Trạch', 454),
(455, 44, ' Huyện Bố Trạch', 455),
(456, 44, ' Huyện Quảng Ninh', 456),
(457, 44, ' Huyện Lệ Thủy', 457),
(458, 44, ' Thị xã Ba Đồn', 458),
(461, 45, ' Thành phố Đông Hà', 461),
(462, 45, ' Thị xã Quảng Trị', 462),
(464, 45, ' Huyện Vĩnh Linh', 464),
(465, 45, ' Huyện Hướng Hóa', 465),
(466, 45, ' Huyện Gio Linh', 466),
(467, 45, ' Huyện Đa Krông', 467),
(468, 45, ' Huyện Cam Lộ', 468),
(469, 45, ' Huyện Triệu Phong', 469),
(470, 45, ' Huyện Hải Lăng', 470),
(471, 45, ' Huyện Cồn Cỏ', 471),
(474, 46, ' Thành phố Huế', 474),
(476, 46, ' Huyện Phong Điền', 476),
(477, 46, ' Huyện Quảng Điền', 477),
(478, 46, ' Huyện Phú Vang', 478),
(479, 46, ' Thị xã Hương Thủy', 479),
(480, 46, ' Thị xã Hương Trà', 480),
(481, 46, ' Huyện A Lưới', 481),
(482, 46, ' Huyện Phú Lộc', 482),
(483, 46, ' Huyện Nam Đông', 483),
(490, 48, ' Quận Liên Chiểu', 490),
(491, 48, ' Quận Thanh Khê', 491),
(492, 48, ' Quận Hải Châu', 492),
(493, 48, ' Quận Sơn Trà', 493),
(494, 48, ' Quận Ngũ Hành Sơn', 494),
(495, 48, ' Quận Cẩm Lệ', 495),
(497, 48, ' Huyện Hòa Vang', 497),
(498, 48, ' Huyện Hoàng Sa', 498),
(502, 49, ' Thành phố Tam Kỳ', 502),
(503, 49, ' Thành phố Hội An', 503),
(504, 49, ' Huyện Tây Giang', 504),
(505, 49, ' Huyện Đông Giang', 505),
(506, 49, ' Huyện Đại Lộc', 506),
(507, 49, ' Thị xã Điện Bàn', 507),
(508, 49, ' Huyện Duy Xuyên', 508),
(509, 49, ' Huyện Quế Sơn', 509),
(510, 49, ' Huyện Nam Giang', 510),
(511, 49, ' Huyện Phước Sơn', 511),
(512, 49, ' Huyện Hiệp Đức', 512),
(513, 49, ' Huyện Thăng Bình', 513),
(514, 49, ' Huyện Tiên Phước', 514),
(515, 49, ' Huyện Bắc Trà My', 515),
(516, 49, ' Huyện Nam Trà My', 516),
(517, 49, ' Huyện Núi Thành', 517),
(518, 49, ' Huyện Phú Ninh', 518),
(519, 49, ' Huyện Nông Sơn', 519),
(522, 51, ' Thành phố Quảng Ngãi', 522),
(524, 51, ' Huyện Bình Sơn', 524),
(525, 51, ' Huyện Trà Bồng', 525),
(526, 51, ' Huyện Tây Trà', 526),
(527, 51, ' Huyện Sơn Tịnh', 527),
(528, 51, ' Huyện Tư Nghĩa', 528),
(529, 51, ' Huyện Sơn Hà', 529),
(530, 51, ' Huyện Sơn Tây', 530),
(531, 51, ' Huyện Minh Long', 531),
(532, 51, ' Huyện Nghĩa Hành', 532),
(533, 51, ' Huyện Mộ Đức', 533),
(534, 51, ' Huyện Đức Phổ', 534),
(535, 51, ' Huyện Ba Tơ', 535),
(536, 51, ' Huyện Lý Sơn', 536),
(540, 52, ' Thành phố Qui Nhơn', 540),
(542, 52, ' Huyện An Lão', 542),
(543, 52, ' Huyện Hoài Nhơn', 543),
(544, 52, ' Huyện Hoài Ân', 544),
(545, 52, ' Huyện Phù Mỹ', 545),
(546, 52, ' Huyện Vĩnh Thạnh', 546),
(547, 52, ' Huyện Tây Sơn', 547),
(548, 52, ' Huyện Phù Cát', 548),
(549, 52, ' Thị xã An Nhơn', 549),
(550, 52, ' Huyện Tuy Phước', 550),
(551, 52, ' Huyện Vân Canh', 551),
(555, 54, ' Thành phố Tuy Hoà', 555),
(557, 54, ' Thị xã Sông Cầu', 557),
(558, 54, ' Huyện Đồng Xuân', 558),
(559, 54, ' Huyện Tuy An', 559),
(560, 54, ' Huyện Sơn Hòa', 560),
(561, 54, ' Huyện Sông Hinh', 561),
(562, 54, ' Huyện Tây Hoà', 562),
(563, 54, ' Huyện Phú Hoà', 563),
(564, 54, ' Huyện Đông Hòa', 564),
(568, 56, ' Thành phố Nha Trang', 568),
(569, 56, ' Thành phố Cam Ranh', 569),
(570, 56, ' Huyện Cam Lâm', 570),
(571, 56, ' Huyện Vạn Ninh', 571),
(572, 56, ' Thị xã Ninh Hòa', 572),
(573, 56, ' Huyện Khánh Vĩnh', 573),
(574, 56, ' Huyện Diên Khánh', 574),
(575, 56, ' Huyện Khánh Sơn', 575),
(576, 56, ' Huyện Trường Sa', 576),
(582, 58, ' Thành phố Phan Rang-Tháp Chàm', 582),
(584, 58, ' Huyện Bác Ái', 584),
(585, 58, ' Huyện Ninh Sơn', 585),
(586, 58, ' Huyện Ninh Hải', 586),
(587, 58, ' Huyện Ninh Phước', 587),
(588, 58, ' Huyện Thuận Bắc', 588),
(589, 58, ' Huyện Thuận Nam', 589),
(593, 60, ' Thành phố Phan Thiết', 593),
(594, 60, ' Thị xã La Gi', 594),
(595, 60, ' Huyện Tuy Phong', 595),
(596, 60, ' Huyện Bắc Bình', 596),
(597, 60, ' Huyện Hàm Thuận Bắc', 597),
(598, 60, ' Huyện Hàm Thuận Nam', 598),
(599, 60, ' Huyện Tánh Linh', 599),
(600, 60, ' Huyện Đức Linh', 600),
(601, 60, ' Huyện Hàm Tân', 601),
(602, 60, ' Huyện Phú Quí', 602),
(608, 62, ' Thành phố Kon Tum', 608),
(610, 62, ' Huyện Đắk Glei', 610),
(611, 62, ' Huyện Ngọc Hồi', 611),
(612, 62, ' Huyện Đắk Tô', 612),
(613, 62, ' Huyện Kon Plông', 613),
(614, 62, ' Huyện Kon Rẫy', 614),
(615, 62, ' Huyện Đắk Hà', 615),
(616, 62, ' Huyện Sa Thầy', 616),
(617, 62, ' Huyện Tu Mơ Rông', 617),
(618, 62, ' Huyện Ia H\' Drai', 618),
(622, 64, ' Thành phố Pleiku', 622),
(623, 64, ' Thị xã An Khê', 623),
(624, 64, ' Thị xã Ayun Pa', 624),
(625, 64, ' Huyện KBang', 625),
(626, 64, ' Huyện Đăk Đoa', 626),
(627, 64, ' Huyện Chư Păh', 627),
(628, 64, ' Huyện Ia Grai', 628),
(629, 64, ' Huyện Mang Yang', 629),
(630, 64, ' Huyện Kông Chro', 630),
(631, 64, ' Huyện Đức Cơ', 631),
(632, 64, ' Huyện Chư Prông', 632),
(633, 64, ' Huyện Chư Sê', 633),
(634, 64, ' Huyện Đăk Pơ', 634),
(635, 64, ' Huyện Ia Pa', 635),
(637, 64, ' Huyện Krông Pa', 637),
(638, 64, ' Huyện Phú Thiện', 638),
(639, 64, ' Huyện Chư Pưh', 639),
(643, 66, ' Thành phố Buôn Ma Thuột', 643),
(644, 66, ' Thị Xã Buôn Hồ', 644),
(645, 66, ' Huyện Ea H\'leo', 645),
(646, 66, ' Huyện Ea Súp', 646),
(647, 66, ' Huyện Buôn Đôn', 647),
(648, 66, ' Huyện Cư M\'gar', 648),
(649, 66, ' Huyện Krông Búk', 649),
(650, 66, ' Huyện Krông Năng', 650),
(651, 66, ' Huyện Ea Kar', 651),
(652, 66, ' Huyện M\'Đrắk', 652),
(653, 66, ' Huyện Krông Bông', 653),
(654, 66, ' Huyện Krông Pắc', 654),
(655, 66, ' Huyện Krông A Na', 655),
(656, 66, ' Huyện Lắk', 656),
(657, 66, ' Huyện Cư Kuin', 657),
(660, 67, ' Thị xã Gia Nghĩa', 660),
(661, 67, ' Huyện Đăk Glong', 661),
(662, 67, ' Huyện Cư Jút', 662),
(663, 67, ' Huyện Đắk Mil', 663),
(664, 67, ' Huyện Krông Nô', 664),
(665, 67, ' Huyện Đắk Song', 665),
(666, 67, ' Huyện Đắk R\'Lấp', 666),
(667, 67, ' Huyện Tuy Đức', 667),
(672, 68, ' Thành phố Đà Lạt', 672),
(673, 68, ' Thành phố Bảo Lộc', 673),
(674, 68, ' Huyện Đam Rông', 674),
(675, 68, ' Huyện Lạc Dương', 675),
(676, 68, ' Huyện Lâm Hà', 676),
(677, 68, ' Huyện Đơn Dương', 677),
(678, 68, ' Huyện Đức Trọng', 678),
(679, 68, ' Huyện Di Linh', 679),
(680, 68, ' Huyện Bảo Lâm', 680),
(681, 68, ' Huyện Đạ Huoai', 681),
(682, 68, ' Huyện Đạ Tẻh', 682),
(683, 68, ' Huyện Cát Tiên', 683),
(688, 70, ' Thị xã Phước Long', 688),
(689, 70, ' Thị xã Đồng Xoài', 689),
(690, 70, ' Thị xã Bình Long', 690),
(691, 70, ' Huyện Bù Gia Mập', 691),
(692, 70, ' Huyện Lộc Ninh', 692),
(693, 70, ' Huyện Bù Đốp', 693),
(694, 70, ' Huyện Hớn Quản', 694),
(695, 70, ' Huyện Đồng Phú', 695),
(696, 70, ' Huyện Bù Đăng', 696),
(697, 70, ' Huyện Chơn Thành', 697),
(698, 70, ' Huyện Phú Riềng', 698),
(703, 72, ' Thành phố Tây Ninh', 703),
(705, 72, ' Huyện Tân Biên', 705),
(706, 72, ' Huyện Tân Châu', 706),
(707, 72, ' Huyện Dương Minh Châu', 707),
(708, 72, ' Huyện Châu Thành', 708),
(709, 72, ' Huyện Hòa Thành', 709),
(710, 72, ' Huyện Gò Dầu', 710),
(711, 72, ' Huyện Bến Cầu', 711),
(712, 72, ' Huyện Trảng Bàng', 712),
(718, 74, ' Thành phố Thủ Dầu Một', 718),
(719, 74, ' Huyện Bàu Bàng', 719),
(720, 74, ' Huyện Dầu Tiếng', 720),
(721, 74, ' Thị xã Bến Cát', 721),
(722, 74, ' Huyện Phú Giáo', 722),
(723, 74, ' Thị xã Tân Uyên', 723),
(724, 74, ' Thị xã Dĩ An', 724),
(725, 74, ' Thị xã Thuận An', 725),
(726, 74, ' Huyện Bắc Tân Uyên', 726),
(731, 75, ' Thành phố Biên Hòa', 731),
(732, 75, ' Thị xã Long Khánh', 732),
(734, 75, ' Huyện Tân Phú', 734),
(735, 75, ' Huyện Vĩnh Cửu', 735),
(736, 75, ' Huyện Định Quán', 736),
(737, 75, ' Huyện Trảng Bom', 737),
(738, 75, ' Huyện Thống Nhất', 738),
(739, 75, ' Huyện Cẩm Mỹ', 739),
(740, 75, ' Huyện Long Thành', 740),
(741, 75, ' Huyện Xuân Lộc', 741),
(742, 75, ' Huyện Nhơn Trạch', 742),
(747, 77, ' Thành phố Vũng Tàu', 747),
(748, 77, ' Thành phố Bà Rịa', 748),
(750, 77, ' Huyện Châu Đức', 750),
(751, 77, ' Huyện Xuyên Mộc', 751),
(752, 77, ' Huyện Long Điền', 752),
(753, 77, ' Huyện Đất Đỏ', 753),
(754, 77, ' Huyện Tân Thành', 754),
(755, 77, ' Huyện Côn Đảo', 755),
(760, 79, ' Quận 1', 760),
(761, 79, ' Quận 12', 761),
(762, 79, ' Quận Thủ Đức', 762),
(763, 79, ' Quận 9', 763),
(764, 79, ' Quận Gò Vấp', 764),
(765, 79, ' Quận Bình Thạnh', 765),
(766, 79, ' Quận Tân Bình', 766),
(767, 79, ' Quận Tân Phú', 767),
(768, 79, ' Quận Phú Nhuận', 768),
(769, 79, ' Quận 2', 769),
(770, 79, ' Quận 3', 770),
(771, 79, ' Quận 10', 771),
(772, 79, ' Quận 11', 772),
(773, 79, ' Quận 4', 773),
(774, 79, ' Quận 5', 774),
(775, 79, ' Quận 6', 775),
(776, 79, ' Quận 8', 776),
(777, 79, ' Quận Bình Tân', 777),
(778, 79, ' Quận 7', 778),
(783, 79, ' Huyện Củ Chi', 783),
(784, 79, ' Huyện Hóc Môn', 784),
(785, 79, ' Huyện Bình Chánh', 785),
(786, 79, ' Huyện Nhà Bè', 786),
(787, 79, ' Huyện Cần Giờ', 787),
(794, 80, ' Thành phố Tân An', 794),
(795, 80, ' Thị xã Kiến Tường', 795),
(796, 80, ' Huyện Tân Hưng', 796),
(797, 80, ' Huyện Vĩnh Hưng', 797),
(798, 80, ' Huyện Mộc Hóa', 798),
(799, 80, ' Huyện Tân Thạnh', 799),
(800, 80, ' Huyện Thạnh Hóa', 800),
(801, 80, ' Huyện Đức Huệ', 801),
(802, 80, ' Huyện Đức Hòa', 802),
(803, 80, ' Huyện Bến Lức', 803),
(804, 80, ' Huyện Thủ Thừa', 804),
(805, 80, ' Huyện Tân Trụ', 805),
(806, 80, ' Huyện Cần Đước', 806),
(807, 80, ' Huyện Cần Giuộc', 807),
(808, 80, ' Huyện Châu Thành', 808),
(815, 82, ' Thành phố Mỹ Tho', 815),
(816, 82, ' Thị xã Gò Công', 816),
(817, 82, ' Thị xã Cai Lậy', 817),
(818, 82, ' Huyện Tân Phước', 818),
(819, 82, ' Huyện Cái Bè', 819),
(820, 82, ' Huyện Cai Lậy', 820),
(821, 82, ' Huyện Châu Thành', 821),
(822, 82, ' Huyện Chợ Gạo', 822),
(823, 82, ' Huyện Gò Công Tây', 823),
(824, 82, ' Huyện Gò Công Đông', 824),
(825, 82, ' Huyện Tân Phú Đông', 825),
(829, 83, ' Thành phố Bến Tre', 829),
(831, 83, ' Huyện Châu Thành', 831),
(832, 83, ' Huyện Chợ Lách', 832),
(833, 83, ' Huyện Mỏ Cày Nam', 833),
(834, 83, ' Huyện Giồng Trôm', 834),
(835, 83, ' Huyện Bình Đại', 835),
(836, 83, ' Huyện Ba Tri', 836),
(837, 83, ' Huyện Thạnh Phú', 837),
(838, 83, ' Huyện Mỏ Cày Bắc', 838),
(842, 84, ' Thành phố Trà Vinh', 842),
(844, 84, ' Huyện Càng Long', 844),
(845, 84, ' Huyện Cầu Kè', 845),
(846, 84, ' Huyện Tiểu Cần', 846),
(847, 84, ' Huyện Châu Thành', 847),
(848, 84, ' Huyện Cầu Ngang', 848),
(849, 84, ' Huyện Trà Cú', 849),
(850, 84, ' Huyện Duyên Hải', 850),
(851, 84, ' Thị xã Duyên Hải', 851),
(855, 86, ' Thành phố Vĩnh Long', 855),
(857, 86, ' Huyện Long Hồ', 857),
(858, 86, ' Huyện Mang Thít', 858),
(859, 86, ' Huyện  Vũng Liêm', 859),
(860, 86, ' Huyện Tam Bình', 860),
(861, 86, ' Thị xã Bình Minh', 861),
(862, 86, ' Huyện Trà Ôn', 862),
(863, 86, ' Huyện Bình Tân', 863),
(866, 87, ' Thành phố Cao Lãnh', 866),
(867, 87, ' Thành phố Sa Đéc', 867),
(868, 87, ' Thị xã Hồng Ngự', 868),
(869, 87, ' Huyện Tân Hồng', 869),
(870, 87, ' Huyện Hồng Ngự', 870),
(871, 87, ' Huyện Tam Nông', 871),
(872, 87, ' Huyện Tháp Mười', 872),
(873, 87, ' Huyện Cao Lãnh', 873),
(874, 87, ' Huyện Thanh Bình', 874),
(875, 87, ' Huyện Lấp Vò', 875),
(876, 87, ' Huyện Lai Vung', 876),
(877, 87, ' Huyện Châu Thành', 877),
(883, 89, ' Thành phố Long Xuyên', 883),
(884, 89, ' Thành phố Châu Đốc', 884),
(886, 89, ' Huyện An Phú', 886),
(887, 89, ' Thị xã Tân Châu', 887),
(888, 89, ' Huyện Phú Tân', 888),
(889, 89, ' Huyện Châu Phú', 889),
(890, 89, ' Huyện Tịnh Biên', 890),
(891, 89, ' Huyện Tri Tôn', 891),
(892, 89, ' Huyện Châu Thành', 892),
(893, 89, ' Huyện Chợ Mới', 893),
(894, 89, ' Huyện Thoại Sơn', 894),
(899, 91, ' Thành phố Rạch Giá', 899),
(900, 91, ' Thị xã Hà Tiên', 900),
(902, 91, ' Huyện Kiên Lương', 902),
(903, 91, ' Huyện Hòn Đất', 903),
(904, 91, ' Huyện Tân Hiệp', 904),
(905, 91, ' Huyện Châu Thành', 905),
(906, 91, ' Huyện Giồng Riềng', 906),
(907, 91, ' Huyện Gò Quao', 907),
(908, 91, ' Huyện An Biên', 908),
(909, 91, ' Huyện An Minh', 909),
(910, 91, ' Huyện Vĩnh Thuận', 910),
(911, 91, ' Huyện Phú Quốc', 911),
(912, 91, ' Huyện Kiên Hải', 912),
(913, 91, ' Huyện U Minh Thượng', 913),
(914, 91, ' Huyện Giang Thành', 914),
(916, 92, ' Quận Ninh Kiều', 916),
(917, 92, ' Quận Ô Môn', 917),
(918, 92, ' Quận Bình Thuỷ', 918),
(919, 92, ' Quận Cái Răng', 919),
(923, 92, ' Quận Thốt Nốt', 923),
(924, 92, ' Huyện Vĩnh Thạnh', 924),
(925, 92, ' Huyện Cờ Đỏ', 925),
(926, 92, ' Huyện Phong Điền', 926),
(927, 92, ' Huyện Thới Lai', 927),
(930, 93, ' Thành phố Vị Thanh', 930),
(931, 93, ' Thị xã Ngã Bảy', 931),
(932, 93, ' Huyện Châu Thành A', 932),
(933, 93, ' Huyện Châu Thành', 933),
(934, 93, ' Huyện Phụng Hiệp', 934),
(935, 93, ' Huyện Vị Thuỷ', 935),
(936, 93, ' Huyện Long Mỹ', 936),
(937, 93, ' Thị xã Long Mỹ', 937),
(941, 94, ' Thành phố Sóc Trăng', 941),
(942, 94, ' Huyện Châu Thành', 942),
(943, 94, ' Huyện Kế Sách', 943),
(944, 94, ' Huyện Mỹ Tú', 944),
(945, 94, ' Huyện Cù Lao Dung', 945),
(946, 94, ' Huyện Long Phú', 946),
(947, 94, ' Huyện Mỹ Xuyên', 947),
(948, 94, ' Thị xã Ngã Năm', 948),
(949, 94, ' Huyện Thạnh Trị', 949),
(950, 94, ' Thị xã Vĩnh Châu', 950),
(951, 94, ' Huyện Trần Đề', 951),
(954, 95, ' Thành phố Bạc Liêu', 954),
(956, 95, ' Huyện Hồng Dân', 956),
(957, 95, ' Huyện Phước Long', 957),
(958, 95, ' Huyện Vĩnh Lợi', 958),
(959, 95, ' Thị xã Giá Rai', 959),
(960, 95, ' Huyện Đông Hải', 960),
(961, 95, ' Huyện Hoà Bình', 961),
(964, 96, ' Thành phố Cà Mau', 964),
(966, 96, ' Huyện U Minh', 966),
(967, 96, ' Huyện Thới Bình', 967),
(968, 96, ' Huyện Trần Văn Thời', 968),
(969, 96, ' Huyện Cái Nước', 969),
(970, 96, ' Huyện Đầm Dơi', 970),
(971, 96, ' Huyện Năm Căn', 971),
(972, 96, ' Huyện Phú Tân', 972),
(973, 96, ' Huyện Ngọc Hiển', 973);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `province_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `update_at` date NOT NULL,
  `note` varchar(255) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `order_items`
--

CREATE TABLE `order_items` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content` text,
  `created_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `content` text,
  `created_at` date DEFAULT NULL,
  `update_at` date DEFAULT NULL,
  `post_category_id` int(11) NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `is_featured` tinyint(4) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `post_categories`
--

CREATE TABLE `post_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `parent_id` int(11) DEFAULT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `sku` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `price` decimal(10,0) DEFAULT NULL,
  `colors` varchar(255) DEFAULT NULL,
  `sizes` varchar(255) DEFAULT NULL,
  `qty` int(11) NOT NULL DEFAULT '0',
  `brand_id` int(11) DEFAULT NULL,
  `product_category_id` int(11) NOT NULL,
  `description` text,
  `content` text,
  `rate` tinyint(4) DEFAULT '0',
  `is_new` tinyint(4) NOT NULL DEFAULT '0',
  `is_promotion` tinyint(4) NOT NULL DEFAULT '0',
  `is_featured` tinyint(4) NOT NULL DEFAULT '0',
  `is_sale` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `sku`, `name`, `slug`, `price`, `colors`, `sizes`, `qty`, `brand_id`, `product_category_id`, `description`, `content`, `rate`, `is_new`, `is_promotion`, `is_featured`, `is_sale`, `created_at`, `updated_at`, `meta_title`, `meta_keyword`, `meta_description`, `status`) VALUES
(1, ' 3399169416577', ' Áo Sơ Mi Nam Tay Dài VFASHION VFS0004 - Đen', ' ao-so-mi-nam-tay-dai-vfashion-vfs0004-den', '129000', ' black', ' L|M|XL|XXL', 10, 2, 1, 'Chất lụa trơn, không nhăn, không xù, không phai màu. Form body Hàn Quốc, dễ kết hợp với các loại quần', 'Áo Sơ Mi Nam Tay Dài VFASHION VFS0004 - Đen giúp bạn nam trông lịch lãm và sang trọng hơn với thiết kế sang trọng, gia công sắc sảo. Form slim ôm gọn gàng, trẻ trung, tay dài thanh lịch. Sản phẩm may từ vải kate cao cấp, được biết đến với đặc tính thấm hút mồ hôi tốt, thoáng mát ngay cả trong điều kiện thời thiết nóng bức cho form áo đứng, sang trọng. Bề mặt vải mềm mịn. Vải kate còn có đặc tính chống bám bẩn và chống mài mòn tốt, đảm bảo sự bền đẹp với thời gian, dễ giặt, nhanh khô.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(2, ' 4652351933160', ' Áo Sơ Mi Nam Đẳng Cấp VFASHION VFS0034 - Caro Xanh', ' ao-so-mi-nam-dang-cap-vfashion-vfs0034-caro-xanh', '149000', ' blue', ' L|M|XL|XXL', 10, 2, 1, 'Chất liệu cotton pha nhẹ, thoáng mát. Form slim fit, dễ kết hợp với các loại quần', 'Áo Sơ Mi Nam Đẳng Cấp VFASHION VFS0034 - Caro Xanh giúp bạn nam trông lịch lãm và sang trọng hơn với thiết kế thời trang, gia công sắc sảo. Form slim ôm gọn gàng, trẻ trung, tay dài thanh lịch. Sản phẩm may từ vải cotton pha, được biết đến với đặc tính thoáng mát ngay cả trong điều kiện thời thiết nóng bức cho form áo đứng, sang trọng. Bề mặt vải mềm mịn, đảm bảo sự bền đẹp với thời gian, dễ giặt, nhanh khô.', 0, 1, 1, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(3, ' 3390812094418', ' Áo Sơ Mi Nam Tay Dài VFASHION VFS0001 - Trắng', ' ao-so-mi-nam-tay-dai-vfashion-vfs0001-trang', '129000', ' white', ' L|M|XL|XXL', 10, 2, 1, 'Chất lụa trơn, không nhăn, không xù, không phai màu. Form body Hàn Quốc, dễ kết hợp với các loại quần', 'Áo Sơ Mi Nam Tay Dài VFASHION VFS0001 - Trắng giúp bạn nam trông lịch lãm và sang trọng hơn với thiết kế sang trọng, gia công sắc sảo. Form slim ôm gọn gàng, trẻ trung, tay dài thanh lịch. Sản phẩm may từ vải kate cao cấp, được biết đến với đặc tính thấm hút mồ hôi tốt, thoáng mát ngay cả trong điều kiện thời thiết nóng bức cho form áo đứng, sang trọng. Bề mặt vải mềm mịn. Vải kate còn có đặc tính chống bám bẩn và chống mài mòn tốt, đảm bảo sự bền đẹp với thời gian, dễ giặt, nhanh khô.', 0, 1, 0, 1, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(4, ' 3623234166396', ' Áo Sơ Mi Nam Tay Dài Việt Tiến - Xanh Biển', ' ao-so-mi-nam-tay-dai-viet-tien-xanh-bien', '320000', ' blue', ' L|M|XL|XXL', 10, 2, 1, 'Chất liệu 65% Poly 35% Cotton cao cấp. Thiết kế thời trang lịch thiệp, tinh tế', 'Áo Sơ Mi Nam Tay Dài Việt Tiến luôn là sự lựa chọn hàng đầu của giới công sở hiện nay, với kiểu dáng lịch thiệp, đường may đẹp và chất lượng vải cao cấp. Áo sơ mi Việt Tiến giúp bạn nam trông lịch lãm và sang trọng hơn với thiết kế sang trọng, gia công sắc sảo. Form ôm gọn gàng, trẻ trung, tay dài thanh lịch. Sản phẩm may từ chất liệu 65% Poly 35% Cotton cao cấp, được biết đến với đặc tính thấm hút mồ hôi tốt, thoáng mát ngay cả trong điều kiện thời thiết nóng bức cho form áo đứng, sang trọng. Bề mặt vải mềm mịn. Vải còn có đặc tính chống bám bẩn và chống mài mòn tốt, đảm bảo sự bền đẹp với thời gian, dễ giặt, nhanh khô.', 0, 1, 0, 0, 1, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(5, ' 9896812959756', ' Áo Sơ Mi Nam Tay Ngắn VFASHION VFS0008 - Xanh Nhạt', ' ao-so-mi-nam-tay-ngan-vfashion-vfs0008-xanh-nhat', '129000', ' blue', ' L|M|XL|XXL', 10, 2, 1, 'Chất kate trơn, không nhăn, không xù, không phai màu. Form body Hàn Quốc, dễ kết hợp với các loại quần', 'Áo Sơ Mi Nam Tay Ngắn VFASHION VFS0008 - Xanh Nhạt giúp bạn nam trông lịch lãm và sang trọng hơn với thiết kế sang trọng, gia công sắc sảo. Form body ôm gọn gàng, trẻ trung, tay ngắn thanh lịch. Sản phẩm may từ vải kate cao cấp, được biết đến với đặc tính thấm hút mồ hôi tốt, thoáng mát ngay cả trong điều kiện thời thiết nóng bức cho form áo đứng, sang trọng. Bề mặt vải mềm mịn. Vải kate còn có đặc tính chống bám bẩn và chống mài mòn tốt, đảm bảo sự bền đẹp với thời gian, dễ giặt, nhanh khô.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(6, ' 2318191594696', ' Áo Sơ Mi Nam Tay Dài VFASHION VFS0023 - Xanh Ngọc Bích', ' ao-so-mi-nam-tay-dai-vfashion-vfs0023-xanh-ngoc-bich', '129000', ' #35b49a', ' L|M|XL|XXL', 10, 2, 1, 'Chất liệu vải kate trơn mịn, thấm hút tốt. Form body Hàn Quốc, dễ kết hợp với các loại quần', 'Áo Sơ Mi Nam Tay Dài VFASHION VFS0023 giúp bạn nam trông lịch lãm và sang trọng hơn với thiết kế thời trang, gia công sắc sảo. Form slim ôm gọn gàng, trẻ trung, tay dài thanh lịch. Sản phẩm may từ vải kate cao cấp, được biết đến với đặc tính thấm hút mồ hôi tốt, thoáng mát ngay cả trong điều kiện thời thiết nóng bức cho form áo đứng, sang trọng. Bề mặt vải mềm mịn. Vải kate còn có đặc tính chống bám bẩn và chống mài mòn tốt, đảm bảo sự bền đẹp với thời gian, dễ giặt, nhanh khô.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(7, ' 3392937588346', ' Áo Thun Nam Sát Nách PERVIC', ' ao-thun-nam-sat-nach-pervic', '99000', ' black', ' L|M|XL|XXL', 10, 2, 1, ' Thiết kế đơn giản. Chất liệu thoáng mát. Thiết kế phong cách khỏe khoắn', 'Áo Thun Nam Sát Nách PERVIC có thiết kế áo trơn đơn giản, cổ V, tay sát nách tôn lên sự khỏe khoắn, năng động. Phom áo vừa vặn tôn lên thân hình khỏe mạnh của đấng mày râu. Áo được làm từ chất liệu 65% Cotton, 35% Spandex thấm hút mồ hôi tốt, không gây hầm bí. Áo thích hợp cho bạn mặc khi chơi thể thao hoặc mặc ở nhà.', 0, 1, 0, 1, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(8, ' 4008075375395', ' Combo Ewin ET007: 5 Quần Mặc Nhà + 1 Áo Lá Cho Nam', ' combo-ewin-et007-5-quan-mac-nha-1-ao-la-cho-nam', '289000', ' random', ' L|M|XL|XXL', 10, 2, 1, 'Thiết kế kẻ caro thanh lịch. Cotton mềm mại và thấm hút tốt mồ hôi. Combo đầy đủ, tiện lợi cho quý ông', 'Combo Ewin ET007: 5 Quần Mặc Nhà + 1 Áo Lá Cho Nam có màu sắc trang nhã với thiết kế caro kẻ sọc nhỏ mang lại vẻ thời trang cho phái mạnh. Quần được thiết kế vừa vặn và tinh tế với những đường may sắc sảo, đảm bảo bộ bền chắc và tính thẩm mỹ của một dòng sản phẩm cao cấp, chắc chắn sẽ làm hài lòng mọi quý ông. Với chất liệu vải cotton cao cấp, cho độ co giãn và thoáng mát, sản phẩm đem lại cảm giác thoải mái và tự tin cho người mặc trong sử hàng ngày hay cả khi tập thể thao, vận động mạnh.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(9, ' 4006119885138', ' Combo Ewin EQ004: 6 Quần Lót Bikini + 4 Quần Boxer + 2 Quần Mặc Nhà Cho Nam', ' combo-ewin-eq004-6-quan-lot-bikini-4-quan-boxer-2-quan-mac-nha-cho-nam', '339000', ' random', ' L|M|XL|XXL', 10, 2, 1, ' Combo đầy đủ và tiện lợi cho các quý ông. Thiết kế tinh tế và trang nhã. Mang đến sự mềm mại và thoải mái', 'Combo Ewin EQ004: 6 Quần Lót Bikini + 4 Quần Boxer + 2 Quần Mặc Nhà Cho Nam được làm từ chất liệu cotton, cho độ co giãn tốt cũng như độ bền cao. Quần có lưng thun co giãn 4 chiều, không gây bó hay hằn ở vùng tam giác, giúp bạn nam luôn cảm thấy thoải mái tối ưu khi đi làm, đi chơi hoặc vận động thể thao. Sản phẩm được thiết kế đơn giản nhưng không kém phần tinh tế với những đường may sắc sảo, đảm bảo bộ bền chắc và tính thẩm mỹ đồng thời cũng rất tiện lợi với bộ combo đầy đủ.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(10, ' 4000558579580', ' Combo Ewin EQ005: 5 Quần Lót Bikini + 5 Quần boxer + 1 Áo Lá Cho Nam', ' combo-ewin-eq005-5-quan-lot-bikini-5-quan-boxer-1-ao-la-cho-nam', '339000', ' random', ' L|M|XL|XXL', 10, 2, 1, ' Combo đầy đủ và tiện lợi cho các quý ông. Thiết kế tinh tế và trang nhã. Mang đến sự mềm mại và thoải mái', 'Combo Ewin EQ005: 5 Quần Lót Bikini + 5 Quần boxer + 1 Áo Lá Cho Nam có màu sắc cùng kiểu dáng đơn giản tôn lên nét lịch lãm và phong cách nam tính cho người mặc. Quần được thiết kế vừa vặn với chất liệu vải thun cao cấp, cho độ co giãn và thoáng mát giúp người mặc luôn cảm thấy thoải mái và tự tin trong sử hàng ngày hay cả khi tập thể thao, vận động mạnh. Combo gồm 5 Quần Lót Bikini + 5 Quần boxer ngoài ra còn tặng kèm một áo lá cho nam.', 0, 1, 1, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(11, ' 3902680588069', ' Đầm Công Sở Phối Sọc Eden D120HN - Hồng Nhạt', ' dam-cong-so-phoi-soc-eden-d120hn-hong-nhat', '299000', ' pink', ' L|M|XL|XXL', 10, 2, 2, 'Chất liệu vải tuyết mưa co giãn tốt, giữ màu hiệu quả. Dáng đầm suông không kén dáng, vận động thoải mái. Thiết kế cổ tròn, tay ngắn năng động và trẻ trung', 'Đầm Công Sở Phối Sọc Eden D120HN - Hồng Nhạt thiết kế cổ tròn phối tay ngắn trẻ trung, tông màu tinh tế và nhã nhặn mang đến cho bạn nhiều sự lựa chọn hơn khi cần phối với túi xách, đồng hồ, giày, áo khoác,... để xuống phố hay đi làm. Chất liệu vải tuyết mưa thoáng mát, co giãn tốt. Khi mặc không bị nhăn, không bám lông hay bụi. Chất liệu vải này còn có tính năng giữ màu tốt, không bị sờn mốc trong khi sử dụng. Đường may tỉ mỉ và chắc chắn, chống bung đứt. Form đầm suông, vừa giúp bạn vận động thoái mái, vừa che đi khuyết điểm cơ thể hiệu quả. Tông màu ngọt ngào và xinh xắn, thích hợp để mặc đi làm, dạo phố hay hẹn hò.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(12, ' 3620757778488', ' Đầm Ren Hoa Sọc Carita PAL0039 - Trắng', ' dam-ren-hoa-soc-carita-pal0039-trang', '329000', ' white', ' L|M|XL|XXL', 10, 2, 2, 'Chất liệu vải ren thun êm ái, mềm mịn. Thiết kế dáng bút chì tôn dáng thanh lịch', 'Đầm Ren Hoa Sọc Carita PAL0039 - Trắng có thiết kế dáng bút chì thanh lịch, họa tiết hoa ren kết hợp cùng tông màu nền nã giúp các nàng dễ dàng phối cùng nhiều kiểu giày hay túi xách. Chất liệu vải ren thun mềm mịn, độ bền màu cao, hạn chế bị nhăn nhàu hay xù lông sau khi giặt. Form đầm ôm nhẹ, tôn vóc dáng thon gọn cũng như tạo sự thoải mái trong từng cử động của cơ thể.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(13, ' 2707388262232', ' Áo Cung Hoàng Đạo Item Du Jour Song Tử ASD00FSOT - Trắng (Freesize)', ' ao-cung-hoang-dao-item-du-jour-song-tu-asd00fsot-trang-freesize', '245000', ' white', ' L|M|XL|XXL', 10, 2, 2, ' Áo Cung Hoàng Đạo Item Du Jour Song Tử ASD00FSOT - Trắng (Freesize)', 'Áo Cung Hoàng Đạo Item Du Jour Song Tử ASD00FSOT - Trắng (Freesize) là món quà thú vị dành cho những cô nàng thuộc cung hoàng đạo Song Tử khi in biểu tượng “Tôi là Song Tử” ngay mặt trước thật nổi bật. Mặt sau áo in dòng chữ “Hay nói dối, giỏi ứng đối” vui nhộn. Áo được may từ chất liệu vải 100% cotton cho độ mềm mại và thấm hút mồ hôi cực tốt, bạn sẽ thoải mái hơn khi mặc.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(14, ' 2703604408453', ' Áo Cung Hoàng Đạo Item Du Jour Sư Tử ASD00FST - Đen (Freesize)', ' ao-cung-hoang-dao-item-du-jour-su-tu-asd00fst-den-freesize', '245000', ' black', ' L|M|XL|XXL', 10, 2, 2, ' Chất liệu vải 100% cotton. Form rộng không kén dáng', 'Áo Cung Hoàng Đạo Item Du Jour Sư Tử ASD00FST - Đen (Freesize) có màu đen tinh tế cho bạn gái phong cách cuốn hút và mạnh mẽ hơn. Áo in biểu tượng “Tôi là Sư Tử” là món quà thú vị cho các bạn nữ thuộc cung hoàng đạo này. Bề mặt áo mềm mại và thấm hút mồ hôi tốt sẽ làm bạn hài lòng khi mặc. May từ chất liệu vải 100% cotton cho độ mềm mại và thấm hút mồ hôi cực tốt giúp bạn cảm nhận sự thoáng mát tối ưu dù là mặc trong ngày hè nóng bức. Chất liệu này dễ giặt sạch, nhanh khô, tiết kiệm thời gian giặt giũ. In biểu tượng “Tôi là Sư Tử” ở mặt trước và “Ghét bọn không biết ninh, ghét bọn nịnh không thật lòng” ở sau lưng Màu hồng ngọt ngào mang đến phong cách nữ tính, đáng yêu cho chị em phụ nữ. Form áo rộng dễ mặc, không kén dáng đồng thời mang lại sự thoải mái khi vận động. Kiểu tay ngắn và cổ tròn mang lại vẻ năng động, khỏe khoắn cho người mặc. Dễ dàng phối cùng jeans dài, short và cả chân váy tùy sở thích. Áo mang đến cho bạn nữ vẻ ngoài trẻ trung để tự tin hơn khi đi học, đi chơi hoặc đi làm.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(15, ' 4555507694529', ' Đầm Nữ Mango 41063519 - Xanh (Size 20)', ' dam-nu-mango-41063519-xanh-size-20', '510000', ' blue', ' L|M|XL|XXL', 10, 2, 2, 'Kiểu dáng trẻ trung, duyên dáng. Thiết kế cổ tim, tay sát nách quyến rũ. Form đầm suông nhẹ, phù hợp với nhiều dáng người. Dây khóa kéo may chìm phía sau tinh tế, dễ thao tác. Màu xanh đen trung tính dễ mix với nhiều phụ kiện', 'Đầm Nữ Mango 41063519 - Xanh (Size 20) sở hữu kiểu dáng trẻ trung, duyên dáng, mang đến cho người mặc vẻ ngoài thanh lịch, cuốn hút và nổi bật. hiết kế cổ tim, tay sát nách, form đầm suông nhẹ, phù hợp với nhiều dáng người đồng thời tôn lên những đường nét quyến rũ tinh tế. Chất liệu mềm mịn, thoáng mát tạo cảm giác thoải mái cho người mặc. Bên cạnh đó, đường may được gia công kỹ lưỡng, chắc chắn, khóa kéo may chìm phía sau tinh tế, dễ thao tác mang đến giá trị sử dụng dài lâu. Màu xanh đen trung tính, thích hợp diện cùng nhiều phụ kiện khác khi đi tiệc, đến nơi công sở,...', 0, 1, 0, 0, 1, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(16, ' 9876195284508', ' Đầm Ren Xòe Tay Lửng Amun DX185 - Trắng (Freesize)', ' dam-ren-xoe-tay-lung-amun-dx185-trang-freesize', '319000', ' white', ' L|M|XL|XXL', 10, 2, 2, 'Chất liệu ren mềm mại, thẩm mỹ cao. Thiết kế cổ tròn, tay lửng kín đáo. Form xòe, phối viền cổ và eo tạo điểm nhấn. Dây kéo sau lưng tiện lợi, dễ mặc', 'Đầm Ren Xòe Tay Lửng Amun DX185 sở hữu kiểu dáng đầm suông theo phong cách tối giản mang lại cho bạn nữ nét đẹp dịu dàng, nữ tính. Thiết kế dáng đầm suông cổ tròn, tay lửng cùng viền cổ và eo khác màu nổi bật, nữ tính, sau lưng có dây kéo tiện lợi giúp bạn nữ thêm duyên dáng, tự tin khi bước xuống phố. Đầm được may từ chất liệu ren mềm mại, thoáng mát tạo cảm giác thoải mái, dễ chịu cho người mặc.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(17, ' 5977637521022', ' Đầm Xòe Voan Lưới Phối Ren Cổ Tròn Tay Ngắn Amun DX144 - Trắng (Freesize)', ' dam-xoe-voan-luoi-phoi-ren-co-tron-tay-ngan-amun-dx144-trang-freesize', '339000', ' white', ' L|M|XL|XXL', 10, 2, 2, 'Chất liệu voan lưới phối ren mềm mại. Kiểu dáng cổ tròn, tay ngắn thoải mái. Eo đính nơ tạo điểm nhấn ấn tượng. Đầm có lớp lót mịn mát, kín đáo', 'Đầm Xòe Voan Lưới Phối Ren Cổ Tròn Tay Ngắn Amun DX144 sở hữu thiết kế nhẹ nhàng và thời trang, dáng đầm xòe trẻ trung. Gam màu trang nhã đi cùng chất liệu cao cấp giúp người mặc luôn quyến rũ và duyên dáng. Chất liệu voan lưới phối ren mềm mại cho cảm giác nhẹ nhàng, thoải mái khi mặc. Thiết kế cổ tròn, tay ngắn với eo đính nơ tạo điểm nhấn. Form dáng đầm vừa vặn cho bạn nữ thêm phần duyên dáng khi mặc.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(18, ' 8412189212617', ' Đầm Suông Cổ Vest Không Tay Amun DMI133 - Đen (Freesize)', ' dam-suong-co-vest-khong-tay-amun-dmi133-den-freesize', '239000', ' black', ' L|M|XL|XXL', 10, 2, 2, 'Kiểu dáng đầm suông thời trang. Chất liệu thun hai da thoáng mát, dễ là ủi. Thiết kế cổ vest sang trọng, sát nách thoải mái. Đắp túi giả tạo điểm nhấn, vạt chéo đẹp mắt', 'Đầm Suông Cổ Vest Không Tay Amun DMI133 có kiểu dáng trẻ trung với các chi tiết nổi bật làm điểm nhấn đem đến cho bạn gái một phong cách năng động, hợp thời trang. Sở hữu kiểu dáng đầm suông theo phong cách tối giản mang lại cho bạn nữ nét đẹp dịu dàng, nữ tính. Đầm được may từ chất liệu thun hai da thoáng mát tạo cảm giác thoải mái, dễ chịu cho người mặc. Thiết kế dáng đầm suông cổ tròn, tay sát nách cùng cổ vest sang trọng, đắp túi giả, vạt chéo giúp bạn nữ thêm duyên dáng, tự tin khi bước xuống phố.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(19, ' 5610623407862', ' Đầm Elisa TT Ladies ĐX18-T1 (Tím)', ' dam-elisa-tt-ladies-dx18-t1-tim', '949000', ' purple', ' L|M|XL|XXL', 10, 2, 2, 'Thiết kế cổ tròn phối tay ren, họa tiết thêu nổi bật. Form suông che khuyết điểm, thoải mái vận động. Chất liệu vải gấm mềm mịn, thấm hút mồ hôi nhanh chóng', 'Đầm Elisa TT Ladies ĐX18-T1 (Tím) được lấy ý tưởng từ loài hoa hồng vàng là hiện thân của nàng Elisa xinh đẹp, thông minh, con của thần Zeus với một thiếu nữ trần gian trong thần thoại Hy Lạp. Màu sắc cánh hồng không phải là đỏ tươi thắm thiết, không phải hồng phấn dịu dàng mà là màu vàng mãnh liệt cháy lòng. TT Ladies mong muốn mang đến sự cao quý của hoa hồng vàng trên sản phẩm Đầm Elisa. Đặc biệt, họa tiết hoa hồng vàng rực rỡ ở giữa làm cho người mặc vừa có nét hiện đại vừa sang trọng. Đầm có thiết kế dáng suông tinh tế kết hợp tay dài phối ren cách điệu che đi những khuyết điểm cơ thể chưa hoàn hảo giúp bạn thêm tự tin, thoải mái. Bên cạnh đó, chất liệu gấm Hàn Quốc không gây kích ứng da, chống nhăn, hút ẩm nhanh, thấm mồ hôi, giảm nhiệt và làm mát cơ thể mang đến cảm giác thoải mái, dễ chịu tối đa.', 0, 1, 0, 1, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(20, ' 2634395348614', ' Váy Chống Nắng Dạng Quần Thông Minh Panda VCN-01 - Xanh Than', ' vay-chong-nang-dang-quan-thong-minh-panda-vcn-01-xanh-than', '65000', ' blue', ' L|M|XL|XXL', 10, 2, 2, 'Cấu tạo 2 lớp, có đường xẻ chính giữa. Thay đổi váy thành quần được. Thao tác đơn giản, dễ thực hiện', 'Váy Chống Nắng Dạng Quần Thông Minh Panda VCN-01 - Xanh Than được cấu tạo thành 2 lớp, có đường xẻ ở chính giữa, có thể mặc lớp ngoài giống như các loại váy chống nắng thông thường, lại vừa có thể biến hóa chiếc váy thành 1 chiếc quần. Thao tác đơn giản chỉ với 2 thao tác nhỏ là kéo lớp vải bên trong và cài sang 2 bên, ngoài công dụng chống nắng, loại váy này sẽ giúp bạn bảo vệ đôi chân tránh bị bỏng do pô xe hoặc trầy xước khi té ngã.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(21, ' 4827441564962', ' Giày Sneaker Thể Thao Nam Zapas GS068BU - Xanh', ' giay-sneaker-the-thao-nam-zapas-gs068bu-xanh', '119000', ' blue', ' L|M|XL|XXL', 10, 3, 3, 'Có độ bền cao và kiểu dáng năng động. Form giày ôm bảo vệ chân tốt. Chất liệu vải sợi cao cấp, mềm mại. Đế giày làm bằng chất liệu cao su tổng hợp', 'Giày Sneaker Thể Thao Nam Zapas GS068BU - Xanh được thiết kế trẻ trung, năng động và sành điệu, khẳng định phong cách thời trang riêng của phái mạnh. Tông màu nam tính, đẳng cấp cùng chất liệu vải mềm mại, thoáng mát, không mang đến cảm giác đau chân khi phải di chuyển nhiều, sản phẩm tạo nên sự tin tưởng tuyệt đối trong sự lựa chọn của giới trẻ thời nay. Thân giày được may từ vải sợi cao cấp vừa bền chắc vừa dễ giặt sạch và khó phai màu. Đế giày làm bằng chất liệu cao su tổng hợp có xẻ rãnh chống trơn trượt, tạo sự êm ái và nhẹ nhàng khi di chuyển. Giày có kiểu dáng thể thao với mũi tròn và form ôm sát, bảo vệ chân tốt hơn đồng thời tôn lên vẻ năng động, khỏe khoắn. Kiểu thắt dây thật tiện lợi khi cần điều chỉnh độ rộng của giày.', 0, 1, 1, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(22, ' 4829545463158', ' Giày Sneaker Thể Thao Nam Zapas GS068GR - Xám', ' giay-sneaker-the-thao-nam-zapas-gs068gr-xam', '119000', ' gray', ' L|M|XL|XXL', 10, 3, 3, 'Có độ bền cao và kiểu dáng năng động. Form giày ôm bảo vệ chân tốt. Chất liệu vải sợi cao cấp, mềm mại', 'Giày Sneaker Thể Thao Nam Zapas GS068GR - Xám được thiết kế trẻ trung, năng động và sành điệu, khẳng định phong cách thời trang riêng của phái mạnh. Tông màu nam tính, đẳng cấp cùng chất liệu vải mềm mại, thoáng mát, không mang đến cảm giác đau chân khi phải di chuyển nhiều, sản phẩm tạo nên sự tin tưởng tuyệt đối trong sự lựa chọn của giới trẻ thời nay. Thân giày được may từ vải sợi cao cấp vừa bền chắc vừa dễ giặt sạch và khó phai màu. Đế giày làm bằng chất liệu cao su tổng hợp có xẻ rãnh chống trơn trượt, tạo sự êm ái và nhẹ nhàng khi di chuyển. Giày có kiểu dáng thể thao với mũi tròn và form ôm sát, bảo vệ chân tốt hơn đồng thời tôn lên vẻ năng động, khỏe khoắn. Kiểu thắt dây thật tiện lợi khi cần điều chỉnh độ rộng của giày.', 0, 1, 0, 1, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(23, ' 8033099631239', ' Giày Mọi Nam Da Bò SUNPOLO SU7542D - Đen', ' giay-moi-nam-da-bo-sunpolo-su7542d-den', '639000', ' black', ' L|M|XL|XXL', 10, 3, 3, 'Chất liệu da bò màu đen bền đẹp. Đế cao su cao 2.5cm chống trơn trượt. Lót chân bằng da thật mềm mại cân đối. Thiết kế xỏ chân nam tính, mạnh mẽ. Giày viền chỉ nổi tinh tế, sắc sảo', 'Giày Mọi Nam Da Bò SUNPOLO SU7542D với chất liệu da bò bền đẹp kết hợp đế cao su êm nhẹ chống trơn trượt cùng thiết kế xỏ chân đặc trưng của giày lười giúp bạn nam luôn thoải mái, tự tin khi đi chơi, dạo phố, đi làm nơi công sở hay đi dự tiệc. Giày được thiết kế tinh xảo với đường may chắc chắn, kiểu dáng xỏ chân trẻ trung, tiện lợi. Phần lót chân bằng da thật mềm mại, cân đối tạo sự êm ái trên mỗi bước đi. Đế giày cao 2.5cm giúp bạn tăng chiều cao một cách tinh tế và nhanh chóng, cho các anh chàng có chiều cao khiêm tốn thêm phần tự tin khi bước đi. Giày với một tông màu chủ đạo giúp cho việc phối đồ trở nên dễ dàng hơn, bạn có thể kết hợp với các loại trang phục hằng ngày tùy theo sở thích và phong cách mix & match riêng biệt của mình.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(24, ' 4822146539628', ' Giày Sneaker Thể Thao Unisex Zapas GS062BA - Đen', ' giay-sneaker-the-thao-unisex-zapas-gs062ba-den', '139000', ' black', ' L|M|XL|XXL', 10, 3, 3, 'Có độ bền cao và kiểu dáng năng động. Form giày ôm bảo vệ chân tốt. Chất liệu vải sợi cao cấp, mềm mại', 'Giày Sneaker Thể Thao Unisex Zapas GS062BA - Đen  được thiết kế trẻ trung, năng động và sành điệu, khẳng định phong cách thời trang riêng của phái mạnh. Tông màu nam tính, đẳng cấp cùng chất liệu vải mềm mại, thoáng mát, không mang đến cảm giác đau chân khi phải di chuyển nhiều, sản phẩm tạo nên sự tin tưởng tuyệt đối trong sự lựa chọn của giới trẻ thời nay. Thân giày được may từ vải sợi cao cấp vừa bền chắc vừa dễ giặt sạch và khó phai màu. Đế giày làm bằng chất liệu cao su tổng hợp có xẻ rãnh chống trơn trượt, tạo sự êm ái và nhẹ nhàng khi di chuyển. Giày có kiểu dáng thể thao với mũi tròn và form ôm sát, bảo vệ chân tốt hơn đồng thời tôn lên vẻ năng động, khỏe khoắn. Kiểu thắt dây thật tiện lợi khi cần điều chỉnh độ rộng của giày.', 0, 1, 0, 1, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(25, ' 4704066950373', ' Giày Vải Nam MIDO\'S 79-MD21-BLACK - Đen', ' giay-vai-nam-mido-s-79-md21-black-den', '450000', ' black', ' L|M|XL|XXL', 10, 3, 3, 'Thiết kế giày đơn giản, ôm chân vừa vặn. Giày đan dây thể thao, có thể điều chỉnh độ rộng. Chất liệu vải dày nhưng thoáng khí, không hầm bí khi mang. Đế cao su bền, độ ma sát cao, chống trơn trượt', 'Giày Vải Nam MIDO\'S 79-MD21-BLACK - Đen có kiểu dáng năng động với bề mặt trơn đơn giản, tạo được nét tinh tế, thời trang cho người mang. Form giày ôm vừa vặn, với chất vải dày nhưng thoáng khí, không tạo cảm giác hầm bí khó chịu, đặc biệt là không gây hôi chân. Thân giày được may từ vải cao cấp vừa bền chắc, vừa dễ giặt sạch và không phai màu. Đế cao su bền dẻo, có độ ma sát với mọi bề mặt cao, vừa chống trơn trượt, vừa mang đến cảm giác êm chân, nhẹ nhàng khi di chuyển. Kiểu giày đan dây thể thao, năng động, rất thuận tiện trong các hoạt động thường ngày.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(26, ' 7428182106366', ' Giày Sneakers Nam PASSO GTK065 - Xám', ' giay-sneakers-nam-passo-gtk065-xam', '109000', ' gray', ' L|M|XL|XXL', 10, 3, 3, 'Có độ bền cao và kiểu dáng thể thao năng động. Form giày ôm bảo vệ chân tốt. Chất liệu vải cao cấp, bền chắc. Đế giày làm bằng cao su có rãnh chống trơn trượt', 'Giày Sneakers Nam PASSO GTK065 - Xám được thiết kế trẻ trung, năng động và sành điệu, khẳng định phong cách thời trang riêng của phái mạnh. Tông màu nam tính, đẳng cấp cùng chất liệu thoải mái, mềm mại, không mang đến cảm giác đau chân khi phải di chuyển nhiều, sản phẩm tạo nên sự tin tưởng tuyệt đối trong sự lựa chọn của giới trẻ thời nay. Thân giày được may từ vải vừa bền chắc vừa dễ giặt sạch và khó phai màu. Đế giày làm bằng chất liệu cao su tổng hợp có xẻ rãnh chống trơn trượt, tạo sự êm ái và nhẹ nhàng khi di chuyển. Giày có kiểu dáng thể thao với mũi tròn và form ôm sát, bảo vệ chân tốt hơn đồng thời tôn lên vẻ năng động, khỏe khoắn.', 0, 1, 1, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(27, ' 4709527617802', ' Giày Casual Unisex D&A M1398 - Xanh Dương', ' giay-casual-unisex-d-a-m1398-xanh-duong', '165000', ' blue', ' L|M|XL|XXL', 10, 3, 3, 'Giày Casual Unisex D&A M1398 - Xanh Dương được may từ vải canvas cao cấp đảm bảo độ bền và êm chân khi đi. Giày thoáng khí, không làm bạn bí bách, tiện sử dụng hằng ngày. Với màu xanh dương chủ đạo, giày cho bạn phong cách trẻ trung và khỏe khoắn.', 'Giày Casual Unisex D&A M1398 - Xanh Dương được may từ vải canvas cao cấp đảm bảo độ bền và êm chân khi đi. Giày thoáng khí, không làm bạn bí bách, tiện sử dụng hằng ngày. Với màu xanh dương chủ đạo, giày cho bạn phong cách trẻ trung và khỏe khoắn.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(28, ' 6860011617192', ' Giày Tây Nam Da Bò SUNPOLO SU3020N - Nâu', ' giay-tay-nam-da-bo-sunpolo-su3020n-nau', '680000', ' brown', ' L|M|XL|XXL', 10, 3, 3, 'Chất liệu da bò màu nâu bền đẹp. Đế cao su cao 3.8cm chống trơn trượt. Lót chân bằng da thật mềm mại cân đối. Thiết kế xỏ chân, mũi tròn sang trọng, lịch sự', 'Giày Tây Nam Da Bò SUNPOLO SU3020N với chất liệu da bò bền đẹp kết hợp đế cao su êm nhẹ chống trơn trượt giúp bạn nam luôn thoải mái, tự tin khi đi chơi, dạo phố, đi làm nơi công sở hay đi dự tiệc. Giày được thiết kế tinh xảo với đường may chắc chắn, kiểu dáng xỏ chân, mũi tròn sang trọng, lịch sự. Phần lót chân bằng da thật mềm mại, cân đối tạo sự êm ái trên mỗi bước đi. Đế giày cao 3.8cm giúp bạn tăng chiều cao một cách tinh tế và nhanh chóng, cho các anh chàng có chiều cao khiêm tốn thêm phần tự tin khi bước đi. Giày với một tông màu chủ đạo giúp cho việc phối đồ trở nên dễ dàng hơn, bạn có thể kết hợp với các loại trang phục hằng ngày tùy theo sở thích và phong cách mix & match riêng biệt của mình.', 0, 1, 0, 0, 1, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(29, ' 5952832505442', ' Giày Tây Nam Da Bò SUNPOLO SU119N - Nâu', ' giay-tay-nam-da-bo-sunpolo-su119n-nau', '555000', ' brown', ' L|M|XL|XXL', 10, 3, 3, 'Chất liệu da bò màu nâu bền đẹp. Đế cao su cao 3cm chống trơn trượt. Lót chân bằng da thật mềm mại cân đối. Thiết kế xỏ chân sang trọng, lịch sự. Mũi giày nhọn, mặt da trơn bóng', 'Giày Tây Nam Da Bò SUNPOLO SU119N với chất liệu da bò bền đẹp kết hợp đế cao su êm nhẹ chống trơn trượt giúp bạn nam luôn thoải mái, tự tin khi đi chơi, dạo phố, đi làm nơi công sở hay đi dự tiệc. Giày được thiết kế tinh xảo với đường may chắc chắn, kiểu dáng xỏ chân, mũi nhọn sang trọng, lịch sự. Phần lót chân bằng da thật mềm mại, cân đối tạo sự êm ái trên mỗi bước đi. Đế giày cao 3cm giúp bạn tăng chiều cao một cách tinh tế và nhanh chóng, cho các anh chàng có chiều cao khiêm tốn thêm phần tự tin khi bước đi. Giày với một tông màu chủ đạo giúp cho việc phối đồ trở nên dễ dàng hơn, bạn có thể kết hợp với các loại trang phục hằng ngày tùy theo sở thích và phong cách mix & match riêng biệt của mình.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(30, ' 7627014959938', ' Giày Thể Thao Nam Hàn Quốc Passo GTK058 - Xám', ' giay-the-thao-nam-han-quoc-passo-gtk058-xam', '99000', ' gray', ' L|M|XL|XXL', 10, 3, 3, 'Kiểu dáng năng động, cá tính. Form giày ôm chân, thoải mái hoạt động. Màu sắc trung tính, dễ phối trang phục. Đế cao su nhẹ nhàng, êm ái', 'Giày Thể Thao Nam Hàn Quốc Passo GTK058 - Xám sở hữu kiểu dáng trẻ trung, năng động với màu sắc trung tính, thể hiện nét cá tính, mạnh mẽ trong từng bước chân, khẳng định phong cách thời trang riêng của phái mạnh.Chất liệu cao su bền bỉ, êm nhẹ. Bề mặt giày mềm mại, chắc chắn, bảo vệ chân tốt, mang đến cảm giác thoải mái tối đa. Đế giày thiết kế thông minh với nhiều rãnh nhỏ chống trơn trượt. Giày được cách điệu với phần phối vải tone xám nhạt hơn ôm lấy thân giày, thể hiện nét trẻ trung, năng động. Form giày ôm sát bàn chân nhưng không bị bó chật vì lớp đệm cao su chắc chắn và êm ái ở phần đế. Dễ dàng điều chỉnh độ rộng chật của giày nhờ kiểu thắt dây tiện lợi.', 0, 1, 0, 0, 1, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(31, ' 8006550881658', ' Giày Cao Gót Da Thật A Different Heel 2 Gosto GS0000293BLK - Đen', ' giay-cao-got-da-that-a-different-heel-2-gosto-gs0000293blk-den', '2099000', ' black', ' L|M|XL|XXL', 10, 3, 4, 'Chất liệu da thật bền chắc. Thiết kế ôm sát vừa vặn bàn chân. Chất da mang đến cảm giác êm ái, thoải mái. Kiểu dáng trẻ trung, nữ tính', 'Giày Cao Gót Da Thật A Different Heel 2 Gosto GS0000293BLK - Đen kiểu dáng nữ tính ôm trọn lấy bàn chân, giúp cho từng bước đi của phái đẹp thêm phần quý phái và sang trọng. Giày được gia công cẩn thận giúp gia tăng độ bền đẹp và kéo dài tuổi thọ cho sản phẩm. Được may từ chất liệu da giúp người mang có cảm giác thoáng mát. Chất liệu keo dán được dùng cho thị trường xuất khẩu nên rất chắc chắn, độ bền cao. Form giày may đúng tiêu chuẩn, đem lại sự thoải mái khi mang. Mũi giày thiết kế hở ngón thoáng mát mang lại sự tự tin cho bạn khi bước đi trên đường. Với kiểu dáng đơn giản nên giúp dễ dàng phối hợp với nhiều loại trang phục khác nhau phù hợp với từng hoàn cảnh.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(32, ' 3897347569362', ' Giày Bốt Nữ Da Thật Gosto Cowgirl GS0000314BLK (Đen)', ' giay-bot-nu-da-that-gosto-cowgirl-gs0000314blk-den', '1199000', ' black', ' L|M|XL|XXL', 10, 3, 4, 'Thiết kế bốt cổ thấp phối thun và dây kéo khóa thời trang, trẻ trung. Form ôm chân vừa vặn, lớp đệm mềm mại, êm chân. Đế cao su chống trơn trượt cao 3 phân tôn dáng chân thon dài. chất liệu da thật căng bóng, bền đẹp, dễ dàng lau chùi', 'Giày Bốt Nữ Da Thật Gosto Cowgirl GS0000314BLK (Đen) sở hữu kiểu dáng bốt cổ thấp phối dây kéo khóa thời trang, hiện đại cho phái đẹp luôn nổi bật trẻ trung thu hút mọi ánh nhìn đặc biệt bạn có thể dễ dàng kết hợp với nhiều loại trang phục như short jeans, đầm ngắn, áo form dài cá tính để phù hợp khi đi làm, đi học, dự tiệc hay gặp gỡ bạn bè. Thiết kế mũi giày nhọn, form ôm chân vừa vặn là giải pháp giúp che khuyết điểm đôi bàn chân thô to không hoàn hảo. Ngoài ra, với bốt cổ thấp kết hợp phần đế cao 3 phân giúp tôn dáng chân thon dài phù hợp cho những nàng nấm lùn sở hữu chiều cao khiêm tốn. Bên cạnh đó, phần đế giày được gia công chắc chắn và bền bỉ chống trơn trượt hay mài mòn tạo sự thoải mái khi mang, lớp đệm êm ái bên trong giúp bạn tự tin trên mỗi bước chân tránh những sự cố đáng tiếc có thể xảy ra. Bề mặt giày bóng mịn đẹp mắt, tạo vẻ ngoài sang trọng và thanh lịch cho người mang được làm từ chất liệu da thật cao cấp, chọn lọc kỹ càng và xử lý theo quy trình kỹ thuật công nghệ độc quyền của Gosto.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(33, ' 4869064554890', ' Giày Sandal Cao Gót Hở Mũi Merlyshoes 0811 - Vàng', ' giay-sandal-cao-got-ho-mui-merly-0811-vang', '239000', ' yellow', ' L|M|XL|XXL', 10, 3, 4, ' Form giày mới ôm chân. Quai giày thiết kế cách điệu. Bề mặt giày cấu tạo bởi si mờ', 'Giày Sandal Cao Gót Hở Mũi Merlyshoes 0811 - Vàng là thiết kế với form giày mới ôm chân, mũi tròn xinh xắn dành cho những cô nàng năng động, cá tính. Mẫu giày được cách điệu tinh tế với phần đinh kim tuyến trên quai ấn tượng. Quai giày được thiết kế cách điệu và vô cùng thanh mảnh ôm gọn bàn chân. Nút khoá kim quai hậu tiện dụng khi bạn muốn nới lỏng hoặc thu nhỏ phù hợp với chân. Bề mặt giày được cấu tạo bởi si mờ êm, tạo sự bền đẹp, sang trọng. Phần lót lòng và sau gót được thiết kế bằng chất liệu mút gọn gàng, êm ái ghề tạo cho bạn gái tự tin sải bước ở mọi nơi.', 0, 1, 0, 1, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(34, ' 4709140317424', ' Giày Casual Unisex D&A M1398 - Xanh Chàm', ' giay-casual-unisex-d-a-m1398-xanh-cham', '165000', ' blue', ' L|M|XL|XXL', 10, 3, 4, 'Giày Casual Unisex D&A M1398 - Xanh Chàm được may từ vải canvas cao cấp vừa bền chắc vừa dễ giặt sạch, tiện dùng hằng ngày. Giày có màu xanh chàm đẹp mắt cho bạn phong cách trẻ trung và khác biệt hơn. Form giày ôm vừa phải, không quá gò bò chân khi đi.', 'Giày Casual Unisex D&A M1398 - Xanh Chàm được may từ vải canvas cao cấp vừa bền chắc vừa dễ giặt sạch, tiện dùng hằng ngày. Giày có màu xanh chàm đẹp mắt cho bạn phong cách trẻ trung và khác biệt hơn. Form giày ôm vừa phải, không quá gò bò chân khi đi.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(35, ' 4298984359382', ' Giày Sneakers Nữ PASSO GTK063 - Đỏ', ' giay-sneakers-nu-passo-gtk063-do', '119000', ' red', ' L|M|XL|XXL', 10, 3, 4, 'Kiểu giày thể thao, cột dây năng động. Chất liệu da tổng hợp dày dặn, bền đẹp. Phần đế thiết kế răng cưa hiện đại. Mũi giày tròn, ôm chân thoải mái. Đế giày cao su, có xẽ rãnh cách điệu, chống trơn trượt tốt.', 'Giày Sneakers Nữ PASSO GTK063 - Đỏ sở hữu thiết kế trẻ trung, năng động với gam màu tươi sáng, thể hiện nét cá tính, mạnh mẽ trong từng bước chân, khẳng định phong cách thời trang riêng của phái nữ. Chất liệu da tổng hợp bền bỉ, dễ giặt, nhanh khô và khó phai màu. Đế giày cao su thiết kế thông minh với nhiều rãnh nhỏ chống trơn trượt. Thân giày thiết kế đơn giản tạo cảm giác trẻ trung, hiện đại. Kiếu dáng trẻ trung hiện đại, form giày ôm sát bàn chân nhưng không bị bó chật vì lớp đệm cao su chắc chắn và êm ái ở phần đế. Dễ dàng điều chỉnh độ rộng chật của giày nhờ kiểu thắt dây tiện lợi.', 0, 1, 1, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(36, ' 6937958058767', ' Giày Sneakers Nữ PASSO GTK063 - Vàng', ' giay-sneakers-nu-passo-gtk063-vang', '119000', ' yellow', ' L|M|XL|XXL', 10, 3, 4, 'Kiểu giày thể thao, cột dây năng động. Chất liệu da tổng hợp dày dặn, bền đẹp. Phần đế thiết kế răng cưa hiện đại. Mũi giày tròn, ôm chân thoải mái. Đế giày cao su, có xẽ rãnh cách điệu, chống trơn trượt tốt', 'Giày Sneakers Nữ PASSO GTK063 - Vàng sở hữu thiết kế trẻ trung, năng động với gam màu tươi sáng, thể hiện nét cá tính, mạnh mẽ trong từng bước chân, khẳng định phong cách thời trang riêng của phái nữ. Chất liệu da tổng hợp bền bỉ, dễ giặt, nhanh khô và khó phai màu. Đế giày cao su thiết kế thông minh với nhiều rãnh nhỏ chống trơn trượt. Thân giày thiết kế đơn giản tạo cảm giác trẻ trung, hiện đại. Kiếu dáng trẻ trung hiện đại, form giày ôm sát bàn chân nhưng không bị bó chật vì lớp đệm cao su chắc chắn và êm ái ở phần đế. Dễ dàng điều chỉnh độ rộng chật của giày nhờ kiểu thắt dây tiện lợi.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(37, ' 4866891100986', ' Giày Sneaker Nữ Passo GTK034 - Đen', ' giay-sneaker-nu-passo-gtk034-den', '109000', ' black', ' L|M|XL|XXL', 10, 3, 4, 'Chất liệu vải tổng hợp bền chắc. Phối màu trẻ trung, năng động. Đế giày làm bằng chất liệu cao su tổng hợp', 'Giày Sneaker Nữ Passo GTK034 - Đen được thiết kế trẻ trung, năng động và sành điệu, khẳng định phong cách thời trang riêng của bạn gái. Tông màu thanh lịch, đẳng cấp cùng chất liệu mềm mại, không mang đến cảm giác đau chân khi phải di chuyển nhiều, sản phẩm tạo nên sự tin tưởng tuyệt đối trong sự lựa chọn của giới trẻ thời nay. Thân giày được may từ vải tổng hợp vừa bền chắc, vừa dễ giặt sạch và khó phai màu. Đế giày làm bằng chất liệu cao su tổng hợp có xẻ rãnh chống trơn trượt, tạo sự êm ái và nhẹ nhàng khi di chuyển. Giày có kiểu dáng thể thao với mũi tròn và form ôm sát, bảo vệ chân tốt hơn đồng thời tôn lên vẻ năng động, khỏe khoắn. Kiểu thắt dây thật tiện lợi khi cần điều chỉnh độ rộng của giày.', 0, 1, 0, 1, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(38, ' 2504901606456', ' Giày Đế Xuồng Mũi Vuông Royal Walk RY079', ' giay-de-xuong-mui-vuong-royal-walk-ry079', '279000', ' gray', ' L|M|XL|XXL', 10, 3, 4, 'Được may từ chất liệu da mềm mại. Form giày ôm chân, thoải mái. Thiết kế vá mũi đính kèm nơ đan chéo đầu mũi giày. Đường viền được may phối tinh tế và khéo léo', 'Giày Đế Xuồng Mũi Vuông Royal Walk RY079 với chất liệu da tổng hợp cao cấp, mềm mại và dễ dàng lau chùi, đế chắc chắn chống trượt tốt. Thiết kế vá mũi đính kèm nơ đan chéo đầu mũi giày nữ tính, mũi vuông đơn giản tạo nên vẻ thanh lịch và sang trọng. Đường viền được may phối tinh tế và khéo léo làm nổi bật đôi giày, làm nên vẻ đẹp cá tính và đầy thời trang. Form giày ôm sát cho đôi chân thêm thon gọn, tạo sự thoải mái khi mang, giúp bạn gái thoải mái hoạt động suốt cả ngày. Thiết kế với kiểu dáng trang nhã, thanh lịch mang đến cho bạn vẻ tự tin và cuốn hút. Item giày búp bê luôn là sự lựa chọn hàng đầu cho các cô nàng năng động.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(39, ' 8420656998223', ' Giày Sandal Mũi Tròn Royal Walk RY084-1 - Hồng Trắng', ' giay-sandal-mui-tron-royal-walk-ry084-1-hong-trang', '199000', ' pink', ' L|M|XL|XXL', 10, 3, 4, 'May từ chất liệu da tổng hợp bền bỉ. Kiểu giày gót trụ, dây gài, mũi bo tròn nổi bật. Form giày ôm chân vừa vặn, tôn dáng dễ mang', 'Giày Sandal Mũi Tròn Royal Walk RY084-1 với chất liệu da tổng hợp cao cấp, mềm mại và dễ dàng lau chùi, đế cứng cáp chống trượt tốt. Thiết kế gót trụ, mũi bo tròn, dây gài tạo nên vẻ thanh lịch và sang trọng. Đường viền được may phối tinh tế và khéo léo, nổi bật nhờ khóa kim loại, làm nên vẻ đẹp cá tính và đầy thời trang. Form ôm sát cho đôi chân thêm thon gọn, tạo sự thoải mái khi mang, giúp bạn gái thoải mái hoạt động suốt cả ngày.', 0, 1, 0, 0, 1, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(40, ' 4862198666460', ' Guốc Everyday Is Sunday GG04 - Hồng', ' guoc-everyday-is-sunday-gg04-hong', '420000', ' pink', ' L|M|XL|XXL', 10, 3, 4, 'Chất liệu da tổng hợp bền chắc, ít bám bụi và chống sờn rách. Form ôm chân thoái mái. Đế giày được gia công siêu bền, chống trơn trượt', 'Guốc Everyday Is Sunday GG04 - Hồng có kiểu dáng dễ mang với đế dày dặn và cứng cáp, chống mài mòn và trơn trượt khi di chuyển. Quai ngang ôm trọn phần chân, mang đến cho bạn vẻ ngoài nữ tính giúp thu hút ánh nhìn. sản phẩm được may từ chất liệu da tổng hợp với bề mặt da mềm mại, dễ dàng lau chùi và bảo quản. Chất liệu keo dán được dùng cho thị trường xuất khẩu nên rất chắc chắn, độ bền cao. Form ôm chân vừa vặn, tạo sự thoải mái khi mang. Phần đề bằng được gia công chắc chắn và bền bỉ, chống trơn trượt hay mài mòn.', 0, 1, 1, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(41, ' 5100635234483', ' Mắt Kính Nữ Polar Solar PS PS1059L C6', ' mat-kinh-nu-polar-solar-ps-ps1059l-c6', '650000', ' blue', ' L|M|XL|XXL', 10, 1, 5, ' Mắt Kính Nữ Polar Solar PS PS1059L C6', 'Mắt Kính Nữ Polar Solar PS PS1059L C6 được gia công từ chất liệu cao cấp bền, đẹp. Trọng lượng siêu nhẹ, không nặng sống mũi, tạo sự thoải mái khi sử dụng. Kiểu dáng thanh mảnh, sang trọng. Sự kết hợp hài hòa giữa hai gam màu xanh biển – vàng kim làm bật lên thẩm mỹ của một dòng sản phẩm cao cấp. Đây còn là gam màu hạn chế tối đa sự bám bẩn, ổ màu, cũ kĩ theo thời gian.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(42, ' 1211112910951', ' Nón Nữ Vành Tròn Everest Fedora H277 (Trắng)', ' non-nu-vanh-tron-everest-fedora-h277-trang', '79000', ' white', ' L|M|XL|XXL', 10, 1, 5, ' Chất liệu vải cói bền đẹp. Thiết kế thoáng không tạo cảm giác hầm khi đội. Có dây thắt nơ tạo điểm nhấn cho chiếc nón. Vành rộng giúp che nắng bảo vệ mắt khỏi tia mặt trời. Có thế phối với nhiều loại trang phục', 'Nón Nữ Vành Tròn Everest Fedora H277 (Trắng) mang đến vẻ ngoài trẻ trung, nữ tính cho người sử dụng với tông màu trơn đơn giản kết hợp với dây nơ tạo điểm nhấn nổi bật. Chất liệu vải cói được dệt may chắc chắn, chống phai sờn và bạc màu theo thời gian. Đường may tinh xảo đảm bảo duy trì tuổi thọ dài lâu cho sản phẩm. Diện nón chống nắng thời trang bạn sẽ thoải mái với các hoạt động ngoài trời mà không sợ nắng và giúp bạn thời trang hơn. Bạn có thể đội nón để đi học, dạo phố hay đi biển vào những dịp rảnh rỗi cùng bạn bè và người thân.', 0, 1, 1, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(43, ' 1816930626345', ' Găng Tay Da Lót Nỉ Cảm Ứng Nữ Panda GTN-01 - Đen', ' gang-tay-da-lot-ni-cam-ung-nu-panda-gtn-01-den', '59000', ' black', ' L|M|XL|XXL', 10, 1, 5, 'Làm từ da mềm mại, có lớp vải lót. Có độ co giãn cao. Thiết kế ôm sát, thoải mái', 'Găng Tay Da Lót Nỉ Cảm Ứng Nữ Panda GTN-01 - Đen được làm từ chất liệu da mềm mại, độ co giãn cao, cản gió kết hợp cùng lớp vải lót cao cấp, ấm áp, giúp đôi tay của bạn được bảo vệ một cách tốt nhất. Vào những ngày trời rét đậm, ngoài những bộ quần áo ấm áp, thì đôi găng tay da cảm ứng nữ cũng là món phụ kiện góp phần không nhỏ trong việc giữ gìn sức khỏe cũng như đảm bảo tính thời trang cho bộ đồ của bạn. Dù di chuyển đường dài với nhiều họat động bạn cũng không phải lo lắng về độ bền của đôi găng tay. Với thiết kế ôm sát, bạn có thể thoải mái hoạt động mà không cảm thấy vướng víu.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(44, '  7912026732281', ' Móc Khóa 12 Con Giáp Đá Mắt Hổ Tuổi Tý Ngọc Quý Gemstones MK7', ' mo-c-kho-a-12-con-gia-p-da-mat-ho-tuoi-ty-ngoc-quy-gemstones-mk7', '500000', ' random', ' L|M|XL|XXL', 10, 1, 5, 'Đá thiên nhiên 100%. Đá mắt Hổ đem lại sự giàu có và may mắn. Đá mắt Hổ được dùng để xua đuổi những phép thuật ma quỷ và tránh tà. Nó có thể cải thiện hệ tiêu hoá. Thiết kế hiện đại, tình xảo', 'Đá thiên nhiên 100%. Đá mắt Hổ đem lại sự giàu có và may mắn. Đá mắt Hổ được dùng để xua đuổi những phép thuật ma quỷ và tránh tà. Nó có thể cải thiện hệ tiêu hoá. Thiết kế hiện đại, tình xảo', 0, 1, 0, 1, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(45, ' 5106713186873', ' Kính Mát Nữ Exfash EF 2751 204', ' kinh-mat-nu-exfash-ef-2751-204', '819000', ' random', ' L|M|XL|XXL', 10, 1, 5, 'Gọng kính làm bằng plastic siêu nhẹ và bền. Tròng kính chống tia UV, bảo vệ mắt tối ưu. Kiểu dáng nữ tính thanh lịch', 'Kính Mát Nữ Exfash EF 2751 204 có thiết kế gọng kính bo tròn, bản lớn hình vuông, phối các đường kẻ sang trọng, kẹp kim loại khác màu tạo điểm nhấn nổi bật. Tròng kính có tông màu xanh tím khói trẻ trung, phù hợp với áo sơmi, jeans rách, váy miđi cho những ngày dạo phố bên bạn bè.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(46, ' 5108335898653', ' Nón Nam Spancap Circle Logo Charcoal PREMI3R FL370 - Xám', ' non-nam-spancap-circle-logo-charcoal-premi3r-fl370-xam', '400000', ' gray', ' L|M|XL|XXL', 10, 1, 5, 'Kiểu dáng mới lạ. Chất liệu co dãn thoải mái. Màu xám đậm năng động, cá tính', 'Nón Nam Spancap Circle Logo Charcoal PREMI3R FL370 là một mẫu nón thời trang đến từ thương hiệu PREMI3R nổi tiếng của Hàn Quốc. Nón có kiểu dáng mới lạ, bỏ đi chiếc khóa nón phía sau, phù hợp với tiêu chí one-size-fits-all hiện nay. Nón spancap màu xám đậm với chất liệu vải 64% polyester, 33% cotton, 3% spandex, giúp chiếc nón có độ co dãn nhất định để fit với mọi size đầu, tạo cảm giác thoải mái khi sử dụng. Nón nổi bật nhờ logo của Flipper được in nổi 3D tạo nên phong cách cá tính, năng động. Màu xám đậm làm chủ đạo dễ dàng khi phối hợp với mọi loại trang phục. Đây chắc hẳn sẽ là một phụ kiện thời trang tuyệt vời giúp nâng tầm set đồ của bạn khi phối hợp với quần jeans, áo thun, chân váy, giày sneakers,...', 0, 1, 0, 0, 1, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(47, ' 5105571931878', ' Thắt Lưng Da Cá Sấu Nam Da Thật Da Giày Việt Nam VNLDLCS0TK18NB - Da Bò Đầu Trắng', ' that-lung-da-ca-sau-nam-da-that-da-giay-viet-nam-vnldlcs0tk18nb-da-bo-dau-trang', '2339000', ' orange', ' L|M|XL|XXL', 10, 1, 5, ' Thắt Lưng Nam Da Cá Sấu Da Thật Da Giày Việt Nam VNLDLCS0TK18NB', 'Thắt Lưng Nam Da Cá Sấu Da Thật Da Giày Việt Nam VNLDLCS0TK18NB - Da Bò Đầu Trắng có màu da bò chủ đạo kết hợp cùng đầu khóa mạ trắng tinh tế giúp nam giới tự tin thể hiện phong cách thời trang thanh lịch và đĩnh đạc của mình. Đầu khóa cách điệu lạ mắt, từng đường cắt may được thực hiện sắc sảo, khẳng định đẳng cấp thương hiệu. Sản phẩm được thuộc da đủ nhiệt độ theo đúng quy chuẩn quốc tế cho chất lượng và độ bền đẹp lý tưởng.', 0, 1, 1, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(48, ' 5109615554894', ' Kính Mát Nam Exfash EF 4771 629', ' kinh-mat-nam-exfash-ef-4771-629', '429000', ' red', ' L|M|XL|XXL', 10, 1, 5, 'Kính Mát Nam Exfash EF 4771 629 màu đỏ cá tính, sành điệu.', 'Kính Mát Nam Exfash EF 4771 629 màu đỏ cá tính, sành điệu. Kiểu kính dơi hiện đại, bảo vệ mắt hiệu quả và giúp bạn nam trông hiện đại hơn. Sản phẩm được gia công từ những chất liệu cao cấp siêu bền. Gọng nhựa cao cấp, càng dùng lâu năm càng bóng đẹp.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(49, ' 5100494559352', ' Kính Mát Phân Cực Unisex Veithdia Gọng Hợp Kim Magie V109T - Vàng', ' kinh-mat-phan-cuc-unisex-veithdia-gong-hop-kim-magie-v109t-vang', '449000', ' yellow', ' L|M|XL|XXL', 10, 1, 5, ' Chất liệu gọng hợp kim Magie cao cấp. Chức năng phân cực chống tia UV giúp bảo vệ mắt. Kiểu dáng quân đội Mỹ cá tính và phong cách. Phù hợp với tiêu chuẩn an toàn của ECC', 'Kính Mát Phân Cực Unisex Veithdia Gọng Hợp Kim Magie V109T - Vàng gây ấn tượng với phong cách thời trang sang trọng với sắc vàng kim tươi sáng, dáng kính quân đội mỹ đầy cá tính. Chất liệu hợp kim Magie cao cấp, mang đến độ bền đẹp cùng tính thẩm mỹ cao, đồng thời không gây khó chịu khi đeo lâu. Kính có bộ màng lọc tiên tiến, giúp lọc các tia UV có hại như UVA và UVB. Lớp màng lọc được phủ bên ngoài kính còn giúp loại bỏ ánh sáng phân cực, mang đến cho bạn tầm nhìn và hình ảnh tốt nhất dù làm việc hay sinh hoạt ngoài trời nắng hoặc có ánh sáng nhiều gây chói.', 0, 1, 0, 1, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1),
(50, ' 5105633151497', ' Set Vòng CHRISTMAS TK23', ' set-vong-christmas-tk23', '60000', ' random', ' L|M|XL|XXL', 10, 1, 5, 'Màu sắc đa dạng, độc đáo, thể hiện cá tính riêng. Làm bằng chất liệu dây thừng, hạt nhựa,... nhiều màu sắc. Kiểu dáng trẻ trung, năng động', 'Set Vòng CHRISTMAS TK23 hướng đến phong cách bụi bặm, chất, handmade,… với mong muốn mang đến cho khách hàng những sản phẩm có chất lượng tốt nhất, thân thiện với môi trường mà lại vừa độc đáo, khác biệt. Sản phẩm được làm thủ công bằng tay tỉ mỉ và tinh xảo. guyên liệu chủ yếu là dây thừng nhiều màu sắc mang đậm dấu ấn giáng sinh. iểu dáng mới lạ, độc đáo. hù hợp để đeo đi học, đi chơi, có thể dùng làm quà tặng đặc biệt cho bạn bè, người thân.', 0, 1, 0, 0, 0, '2018-04-05', '2018-04-15', NULL, NULL, NULL, 1);

-- --------------------------------------------------------

--
-- Table structure for table `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `description` text,
  `parent_id` int(11) DEFAULT '0',
  `meta_title` varchar(255) DEFAULT NULL,
  `meta_keyword` varchar(255) DEFAULT NULL,
  `meta_description` varchar(255) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `slug`, `image`, `description`, `parent_id`, `meta_title`, `meta_keyword`, `meta_description`, `status`) VALUES
(1, 'Trang phục Nam', 'trang-phuc-nam', '', '', 0, '', '', '', 0),
(2, 'Trang phục Nữ', 'trang-phuc-nu', '', '', 0, '', '', '', 0),
(3, 'Giày Nam', 'giay-nam', '', '', 0, '', '', '', 0),
(4, 'Giày Nữ', 'giay-nu', '', '', 0, '', '', '', 0),
(5, 'Phụ kiện', 'phu-kien', '', '', 0, '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_images`
--

CREATE TABLE `product_images` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_featured` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `is_featured`) VALUES
(1, 1, ' img/products/1-1.jpg', 1),
(2, 2, ' img/products/1-2.jpg', 1),
(3, 3, ' img/products/1-3.jpg', 1),
(4, 4, ' img/products/1-4.jpg', 1),
(5, 5, ' img/products/1-5.jpg', 1),
(6, 6, ' img/products/1-6.jpg', 1),
(7, 7, ' img/products/1-7.jpg', 1),
(8, 8, ' img/products/1-8.jpg', 1),
(9, 9, ' img/products/1-9.jpg', 1),
(10, 10, ' img/products/1-10.jpg', 1),
(11, 11, ' img/products/2-1.jpg', 1),
(12, 12, ' img/products/2-2.jpg', 1),
(13, 13, ' img/products/2-3.jpg', 1),
(14, 14, ' img/products/2-4.jpg', 1),
(15, 15, ' img/products/2-5.jpg', 1),
(16, 16, ' img/products/2-6.jpg', 1),
(17, 17, ' img/products/2-7.jpg', 1),
(18, 18, ' img/products/2-8.jpg', 1),
(19, 19, ' img/products/2-9.jpg', 1),
(20, 20, ' img/products/2-10.jpg', 1),
(21, 21, ' img/products/3-1.jpg', 1),
(22, 22, ' img/products/3-2.jpg', 1),
(23, 23, ' img/products/3-3.jpg', 1),
(24, 24, ' img/products/3-4.jpg', 1),
(25, 25, ' img/products/3-5.jpg', 1),
(26, 26, ' img/products/3-6.jpg', 1),
(27, 27, ' img/products/3-7.jpg', 1),
(28, 28, ' img/products/3-8.jpg', 1),
(29, 29, ' img/products/3-9.jpg', 1),
(30, 30, ' img/products/3-10.jpg', 1),
(31, 31, ' img/products/4-1.jpg', 1),
(32, 32, ' img/products/4-2.jpg', 1),
(33, 33, ' img/products/4-3.jpg', 1),
(34, 34, ' img/products/4-4.jpg', 1),
(35, 35, ' img/products/4-5.jpg', 1),
(36, 36, ' img/products/4-6.jpg', 1),
(37, 37, ' img/products/4-7.jpg', 1),
(38, 38, ' img/products/4-8.jpg', 1),
(39, 39, ' img/products/4-9.jpg', 1),
(40, 40, ' img/products/4-10.jpg', 1),
(41, 41, ' img/products/5-1.jpg', 1),
(42, 42, ' img/products/5-2.jpg', 1),
(43, 43, ' img/products/5-3.jpg', 1),
(44, 44, ' img/products/5-4.jpg', 1),
(45, 45, ' img/products/5-5.jpg', 1),
(46, 46, ' img/products/5-6.jpg', 1),
(47, 47, ' img/products/5-7.jpg', 1),
(48, 48, ' img/products/5-8.jpg', 1),
(49, 49, ' img/products/5-9.jpg', 1),
(50, 50, ' img/products/5-10.jpg', 1),
(51, 42, 'img/products/logo.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product_relates`
--

CREATE TABLE `product_relates` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `product_relates_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `provinces`
--

CREATE TABLE `provinces` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `code` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `provinces`
--

INSERT INTO `provinces` (`id`, `name`, `code`) VALUES
(1, ' Hà Nội', 1),
(2, ' Hà Giang', 2),
(4, ' Cao Bằng', 4),
(6, ' Bắc Kạn', 6),
(8, ' Tuyên Quang', 8),
(10, ' Lào Cai', 10),
(11, ' Điện Biên', 11),
(12, ' Lai Châu', 12),
(14, ' Sơn La', 14),
(15, ' Yên Bái', 15),
(17, ' Hoà Bình', 17),
(19, ' Thái Nguyên', 19),
(20, ' Lạng Sơn', 20),
(22, ' Quảng Ninh', 22),
(24, ' Bắc Giang', 24),
(25, ' Phú Thọ', 25),
(26, ' Vĩnh Phúc', 26),
(27, ' Bắc Ninh', 27),
(30, ' Hải Dương', 30),
(31, ' Hải Phòng', 31),
(33, ' Hưng Yên', 33),
(34, ' Thái Bình', 34),
(35, ' Hà Nam', 35),
(36, ' Nam Định', 36),
(37, ' Ninh Bình', 37),
(38, ' Thanh Hóa', 38),
(40, ' Nghệ An', 40),
(42, ' Hà Tĩnh', 42),
(44, ' Quảng Bình', 44),
(45, ' Quảng Trị', 45),
(46, ' Thừa Thiên Huế', 46),
(48, ' Đà Nẵng', 48),
(49, ' Quảng Nam', 49),
(51, ' Quảng Ngãi', 51),
(52, ' Bình Định', 52),
(54, ' Phú Yên', 54),
(56, ' Khánh Hòa', 56),
(58, ' Ninh Thuận', 58),
(60, ' Bình Thuận', 60),
(62, ' Kon Tum', 62),
(64, ' Gia Lai', 64),
(66, ' Đắk Lắk', 66),
(67, ' Đắk Nông', 67),
(68, ' Lâm Đồng', 68),
(70, ' Bình Phước', 70),
(72, ' Tây Ninh', 72),
(74, ' Bình Dương', 74),
(75, ' Đồng Nai', 75),
(77, ' Bà Rịa - Vũng Tàu', 77),
(79, ' Hồ Chí Minh', 79),
(80, ' Long An', 80),
(82, ' Tiền Giang', 82),
(83, ' Bến Tre', 83),
(84, ' Trà Vinh', 84),
(86, ' Vĩnh Long', 86),
(87, ' Đồng Tháp', 87),
(89, ' An Giang', 89),
(91, ' Kiên Giang', 91),
(92, ' Cần Thơ', 92),
(93, ' Hậu Giang', 93),
(94, ' Sóc Trăng', 94),
(95, ' Bạc Liêu', 95),
(96, ' Cà Mau', 96);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `rate` tinyint(4) NOT NULL,
  `content` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `update_at` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `fullname` varchar(255) NOT NULL,
  `gender` tinyint(4) NOT NULL,
  `birthdate` date NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `province_id` int(11) NOT NULL,
  `district_id` int(11) NOT NULL,
  `address` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` date NOT NULL,
  `update_at` date NOT NULL,
  `user_group_id` int(11) NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `fullname`, `gender`, `birthdate`, `email`, `phone`, `province_id`, `district_id`, `address`, `password`, `created_at`, `update_at`, `user_group_id`, `status`) VALUES
(1, 'Trần Văn Cam', 1, '1996-04-05', 'campxpro@gmail.com', '0969932704', 1, 280, 'số 9 ngõ 273 đường Trần Cung', '12345678', '2009-03-09', '2010-10-01', 1, 1),
(2, 'Phạm Vũ Thảo My', 0, '1997-01-07', 'tenmyhopham@gmail.com', '0869127195', 34, 339, 'Số 18 Phố Viên - Phường Đức Thắng', '12345678', '2010-06-01', '2012-05-04', 2, 1),
(3, 'Trần Thu Trang', 0, '1998-01-19', 'trangtran@gmail.com', '01690000000', 34, 339, '334 Nguyễn Trãi', '12345678', '2007-09-06', '2014-04-01', 2, 1),
(4, 'Nguyễn Bá Giáp', 1, '1994-06-09', 'lala@gmail.com', '01234731395', 36, 356, '144 đường Xuân Thủy', '12345678', '2005-10-18', '2013-08-27', 2, 1),
(5, 'Ngọc Trinh', 0, '1989-09-27', 'ngoctrinh@gmail.com', '0989999999', 84, 846, 'số 0 ngõ 0 đường cũng không', '12345678', '2006-12-10', '2007-03-20', 3, 1);

-- --------------------------------------------------------

--
-- Table structure for table `user_groups`
--

CREATE TABLE `user_groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `amount` decimal(10,0) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_groups`
--

INSERT INTO `user_groups` (`id`, `name`, `type`, `amount`) VALUES
(1, 'Vip', 'rate', '50'),
(2, 'Gold', 'rate', '40'),
(3, 'Positive', 'rate', '30'),
(4, 'Normal', 'rate', '10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banners`
--
ALTER TABLE `banners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `coupons`
--
ALTER TABLE `coupons`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `province_id` (`province_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `province_id` (`province_id`),
  ADD KEY `district_id` (`district_id`);

--
-- Indexes for table `order_items`
--
ALTER TABLE `order_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `post_category_id` (`post_category_id`);

--
-- Indexes for table `post_categories`
--
ALTER TABLE `post_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `brand_id` (`brand_id`),
  ADD KEY `product_category_id` (`product_category_id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `slug` (`slug`),
  ADD KEY `parent_id` (`parent_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`);

--
-- Indexes for table `product_relates`
--
ALTER TABLE `product_relates`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `product_relates_id` (`product_relates_id`);

--
-- Indexes for table `provinces`
--
ALTER TABLE `provinces`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`),
  ADD KEY `product_id` (`product_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `province_id` (`province_id`),
  ADD KEY `district_id` (`district_id`),
  ADD KEY `user_group_id` (`user_group_id`);

--
-- Indexes for table `user_groups`
--
ALTER TABLE `user_groups`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `banners`
--
ALTER TABLE `banners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupons`
--
ALTER TABLE `coupons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=974;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_items`
--
ALTER TABLE `order_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `post_categories`
--
ALTER TABLE `post_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `product_relates`
--
ALTER TABLE `product_relates`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `provinces`
--
ALTER TABLE `provinces`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `user_groups`
--
ALTER TABLE `user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `comments_ibfk_2` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`);

--
-- Constraints for table `districts`
--
ALTER TABLE `districts`
  ADD CONSTRAINT `districts_ibfk_1` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `order_items`
--
ALTER TABLE `order_items`
  ADD CONSTRAINT `order_items_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `order_items_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`);

--
-- Constraints for table `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_ibfk_1` FOREIGN KEY (`post_category_id`) REFERENCES `post_categories` (`id`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`product_category_id`) REFERENCES `product_categories` (`id`);

--
-- Constraints for table `product_images`
--
ALTER TABLE `product_images`
  ADD CONSTRAINT `product_images_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `product_relates`
--
ALTER TABLE `product_relates`
  ADD CONSTRAINT `product_relates_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Constraints for table `reviews`
--
ALTER TABLE `reviews`
  ADD CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`),
  ADD CONSTRAINT `reviews_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`user_group_id`) REFERENCES `user_groups` (`id`),
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`province_id`) REFERENCES `provinces` (`id`),
  ADD CONSTRAINT `users_ibfk_3` FOREIGN KEY (`district_id`) REFERENCES `districts` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
