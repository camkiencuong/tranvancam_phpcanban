<?php 
session_start();
ob_start();
require('connect.php');
require('functions.php');
require('page_header.php');
require('class/Product.php');

$baseURL = 'products.php';
$object = new Product();
$tableCols = ['id','sku','name','slug','price','colors','sizes','qty','brand_id','product_category_id','description','content','rate','is_new','is_promotion','is_featured','is_sale','created_at','updated_at','meta_title','meta_keyword','meta_description','status'];

require('task_show.php');

$categories = $object->getData('*','product_categories');
$brands = $object->getData('*','brands');

if (!isset($_GET['act'])):
$totalRecord = $object->totalRecord($where);
$sanpham = $object->index('*',$where,$limit,$offset,$join,$orderby);

?>

<!-- start show -->
<section class="show clear <?php if (isset($_GET['view'])) echo 'blur'; ?>">
	<div class="namepage clear">
		<div class="container">
			<h1>Quản lý Sản phẩm</h1>
		</div>
	</div>
	<div class="menubar clear">
		<div class="container">
			<ul>
				<li><a href="<?php echo $baseURL.'?act=add'; ?>">Thêm sản phẩm</a></li>
				<li class="message" style="padding-left:5%;">
					<?php if(isset($_SESSION['flash_msg'])) :?>
						<span><?php echo $_SESSION['flash_msg'];?></span>
						<?php unset($_SESSION['flash_msg']); endif;?>
						<?php if(count($error)>0):
						for ($i=0; $i < count($error); $i++) :?>
						<span><?php echo $error[$i]."<br>";?></span>
					<?php endfor; endif;?>
				</li>
			</ul>
		</div>
	</div>
	<div class="content object-show tb-border">
		<div class=" container number clear">
			Số dòng hiển thị: 
			<select onchange="window.location.href=this.value">
				<option value="<?php echo $baseURL.$queryLimit.'limit=10';?>" <?php if ($limit=='10') {echo 'selected';}?>>10</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=20';?>" <?php if ($limit=='20') {echo 'selected';}?>>20</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=50';?>" <?php if ($limit=='50') {echo 'selected';}?>>50</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=100';?>" <?php if ($limit=='100') {echo 'selected';}?>>100</option>
			</select>
		</div>
		<div class="container">
			<span><big>Tổng: <?php echo $totalRecord; ?></big></span>
			<div class="tuychinh">
				<span class="orderby">
					Sắp xếp: 
					<select onchange="window.location.href=this.value">
						<option value="<?php echo $baseURL.$queryBy.'col=id&by=ASC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'ASC') {echo 'selected';} ?> >id Tăng</option>
						<option value="<?php echo $baseURL.$queryBy.'col=id&by=DESC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'DESC') {echo 'selected';} ?> >id Giảm</option>
						<option value="<?php echo $baseURL.$queryBy.'col=name&by=ASC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'ASC') {echo 'selected';} ?> >name Tăng</option>
						<option value="<?php echo $baseURL.$queryBy.'col=name&by=DESC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'DESC') {echo 'selected';} ?> >name Giảm</option>
					</select>
				</span>
				<span class="finding">
					<form action="" method="get">
						<input id="key" type="text" name="key" class="form" placeholder="từ khóa" value="<?php if(isset($_GET['key'])) {echo $_GET['key'];}?>">
						<input onclick="return testFind();" type="submit" name="find" value="Tìm Kiếm" class="button">
					</form>
				</span>
			</div>
			<?php if ($totalRecord == 0):?>
				<div class="container dt-center clear">Không có dữ liệu để hiển thị!</div>
			<?php else: ?>
				<div class="form-table clear">
					<form action="" method="post">
						<table border="1">
							<tr>
								<th colspan="5">Thao tác</th>
								<th>id</th>
								<th>sku</th>
								<th>Tên sản phẩm</th>
								<th>slug</th>
								<th>Giá</th>
								<th>màu sắc</th>
								<th>sizes</th>
								<th>Số lượng</th>
								<th>Thương hiệu</th>
								<th>Danh mục</th>
								<th>Mô tả</th>
								<th>Chi tiết</th>
								<th>Sao</th>
								<th>Mới</th>
								<th>Khuyến mãi</th>
								<th>Nổi bật</th>
								<th>Bán chạy</th>
								<th>Ngày tạo</th>
								<th>Ngày sửa</th>
								<th>meta title</th>
								<th>meta keyword</th>
								<th>meta description</th>
								<th>Trạng thái</th>
							</tr>
							<?php for ($i=0; $i < count($sanpham); $i++) :?>
								<tr>
									<td class="options"><input type="checkbox" name="checkbox[]" value="<?php echo $sanpham[$i]['id'];?>" class="checkbox" <?php if(isset($_POST['checkall'])) {echo 'checked';} ?>></td>
									<td class="options"><a href="<?php echo $baseURL.'?act=edit&id='.$sanpham[$i]['id'];?>">sửa</a></td>
									<td class="options"><a onclick="return confirm('Bạn chắc chắn muốn xóa không?');" href="<?php echo $baseURL.'?id_del='.$sanpham[$i]['id'];?>">xóa</a></td>
									<td class="options"><a href="<?php echo $baseURL.$queryImg.'view=image&id='.$sanpham[$i]['id'];?>">hình ảnh</a></td>
									<td class="options"><a href="<?php echo $baseURL.$queryImg.'view=relate&id='.$sanpham[$i]['id'];?>">liên quan</a></td>
									<td class="data-show"><?php echo $sanpham[$i]['id']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['sku']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['name']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['slug']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['price'].' (đ)'; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['colors']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['sizes']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['qty']; ?></td>
									<td class="data-show"><?php echo $object->brandName($sanpham[$i]['brand_id']); ?></td>
									<td class="data-show"><?php echo $object->categoryName($sanpham[$i]['product_category_id']); ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['description']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['content']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['rate']; ?></td>
									<td class="data-show"><?php echo $object->isYes($sanpham[$i]['is_new']); ?></td>
									<td class="data-show"><?php echo $object->isYes($sanpham[$i]['is_promotion']); ?></td>
									<td class="data-show"><?php echo $object->isYes($sanpham[$i]['is_featured']); ?></td>
									<td class="data-show"><?php echo $object->isYes($sanpham[$i]['is_sale']); ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['created_at']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['updated_at']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['meta_title']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['meta_keyword']; ?></td>
									<td class="data-show"><?php echo $sanpham[$i]['meta_description']; ?></td>
									<td class="data-show"><?php echo $object->status($sanpham[$i]['status']); ?></td>
								</tr>
							<?php endfor; ?>
						</table>
						<div class="menubot clear">
							<input type="checkbox" id="selectall" onClick="selectAll(this)" class="checkbox">
							<button type="button" id="toggle" value="SelectAll" onClick="checkAll()">Chọn Tất</button>
							<input onclick="return testDel()" type="submit" name="multi_del" value="Xóa">
							<input onclick="return testEdit()" type="submit" name="multi_edit" value="Sửa">
							<input onclick="return testEdit()" type="submit" name="active" value="Kích Hoạt">
							<input onclick="return testEdit()" type="submit" name="block" value="Vô Hiệu">
							<input onclick="return testEdit()" type="submit" name="product_img" value="Thêm/Sửa Ảnh">
							<select name="brand_id">
								<?php foreach ($brands as $brand) :?>
									<option value="<?php echo $brand['id']; ?>"><?php echo $brand['name'];?></option>
								<?php endforeach;?>
							</select>
							<input onclick="return testEdit()" type="submit" name="brand" value="thương hiệu">
							<select name="product_category_id">
								<?php foreach ($categories as $category) :?>
									<option value="<?php echo $category['id']; ?>"><?php echo $category['name'];?></option>
								<?php endforeach;?>
							</select>
							<input onclick="return testEdit()" type="submit" name="category" value="danh mục">
							||
							<input onclick="return testEdit()" type="submit" name="is_new" value="Mới">
							<input onclick="return testEdit()" type="submit" name="is_promotion" value="Khuyến mãi">
							<input onclick="return testEdit()" type="submit" name="is_featured" value="Nổi bật">
							<input onclick="return testEdit()" type="submit" name="is_sale" value="Bán chạy">
						</div>
					</form>
				</div>
				<div class="paging clear">
					<div class="container">
						<?php echo paging($baseURL,$queryString,$totalRecord,$page,$limit);?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>
<!-- end show -->


<!-- start create  -->
<?php if (isset($_GET['act'])):

require('task_action.php');

if (isset($_POST['addnow'])) {
	$data = $_POST['data'];
	for ($i=0; $i < count($data); $i++) { 
		$id = trim($data[$i]['id']);
		$name = trim($data[$i]['name']);
		$slug = to_slug($name);
		$price = trim($data[$i]['price']);
		$colors = trim($data[$i]['colors']);
		$sizes = trim($data[$i]['sizes']);
		$qty = trim($data[$i]['qty']);
		$brand_id = $data[$i]['brand_id'];
		$product_category_id = $data[$i]['product_category_id'];
		$description = trim($data[$i]['description']);
		$content = trim($data[$i]['content']);
		$rate = trim($data[$i]['rate']);
		$is_new = (isset($data[$i]['is_new']) ? $data[$i]['is_new'] : 0);
		$is_promotion = (isset($data[$i]['is_promotion']) ? $data[$i]['is_promotion'] : 0);
		$is_featured = (isset($data[$i]['is_featured']) ? $data[$i]['is_featured'] : 0);
		$is_sale = (isset($data[$i]['is_sale']) ? $data[$i]['is_sale'] : 0);
		$created_at = trim($data[$i]['created_at']);
		$updated_at = trim($data[$i]['updated_at']);
		$meta_title = trim($data[$i]['meta_title']);
		$meta_keyword = trim($data[$i]['meta_keyword']);
		$meta_description = trim($data[$i]['meta_description']);
		$status = $data[$i]['status'];
		$sku = create_sku($product_category_id,$brand_id,$id);

		if ($name == '') {
			$error[] = "'Tên sản phẩm' id=$id không được trống!";
			continue;
		}
		$where = " AND sku=$sku";
		$testsku = $object->index('*',$where);
		if (count($testsku)>0) {
			$error[] = "Sku = '$sku' đã tồn tại! (id=$id)";
			continue;
		}
		$where = ' AND slug="'.$slug.'"';
		$testSlug = $object->index('*',$where);
		if (count($testSlug)!=0) {
			$error[] = "Slug = '$slug' đã tồn tại! (id=$id)";
			continue;
		}
		$str = [];
		foreach ($tableCols as $col) {
			$str[] = $$col;
		}
		$value_array[] = "'".implode("','", $str)."'";
	}
	if (count($error)==0) {
		$error[] = $object->create($value_array);
		if ($error[0] == '') {
			$_SESSION['flash_msg'] = 'Thêm Thành Công!';
			header("location: $baseURL");exit;
		}
	}
}
if (isset($_POST['editnow'])) {
	$data = $_POST['data'];
	for ($i=0; $i < count($data); $i++) { 
		$id = trim($data[$i]['id']);
		$name = trim($data[$i]['name']);
		$slug = to_slug($name);
		$price = trim($data[$i]['price']);
		$colors = trim($data[$i]['colors']);
		$sizes = trim($data[$i]['sizes']);
		$qty = trim($data[$i]['qty']);
		$brand_id = $data[$i]['brand_id'];
		$product_category_id = $data[$i]['product_category_id'];
		$description = trim($data[$i]['description']);
		$content = trim($data[$i]['content']);
		$rate = trim($data[$i]['rate']);
		$is_new = (isset($data[$i]['is_new']) ? $data[$i]['is_new'] : 0);
		$is_promotion = (isset($data[$i]['is_promotion']) ? $data[$i]['is_promotion'] : 0);
		$is_featured = (isset($data[$i]['is_featured']) ? $data[$i]['is_featured'] : 0);
		$is_sale = (isset($data[$i]['is_sale']) ? $data[$i]['is_sale'] : 0);
		$created_at = trim($data[$i]['created_at']);
		$updated_at = trim($data[$i]['updated_at']);
		$meta_title = trim($data[$i]['meta_title']);
		$meta_keyword = trim($data[$i]['meta_keyword']);
		$meta_description = trim($data[$i]['meta_description']);
		$status = $data[$i]['status'];
		$sku = create_sku($product_category_id,$brand_id,$id);

		$id_out = $edit_array[$i]['id'];
		$slug_out = $edit_array[$i]['slug'];
		$sku_out = $edit_array[$i]['sku'];
		$where = ' AND id='.$id.' AND id<>'.$id_out;
		$testid = $object->index('*',$where);
		if (count($testid)>0) {
			$error[] = 'lỗi id='.$id.' bị trùng!';
			continue;
		}
		if ($name == '') {
			$error[] = "'Tên sản phẩm' id=$id không được trống!";
			continue;
		}
		$where = " AND sku=$sku AND sku<>$sku_out";
		$testsku = $object->index('*',$where);
		if (count($testsku)>0) {
			$error[] = "Sku = '$sku' đã tồn tại! (id=$id)";
			continue;
		}
		$where = " AND slug='$slug' AND slug<>'$slug_out'";
		$testslug = $object->index('*',$where);
		if (count($testslug)>0) {
			$error[] = "Slug = '$slug' đã tồn tại! (id=$id)";
			continue;
		}
		$id_array[] = 'id='.$id;
		$str = [];
		foreach ($tableCols as $col) {
			$str[] = "$col='".$$col."'";
		}
		$set_array[] = implode(", ", $str);
	}
	if (count($error) == 0) {
		$error[] = $object->update($set_array,$id_array);
		if ($error[0] == '') {
			$_SESSION['flash_msg'] = 'Sửa Thành Công!';
			header("location: $baseURL");exit;
		}
	}
	
}
?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<?php if ($_GET['act']=='add'):?>
				<h1>Thêm sản phẩm Sản phẩm</h1>
			<?php endif; ?>
			<?php if ($_GET['act']=='edit'):?>
				<h1>Sửa sản phẩm Sản phẩm</h1>
			<?php endif; ?>
		</div>
	</div>
	<div class="container" >
		<div class="message" style="padding-left:5%">
			<?php if(isset($_SESSION['flash_msg'])) :?>
				<span><?php echo $_SESSION['flash_msg'];?></span>
				<?php unset($_SESSION['flash_msg']); endif;?>
				<?php if(count($error)>0) :
				for ($i=0; $i < count($error); $i++) :?>
				<span><?php echo $error[$i]."<br>";?></span>
			<?php endfor; endif;?>
		</div>
	</div>
	<div class="back">
		<div class="container">
			<a onclick="return testBack()" href="<?php echo $baseURL;?>">Trở Về</a>
		</div>
	</div>
	<?php if ($_GET['act']=='add'):?>
		<div class="number clear">
			<div class="container">
				Số lượng dòng: 
				<select onchange="window.location.href=this.value">
					<option value="<?php echo $baseURL.'?act=add&num=1'; ?>" <?php if ($num=='1') {echo 'selected';}?>>1</option>
					<option value="<?php echo $baseURL.'?act=add&num=2'; ?>" <?php if ($num=='2') {echo 'selected';}?>>2</option>
					<option value="<?php echo $baseURL.'?act=add&num=5'; ?>" <?php if ($num=='5') {echo 'selected';}?>>5</option>
					<option value="<?php echo $baseURL.'?act=add&num=10'; ?>" <?php if ($num=='10') {echo 'selected';}?>>10</option>
					<option value="<?php echo $baseURL.'?act=add&num=20'; ?>" <?php if ($num=='20') {echo 'selected';}?>>20</option>
				</select>
			</div>
		</div>
	<?php endif;?>
	<div class="form-create clear">
		<div class="full-width">
			<form action="" method="post">
				<table>
					<tr>
						<th>id</th>
						<th>sku</th>
						<th>Tên sản phẩm</th>
						<th>slug</th>
						<th>Giá</th>
						<th>màu sắc</th>
						<th>sizes</th>
						<th>Số lượng</th>
						<th>Thương hiệu</th>
						<th>Danh mục</th>
						<th>Mô tả</th>
						<th>Chi tiết</th>
						<th>Sao</th>
						<th>Mới</th>
						<th>Khuyến mãi</th>
						<th>Nổi bật</th>
						<th>Bán chạy</th>
						<th>Ngày tạo</th>
						<th>Ngày sửa</th>
						<th>meta title</th>
						<th>meta keyword</th>
						<th>meta description</th>
						<th>Trạng thái</th>
					</tr>
					<?php for ($i=0; $i < $num; $i++):?>
						<tr>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php if(isset($edit_array)) {echo $edit_array[$i]['id'];} else {echo ++$idMax;} ?>">
							</td>
							<td>
								<input type="text" id='sku<?php echo $i; ?>' name="data[<?php echo $i; ?>][sku]" placeholder="sku (auto)" readonly class="gianua" value="<?php if(isset($edit_array)) {echo $edit_array[$i]['sku'];} elseif(isset($data)) {echo $data[$i]['sku'];}?>">
							</td>
							<td>
								<input type="text" onkeyup="ChangeToSlug(this.value,<?php echo $i; ?>)" onchange="ChangeToSlug(this.value,<?php echo $i; ?>)" id='title<?php echo $i; ?>' name="data[<?php echo $i; ?>][name]" placeholder="tên sản phẩm" autofocus value="<?php if(isset($data)) {echo $data[$i]['name'];} elseif(isset($edit_array)) {echo $edit_array[$i]['name'];} ?>">
							</td>
							<td>
								<input type="text" id='slug<?php echo $i; ?>' name="data[<?php echo $i; ?>][slug]" placeholder="slug (auto)" readonly value="<?php if(isset($edit_array)) {echo $edit_array[$i]['slug'];} elseif(isset($data)) {echo $data[$i]['slug'];}?>">
							</td>
							<td>
								<input type="number" name="data[<?php echo $i; ?>][price]" placeholder="giá cả (đ)" class='nuanon' value="<?php if(isset($data)) {echo $data[$i]['price'];} elseif(isset($edit_array)) {echo $edit_array[$i]['price'];} ?>">
							</td>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][colors]" placeholder="màu sắc" class='nuanon' value="<?php if(isset($data)) {echo $data[$i]['colors'];} elseif(isset($edit_array)) {echo $edit_array[$i]['colors'];} ?>">
							</td>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][sizes]" placeholder="L | M | XL | XXL" class='nuanon' value="<?php if(isset($data)) {echo $data[$i]['sizes'];} elseif(isset($edit_array)) {echo $edit_array[$i]['sizes'];} ?>">
							</td>
							<td>
								<input type="number" name="data[<?php echo $i; ?>][qty]" placeholder="số lượng" class='nuanon' value="<?php if(isset($data)) {echo $data[$i]['qty'];} elseif(isset($edit_array)) {echo $edit_array[$i]['qty'];} ?>">
							</td>
							<td>
								<select name="data[<?php echo $i; ?>][brand_id]">
									<?php foreach ($brands as $brand) :?>
										<option value="<?php echo $brand['id'];?>" <?php if(isset($data) && $data[$i]['brand_id'] == $brand['id'] || isset($edit_array) && $edit_array[$i]['brand_id'] == $brand['id']) {echo 'selected';} ?>><?php echo $brand['name'];?></option>
									<?php endforeach;?>
								</select>
							</td>
							<td>
								<select name="data[<?php echo $i; ?>][product_category_id]">
									<?php foreach ($categories as $category) :?>
										<option value="<?php echo $category['id'];?>" <?php if(isset($data) && $data[$i]['product_category_id'] == $category['id'] || isset($edit_array) && $edit_array[$i]['product_category_id'] == $category['id']) {echo 'selected';} ?>><?php echo $category['name'];?></option>
									<?php endforeach;?>
								</select>
							</td>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][description]" placeholder="mô tả" value="<?php if(isset($data)) {echo $data[$i]['description'];} elseif(isset($edit_array)) {echo $edit_array[$i]['description'];} ?>">
							</td>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][content]" placeholder="chi tiết" value="<?php if(isset($data)) {echo $data[$i]['content'];} elseif(isset($edit_array)) {echo $edit_array[$i]['content'];} ?>">
							</td>
							<td>
								<input type="number" name="data[<?php echo $i; ?>][rate]" placeholder="*sao*" class='motnua' value="<?php if(isset($data)) {echo $data[$i]['rate'];} elseif(isset($edit_array)) {echo $edit_array[$i]['rate'];} ?>">
							</td>
							<td class="dt-center">
								<input type="checkbox" name="is_new" class="checkbox" value="1" <?php if(isset($data) && $data[$i]['is_new'] == '1' || isset($edit_array) && $edit_array[$i]['is_new'] == '1') {echo 'checked';} ?>>
							</td>
							<td class="dt-center">
								<input type="checkbox" name="is_promotion" class="checkbox" value="1" <?php if(isset($data) && $data[$i]['is_promotion'] == '1' || isset($edit_array) && $edit_array[$i]['is_promotion'] == '1') {echo 'checked';} ?>>
							</td>
							<td class="dt-center">
								<input type="checkbox" name="is_featured" class="checkbox" value="1" <?php if(isset($data) && $data[$i]['is_featured'] == '1' || isset($edit_array) && $edit_array[$i]['is_featured'] == '1') {echo 'checked';} ?>>
							</td>
							<td class="dt-center">
								<input type="checkbox" name="is_sale" class="checkbox" value="1" <?php if(isset($data) && $data[$i]['is_sale'] == '1' || isset($edit_array) && $edit_array[$i]['is_sale'] == '1') {echo 'checked';} ?>>
							</td>
							<td>
								<input type="date" name="data[<?php echo $i; ?>][created_at]" placeholder="yyyy-mm-dd" class="gianua" value="<?php if(isset($data)) {echo $data[$i]['created_at'];} elseif(isset($edit_array)) {echo $edit_array[$i]['created_at'];} elseif($_GET['act']=='add') {echo date('Y-m-d');} ?>">
							</td>
							<td>
								<input type="date" name="data[<?php echo $i; ?>][updated_at]" placeholder="yyyy-mm-dd" class="gianua" value="<?php if(isset($data)) {echo $data[$i]['updated_at'];} elseif($_GET['act']=='edit') {echo date('Y-m-d');} elseif(isset($edit_array)) {echo $edit_array[$i]['updated_at'];} ?>">
							</td>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][meta_title]" placeholder="meta_title" value="<?php if(isset($data)) {echo $data[$i]['meta_title'];} elseif(isset($edit_array)) {echo $edit_array[$i]['meta_title'];} ?>">
							</td>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][meta_keyword]" placeholder="meta_keyword" value="<?php if(isset($data)) {echo $data[$i]['meta_keyword'];} elseif(isset($edit_array)) {echo $edit_array[$i]['meta_keyword'];} ?>">
							</td>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][meta_description]" placeholder="meta_description" value="<?php if(isset($data)) {echo $data[$i]['meta_description'];} elseif(isset($edit_array)) {echo $edit_array[$i]['meta_description'];} ?>">
							</td>
							<td>
								<select name="data[<?php echo $i; ?>][status]">
									<option value="1" <?php if(isset($data) && $data[$i]['status'] == '1' || isset($edit_array) && $edit_array[$i]['status'] == '1') {echo 'selected';} ?>>Kích hoạt</option>
									<option value="0" <?php if(isset($data) && $data[$i]['status'] == '0' || isset($edit_array) && $edit_array[$i]['status'] == '0') {echo 'selected';} ?>>Vô hiệu</option>
								</select>
							</td>
						</tr>
					<?php endfor;?>
				</table>
				<div class="container button">
					<?php if ($_GET['act']=='add'):?>
						<input onclick="return testform(<?php echo $num; ?>)" type="submit" name="addnow" value="Thêm Vào" class="submit">
					<?php endif; ?>
					<?php if ($_GET['act']=='edit'):?>
						<input onclick="return testform(<?php echo $num; ?>)" type="submit" name="editnow" value="Cập Nhật" class="submit">
					<?php endif; ?>
				</div>
			</form>
		</div>
	</div>
</section>
<?php endif; ?>
<!-- end create  -->


<!-- start show image -->
<?php if (isset($_GET['view']) && $_GET['view']=='image' && isset($_GET['id']) && $_GET['id']!=''):

$id = $_GET['id'];
$back = $baseURL.$queryImg;
if (isset($_POST['close']) || $id=='') {
	header("location: $back");exit;
}
$where = "AND product_id=$id";
$image = $object->getData('image','product_images', $where);
if (count($image)==0) {
	$bigImg = 'img/products/1-0.jpg';
} else {
	$bigImg = $image[0]['image'];
}
$avatar = (isset($_GET['img']) ? $_GET['img'] : $bigImg);
?>
<section class="view">
	<div class="khung container">
		<div class="title clear">
			<h2>Hình Ảnh Sản Phẩm</h2>
			<form action="" method="post">
				<input type="submit" name="close" value="Đóng">
			</form>
		</div>
		<div class="show clear">
			<div class="avatar">
				<img src="<?php echo $avatar; ?>">
			</div>
			<?php if (count($image) == 0): ?>
			<div class="photo clear">
				Không tìm thấy hình ảnh để hiển thị!
			</div>
			<?php else: ?>
			<div class="photo clear">
				<?php for ($i=0; $i < count($image); $i++): $src=$image[$i]['image']; ?>
				<div class="bot"><a href="<?php echo $baseURL.$queryImg.'view=image&id='.$id.'&img='.$src; ?>"><img src="<?php echo $src; ?>"></a></div>
				<?php endfor; ?>
				<div class="bot"><a href="<?php echo 'product_images.php?'.'act=add&id='.$id; ?>">Thêm ảnh</a></div>
			</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>
<!-- end show image -->


<!-- start show product relate -->
<?php if (isset($_GET['view']) && $_GET['view']=='relate'): 

$id = $_GET['id'];
$back = $baseURL.$queryImg;
if (isset($_POST['close']) || $id=='') {
	header("location: $back");exit;
}
$where = "AND product_id=$id";
?>
<section class="view">
	<div class="khung container">
		<div class="title clear">
			<h2>Sản Phẩm Liên Quan</h2>
			<form action="" method="post">
				<input type="submit" name="close" value="Đóng">
			</form>
		</div>
		<div class="show clear">
			<div class="avatar">
				<img src="img/products/1-0.jpg">
				<h2>chức năng chưa thực hiện</h2>
			</div>
			<div class="photo clear">
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
<!-- end show product relate -->

<script type="text/javascript" src="js/script.js"></script>
<?php
require('page_footer.php');
?>