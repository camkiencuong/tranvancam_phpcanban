<?php
session_start();
if (!isset($_SESSION['user']['username'])) {
    header('Location: index.php');
}


$error=[];
if (isset($_POST['submit'])) {
    if ($_FILES['avatar']['tmp_name'] != '') {
        $imgFile = getimagesize($_FILES['avatar']['tmp_name']);
        $imgType = strtolower(pathinfo($_FILES['avatar']['name'], PATHINFO_EXTENSION));
    } 
    if ($_FILES["avatar"]["error"] != 0) {
        $error[] = "Vui lòng chọn ảnh để Upload";
    } elseif (!is_array($imgFile)) {
        $error[] = "File chọn không phải là ảnh";
    } elseif ($_FILES["avatar"]["size"] > 2000000) {
        $error[] = 'Ảnh không được quá 2 MB';
    } else {
        $save = 'upload/' . $_FILES['avatar']['name'];
        $_SESSION['user']['avatar'] = $save;
        move_uploaded_file($_FILES['avatar']['tmp_name'], $save);
        header('Location: test.php');
    }
}
?>


<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Avatar</title>
    <link rel="stylesheet" type="text/css" href="css/styleindex.css">

</head>
<body>
    <div class="container">
        <div class="header">
            <div class="nameshow">
                <h2>
                    Xin Chào, 
                    <big><b><?php echo $_SESSION['user']['username'];?></b></big>
                </h2>
                <h1>Cập nhật ảnh đại diện</h1>
            </div>
        </div>
        <form action="" method="POST" enctype="multipart/form-data">
            <table>
                <tr>
                    <td colspan="2"><big>
                        <?php if (count($error)>0) :?>
                            <?php for ($i=0; $i < count($error); $i++) :?>
                                <div style="color:red"><?php echo $error[$i]; ?></div>
                            <?php endfor; ?>
                        <?php endif; ?></big>
                    </td>
                </tr>
                <tr>
                    <td>Chọn ảnh: </td>
                    <td><input type="file" name="avatar">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="dangnhap"><button type="submit" name="submit">Upload Avatar</button></td>
                </tr>
            </table>
        </form>
    </div>

</body>
</html>