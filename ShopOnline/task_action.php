<?php

date_default_timezone_set('Asia/Ho_Chi_Minh');

if ($_GET['act'] == 'add') {
	$num = (isset($_GET['num']) ? $_GET['num'] : 1);
	$idMax = $object->index('id','',1,0,'','ORDER BY id DESC',0)['id'];
} elseif ($_GET['act'] == 'edit' && isset($_GET['id']) && $_GET['id'] != '') {
	$num = 1;
	$where = " AND id=".$_GET['id'];
	$id_out = $_GET['id'];
	$edit_array = $object->index('*',$where);
	if (count($edit_array[0])==0) {
		header("location: $baseURL");exit;
	}
} elseif ($_GET['act'] == 'edit' && isset($_SESSION['id_edit'])) {
	$edit_array = $_SESSION['id_edit'];
	unset($_SESSION['id_edit']);
	if (count($edit_array)==0) {
		header("location: $baseURL");exit;
	}
	$num = count($edit_array);
}
