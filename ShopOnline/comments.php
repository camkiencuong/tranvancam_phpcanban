<?php 
session_start();
ob_start();
require('connect.php');
require('functions.php');
require('page_header.php');

$table = 'comments';
$baseURL = 'comments.php';
$page = (isset($_GET['page']) ? $_GET['page'] : 1);
$queryString = queryString($_GET, 'page');
$limit = 10;
$offset = ($page-1)*$limit;
$totalRecord = totalRecord($table);
$where = $join = $orderby = '';

$table_cols_in = ['id','post_id','user_id','fullname','phone','email','content','created_at','status'];
$table_cols_out = ['id','id tin tức','id thành viên','Họ tên','Số điện thoại','email','Nội dung','Tạo lúc','Trạng thái'];

$comments = getAll('*',$table,$where,$limit,$offset,$join,$orderby);

?>
<section>
	<div class="namepage clear">
		<div class="container">
			<h1>Quản lý Bình luận</h1>
		</div>
	</div>
	<div class="content tb-border tb-center clear">
		<div class="container">
			<h3>Tổng: <?php echo $totalRecord; ?></h3>
			<table border="1">
				<tr>
					<th colspan="3">Thao tác</th>
					<?php foreach ($table_cols_out as $out) :?>
					<th><?php echo $out; ?></th>
					<?php endforeach; ?>
				</tr>
					<?php for ($i=0; $i < count($comments); $i++): ?>
				<tr>
					<td class="options"><input type="checkbox" name="checkbox" class="checkbox"></td>
					<td class="options"><a href="">sửa</a></td>
					<td class="options"><a href="">xóa</a></td>
					<?php foreach ($table_cols_in as $in) :?>
					<td class="data-show"><?php echo $comments[$i][$in]; ?></td>
					<?php endforeach; ?>
				</tr>
					<?php endfor; ?>
			</table>
		</div>
	</div>
	<div class="paging clear">
		<div class="container">
			<?php echo paging($baseURL,$queryString,$totalRecord,$page,$limit);?>
		</div>
	</div>
</section>
<?php
require('page_footer.php');
?>