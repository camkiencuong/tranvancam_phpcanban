<?php 
session_start();
ob_start();
require('connect.php');
require('functions.php');
require('page_header.php');
require('class/ProductCategory.php');

$object = new ProductCategory();
$baseURL = 'product_categories.php';
$tableCols = ['id','name','slug','image','description','parent_id','meta_title','meta_keyword','meta_description','status'];

require('task_show.php');

if (!isset($_GET['act'])):
	$totalRecord = $object->totalRecord($where);
	$pro_cates = $object->index('*',$where,$limit,$offset,$join,$orderby);

?>

<!-- start show -->
<section class="sp-cate clear">
	<div class="namepage clear">
		<div class="container">
			<h1>Quản lý Danh mục Sản phẩm</h1>
		</div>
	</div>
	<div class="menubar clear">
		<div class="container">
			<ul>
				<li><a href="<?php echo $baseURL.'?act=add'; ?>">Thêm Danh Mục</a></li>
				<li class="message" style="padding-left:5%;">
					<?php if(isset($_SESSION['flash_msg'])) :?>
						<span><?php echo $_SESSION['flash_msg'];?></span>
						<?php unset($_SESSION['flash_msg']); endif;?>
						<?php if(count($error)>0):
						for ($i=0; $i < count($error); $i++) :?>
						<span><?php echo $error[$i]."<br>";?></span>
					<?php endfor; endif;?>
				</li>
			</ul>
		</div>
	</div>
	<div class="content object-show tb-border">
		<div class=" container number clear">
			Số dòng hiển thị: 
			<select onchange="window.location.href=this.value">
				<option value="<?php echo $baseURL.$queryLimit.'limit=10';?>" <?php if ($limit=='10') {echo 'selected';}?>>10</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=20';?>" <?php if ($limit=='20') {echo 'selected';}?>>20</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=50';?>" <?php if ($limit=='50') {echo 'selected';}?>>50</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=100';?>" <?php if ($limit=='100') {echo 'selected';}?>>100</option>
			</select>
		</div>
		<div class="container">
			<span><big>Tổng: <?php echo $totalRecord; ?></big></span>
			<div class="tuychinh">
				<span class="orderby">
					Sắp xếp: 
					<select onchange="window.location.href=this.value">
						<option value="<?php echo $baseURL.$queryBy.'col=id&by=ASC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'ASC') {echo 'selected';} ?> >id Tăng</option>
						<option value="<?php echo $baseURL.$queryBy.'col=id&by=DESC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'DESC') {echo 'selected';} ?> >id Giảm</option>
						<option value="<?php echo $baseURL.$queryBy.'col=name&by=ASC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'ASC') {echo 'selected';} ?> >name Tăng</option>
						<option value="<?php echo $baseURL.$queryBy.'col=name&by=DESC'; ?>" <?php if (isset($col) && $col=='name' && $by == 'DESC') {echo 'selected';} ?> >name Giảm</option>
					</select>
				</span>
				<span class="finding">
					<form action="" method="get">
						<input id="key" type="text" name="key" class="form" placeholder="từ khóa" value="<?php if(isset($_GET['key'])) {echo $_GET['key'];}?>">
						<input onclick="return testFind();" type="submit" name="find" value="Tìm Kiếm" class="button">
					</form>
				</span>
			</div>
			<?php if ($totalRecord == 0):?>
				<div class="container dt-center clear">Không có dữ liệu để hiển thị!</div>
			<?php else: ?>
				<div class="form-table clear">
					<form action="" method="post">
						<table border="1">
							<tr>
								<th colspan="3">Thao tác</th>
								<th>id</th>
								<th>Tên danh mục</th>
								<th>slug</th>
								<th>Hình ảnh</th>
								<th>Mô tả</th>
								<th>Danh mục cha</th>
								<th>meta title</th>
								<th>meta keyword</th>
								<th>meta description</th>
								<th>Trạng thái</th>
							</tr>
							<?php for ($i=0; $i < count($pro_cates); $i++) :?>
								<tr>
									<td class="options"><input type="checkbox" name="checkbox[]" value="<?php echo $pro_cates[$i]['id'];?>" class="checkbox" <?php if(isset($_POST['checkall'])) {echo 'checked';} ?>></td>
									<td class="options"><a href="<?php echo $baseURL.'?act=edit&id='.$sanpham[$i]['id'];?>">sửa</a></td>
									<td class="options"><a onclick="return confirm('Bạn chắc chắn muốn xóa không?');" href="<?php echo $baseURL.'?id_del='.$sanpham[$i]['id'];?>">xóa</a></td>
									<td class="data-show"><?php echo $pro_cates[$i]['id']; ?></td>
									<td class="data-show"><?php echo $pro_cates[$i]['name']; ?></td>
									<td class="data-show"><?php echo $pro_cates[$i]['slug']; ?></td>
									<td class="data-show"><?php echo $pro_cates[$i]['image']; ?></td>
									<td class="data-show"><?php echo $pro_cates[$i]['description']; ?></td>
									<td class="data-show"><?php echo $object->parent_id($pro_cates[$i]['parent_id']); ?></td>
									<td class="data-show"><?php echo $pro_cates[$i]['meta_title']; ?></td>
									<td class="data-show"><?php echo $pro_cates[$i]['meta_keyword']; ?></td>
									<td class="data-show"><?php echo $pro_cates[$i]['meta_description']; ?></td>
									<td class="data-show"><?php echo $object->status($pro_cates[$i]['status']); ?></td>
								</tr>
							<?php endfor; ?>
						</table>
						<div class="menubot clear">
							<input type="checkbox" id="selectall" onClick="selectAll(this)" class="checkbox">
							<button type="button" id="toggle" value="SelectAll" onClick="checkAll()">Chọn Tất</button>
							<input onclick="return testDel()" type="submit" name="multi_del" value="Xóa">
							<input onclick="return testEdit()" type="submit" name="multi_edit" value="Sửa">
							<input onclick="return testEdit()" type="submit" name="active" value="Kích Hoạt">
							<input onclick="return testEdit()" type="submit" name="block" value="Vô Hiệu">
							<select name="parent_id">
								<option value="0">-- không --</option>
								<?php foreach ($pro_cates as $category):
								if($category['parent_id']=='0'): ?>
								<option value="<?php echo $category['id'];?>"><?php echo $category['name'];?></option>
							<?php endif; endforeach;?>
						</select>
						<input onclick="return testEdit()" type="submit" name="parent" value="Danh mục cha">
					</div>
				</form>
			</div>
			<div class="paging clear">
				<div class="container">
					<?php echo paging($baseURL,$queryString,$totalRecord,$page,$limit);?>
				</div>
			</div>
		<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>
<!-- end show -->


<!-- start create  -->
<?php if (isset($_GET['act'])):

require('task_action.php');

if (isset($_POST['addnow'])) {
	$data = $_POST['data'];
	for ($i=0; $i < count($data); $i++) { 
		$id = trim($data[$i]['id']);
		$name = trim($data[$i]['name']);
		$slug = to_slug($name);
		$image = trim($data[$i]['image']);
		$description = trim($data[$i]['description']);
		$parent_id = $data[$i]['parent_id'];
		$meta_title = trim($data[$i]['meta_title']);
		$meta_keyword = trim($data[$i]['meta_keyword']);
		$meta_description = trim($data[$i]['meta_description']);
		$status = $data[$i]['status'];

		if ($name == '') {
			$error[] = "'Tên Danh Mục' id=$id không được trống!";
			continue;
		}
		$where = ' AND slug="'.$slug.'"';
		$testSlug = $object->index('*',$where);
		if (count($testSlug)!=0) {
			$error[] = "Slug = '$slug' đã tồn tại! (id=$id)";
			continue;
		}
		$str = [];
		foreach ($tableCols as $col) {
			$str[] = $$col;
		}
		$value_array[] = "'".implode("','", $str)."'";
	}
	if (count($error)==0) {
		$error[] = $object->create($value_array);
		if ($error[0] == '') {
			$_SESSION['flash_msg'] = 'Thêm Thành Công!';
			header("location: $baseURL");exit;
		}
	}
}
if (isset($_POST['editnow'])) {
	$data = $_POST['data'];
	for ($i=0; $i < count($data); $i++) { 
		$id = trim($data[$i]['id']);
		$name = trim($data[$i]['name']);
		$slug = to_slug($name);
		$image = trim($data[$i]['image']);
		$description = trim($data[$i]['description']);
		$parent_id = $data[$i]['parent_id'];
		$meta_title = trim($data[$i]['meta_title']);
		$meta_keyword = trim($data[$i]['meta_keyword']);
		$meta_description = trim($data[$i]['meta_description']);
		$status = $data[$i]['status'];

		$id_out = $edit_array[$i]['id'];
		$slug_out = $edit_array[$i]['slug'];
		$where = ' AND id='.$id.' AND id<>'.$id_out;
		$testid = $object->index('*',$where);
		if (count($testid)>0) {
			$error[] = 'lỗi id='.$id.' bị trùng!';
			continue;
		}
		if ($name == '') {
			$error[] = "'Tên Danh Mục' id=$id không được trống!";
			continue;
		}
		$where = " AND slug='$slug' AND slug<>'$slug_out'";
		$testslug = $object->index('*',$where);
		if (count($testslug)>0) {
			$error[] = "Slug = '$slug' đã tồn tại! (id=$id)";
			continue;
		}
		$id_array[] = 'id='.$id;
		$str = [];
		foreach ($tableCols as $col) {
			$str[] = "$col='".$$col."'";
		}
		$set_array[] = implode(", ", $str);
	}
	if (count($error) == 0) {
		$error[] = $object->update($set_array,$id_array);
		if ($error[0] == '') {
			$_SESSION['flash_msg'] = 'Sửa Thành Công!';
			header("location: $baseURL");exit;
		}
	}
	
}
?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<?php if ($_GET['act']=='add'):?>
				<h1>Thêm Danh mục Sản phẩm</h1>
			<?php endif; ?>
			<?php if ($_GET['act']=='edit'):?>
				<h1>Sửa Danh mục Sản phẩm</h1>
			<?php endif; ?>
		</div>
	</div>
	<div class="container" >
		<div class="message" style="padding-left:5%">
			<?php if(isset($_SESSION['flash_msg'])) :?>
				<span><?php echo $_SESSION['flash_msg'];?></span>
			<?php unset($_SESSION['flash_msg']); endif;?>
			<?php if(count($error)>0) :
				for ($i=0; $i < count($error); $i++) :?>
				<span><?php echo $error[$i]."<br>";?></span>
			<?php endfor; endif;?>
		</div>
	</div>
	<div class="back">
		<div class="container">
			<a onclick="return testBack()" href="<?php echo $baseURL;?>">Trở Về</a>
		</div>
	</div>
	<?php if ($_GET['act']=='add'):?>
		<div class="number clear">
			<div class="container">
				Số lượng dòng: 
				<select onchange="window.location.href=this.value">
					<option value="<?php echo $baseURL.'?act=add&num=1'; ?>" <?php if ($num=='1') {echo 'selected';}?>>1</option>
					<option value="<?php echo $baseURL.'?act=add&num=2'; ?>" <?php if ($num=='2') {echo 'selected';}?>>2</option>
					<option value="<?php echo $baseURL.'?act=add&num=5'; ?>" <?php if ($num=='5') {echo 'selected';}?>>5</option>
					<option value="<?php echo $baseURL.'?act=add&num=10'; ?>" <?php if ($num=='10') {echo 'selected';}?>>10</option>
					<option value="<?php echo $baseURL.'?act=add&num=20'; ?>" <?php if ($num=='20') {echo 'selected';}?>>20</option>
				</select>
			</div>
		</div>
	<?php endif;?>
	<div class="form-create clear">
		<div class="full-width">
			<form action="" method="post">
				<table>
					<tr>
						<th>id</th>
						<th>Tên danh mục</th>
						<th>slug</th>
						<th>Hình ảnh</th>
						<th>Mô tả</th>
						<th>Danh mục cha</th>
						<th>meta title</th>
						<th>meta keyword</th>
						<th>meta description</th>
						<th>Trạng thái</th>
					</tr>
					<?php for ($i=0; $i < $num; $i++):?>
						<tr>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][id]" placeholder="id (auto)" class="motnua dt-center" readonly value="<?php if(isset($edit_array)) {echo $edit_array[$i]['id'];} else {echo ++$idMax;} ?>">
							</td>
							<td>
								<input type="text" onkeyup="ChangeToSlug(this.value,<?php echo $i; ?>)" onchange="ChangeToSlug(this.value,<?php echo $i; ?>)" id='title<?php echo $i; ?>' name="data[<?php echo $i; ?>][name]" placeholder="tên danh mục" autofocus value="<?php if(isset($data)) {echo $data[$i]['name'];} elseif(isset($edit_array)) {echo $edit_array[$i]['name'];} ?>">
							</td>
							<td>
								<input type="text" id='slug<?php echo $i; ?>' name="data[<?php echo $i; ?>][slug]" placeholder="slug (auto)" readonly value="<?php if(isset($edit_array)) {echo $edit_array[$i]['slug'];} elseif(isset($data)) {echo $data[$i]['slug'];}?>">
							</td>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][image]" placeholder="path hình ảnh" value="<?php if(isset($data)) {echo $data[$i]['image'];} elseif(isset($edit_array)) {echo $edit_array[$i]['image'];} ?>">
							</td>
							<td>
								<input type="text" name="data[<?php echo $i; ?>][description]" placeholder="mô tả" value="<?php if(isset($data)) {echo $data[$i]['description'];} elseif(isset($edit_array)) {echo $edit_array[$i]['description'];} ?>">
							</td>
							<td>
								<select name="data[<?php echo $i; ?>][parent_id]">
									<option value="0">-- danh mục cha --</option>
									<?php foreach ($pro_cates as $category) :
									if ($category['parent_id']=='0') :?>
									<option value="<?php echo $category['id'];?>" <?php if(isset($data) && $data[$i]['parent_id'] == $category['id'] || isset($edit_array) && $edit_array[$i]['parent_id'] == $category['id']) {echo 'selected';} ?>><?php echo $category['name'];?></option>
								<?php endif; endforeach;?>
							</select>
						</td>
						<td>
							<input type="text" name="data[<?php echo $i; ?>][meta_title]" placeholder="meta_title" value="<?php if(isset($data)) {echo $data[$i]['meta_title'];} elseif(isset($edit_array)) {echo $edit_array[$i]['meta_title'];} ?>">
						</td>
						<td>
							<input type="text" name="data[<?php echo $i; ?>][meta_keyword]" placeholder="meta_keyword" value="<?php if(isset($data)) {echo $data[$i]['meta_keyword'];} elseif(isset($edit_array)) {echo $edit_array[$i]['meta_keyword'];} ?>">
						</td>
						<td>
							<input type="text" name="data[<?php echo $i; ?>][meta_description]" placeholder="meta_description" value="<?php if(isset($data)) {echo $data[$i]['meta_description'];} elseif(isset($edit_array)) {echo $edit_array[$i]['meta_description'];} ?>">
						</td>
						<td>
							<select name="data[<?php echo $i; ?>][status]">
								<option value="1" <?php if(isset($data) && $data[$i]['status'] == '1' || isset($edit_array) && $edit_array[$i]['status'] == '1') {echo 'selected';} ?>>Kích hoạt</option>
								<option value="0" <?php if(isset($data) && $data[$i]['status'] == '0' || isset($edit_array) && $edit_array[$i]['status'] == '0') {echo 'selected';} ?>>Vô hiệu</option>
							</select>
						</td>
					</tr>
				<?php endfor;?>
			</table>
			<div class="container button">
				<?php if ($_GET['act']=='add'):?>
					<input onclick="return testform(<?php echo $num; ?>)" type="submit" name="addnow" value="Thêm Vào" class="submit">
				<?php endif; ?>
				<?php if ($_GET['act']=='edit'):?>
					<input onclick="return testform(<?php echo $num; ?>)" type="submit" name="editnow" value="Cập Nhật" class="submit">
				<?php endif; ?>
			</div>
		</form>
	</div>
</div>
</section>
<?php endif; ?>
<!-- end create  -->

<script type="text/javascript" src="js/script.js"></script>
<?php
require('page_footer.php');
?>