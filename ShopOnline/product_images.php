<?php 
session_start();
ob_start();
require('connect.php');
require('functions.php');
require('page_header.php');
require('class/ProductImage.php');

$baseURL = 'product_images.php';
$tableCols = ['id', 'product_id', 'image', 'is_featured'];
$object = new ProductImage();

require('task_show.php');

if (!isset($_GET['act'])):
$totalRecord = $object->totalRecord($where);
$hinhanh = $object->index('*',$where,$limit,$offset,$join,$orderby);
if (count($hinhanh) > 0) {
	$src = isset($_GET['img']) ? $_GET['img'] : $hinhanh[0]['image'];
}

?>

<!-- start show -->
<section class="sp-cate clear">
	<div class="namepage clear">
		<div class="container">
			<h1>Quản lý Hình ảnh Sản phẩm</h1>
		</div>
	</div>
	<div class="menubar clear">
		<div class="container">
			<ul>
				<li></li>
				<li class="message" style="padding-left:5%;">
					<?php if(isset($_SESSION['flash_msg'])) :?>
						<span><?php echo $_SESSION['flash_msg'];?></span>
						<?php unset($_SESSION['flash_msg']); endif;?>
						<?php if(count($error)>0):
						for ($i=0; $i < count($error); $i++) :?>
						<span><?php echo $error[$i]."<br>";?></span>
					<?php endfor; endif;?>
				</li>
			</ul>
		</div>
	</div>
	<div class="content object-show tb-border">
		<div class=" container number clear">
			Số dòng hiển thị: 
			<select onchange="window.location.href=this.value">
				<option value="<?php echo $baseURL.$queryLimit.'limit=10';?>" <?php if ($limit=='10') {echo 'selected';}?>>10</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=20';?>" <?php if ($limit=='20') {echo 'selected';}?>>20</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=50';?>" <?php if ($limit=='50') {echo 'selected';}?>>50</option>
				<option value="<?php echo $baseURL.$queryLimit.'limit=100';?>" <?php if ($limit=='100') {echo 'selected';}?>>100</option>
			</select>
		</div>
		<div class="full-width">
			<span><big>Tổng: <?php echo $totalRecord; ?></big></span>
			<div class="tuychinh">
				<span class="orderby">
					Sắp xếp: 
					<select onchange="window.location.href=this.value">
						<option value="<?php echo $baseURL.$queryBy.'col=id&by=ASC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'ASC') {echo 'selected';} ?> >id Tăng</option>
						<option value="<?php echo $baseURL.$queryBy.'col=id&by=DESC'; ?>" <?php if (isset($col) && $col=='id' && $by == 'DESC') {echo 'selected';} ?> >id Giảm</option>
						<option value="<?php echo $baseURL.$queryBy.'col=product_id&by=ASC'; ?>" <?php if (isset($col) && $col=='product_id' && $by == 'ASC') {echo 'selected';} ?>>product_id Tăng</option>
						<option value="<?php echo $baseURL.$queryBy.'col=product_id&by=DESC'; ?>" <?php if (isset($col) && $col=='product_id' && $by == 'DESC') {echo 'selected';} ?>>product_id Giảm</option>
					</select>
				</span>
				<span class="finding">
					<form action="" method="get">
						<input id="key" type="text" name="key" class="form" placeholder="từ khóa" value="<?php if(isset($_GET['key'])) {echo $_GET['key'];}?>">
						<input onclick="return testFind();" type="submit" name="find" value="Tìm Kiếm" class="button">
					</form>
				</span>
			</div>
			<?php if ($totalRecord == 0):?>
			<div class="container dt-center clear">Không có dữ liệu để hiển thị!</div>
			<?php else: ?>
				<div class="form-table clear hinhanh">
					<form action="" method="post">
						<table border="1">
							<tr>
								<th colspan="3">Thao tác</th>
								<th>id</th>
								<th>Sản Phẩm</th>
								<th>Hình ảnh</th>
								<th>Đại diện</th>
							</tr>
							<?php for ($i=0; $i < count($hinhanh); $i++) :?>
								<tr>
									<td class="options"><input type="checkbox" name="checkbox[]" value="<?php echo $hinhanh[$i]['id'];?>" class="checkbox" <?php if(isset($_POST['checkall'])) {echo 'checked';} ?>></td>
									<td class="options"><a href="<?php echo $baseURL.'?act=edit&id='.$hinhanh[$i]['product_id'];?>">thêm/sửa</a></td>
									<td class="options"><a onclick="return confirm('Bạn chắc chắn muốn xóa không?');" href="<?php echo $baseURL.'?id_del='.$hinhanh[$i]['id'];?>">xóa</a></td>
									<td class="data-show"><?php echo $hinhanh[$i]['id']; ?></td>
									<td class="data-show"><?php echo $object->productName($hinhanh[$i]['product_id']); ?></td>
									<td class="data-show dt-center"><a href="<?php echo $baseURL.$queryImg.'img='.$hinhanh[$i]['image']; ?>" class="icon"><img src="<?php echo $hinhanh[$i]['image']; ?>"></a></td>
									<td class="data-show"><?php echo $object->isYes($hinhanh[$i]['is_featured']); ?></td>
								</tr>
							<?php endfor; ?>
							<tr>
								<td colspan="8">
									<div class="menubot">
										<input type="checkbox" id="selectall" onClick="selectAll(this)" class="checkbox">
										<button type="button" id="toggle" value="SelectAll" onClick="checkAll()">Chọn Tất</button>
										<input onclick="return testDel()" type="submit" name="multi_del" value="Xóa">
										<input onclick="return testEdit()" type="submit" name="multi_edit" value="Sửa">
									</div>
								</td>
							</tr>
						</table>
						<div class="xemanh">
							<img src="<?php echo $src; ?>">
							<address><?php echo $src; ?></address>
						</div>
					</form>
				</div>
				<div class="paging clear">
					<div class="container">
						<?php echo paging($baseURL,$queryString,$totalRecord,$page,$limit);?>
					</div>
				</div>
			<?php endif; ?>
		</div>
	</div>
</section>
<?php endif; ?>
<!-- end show -->













<!-- start create  -->
<?php if (isset($_GET['act'])):

$idMax = $object->index('id','',1,0,'','ORDER BY id DESC',0)['id'];

if ($_GET['act'] == 'edit' && isset($_GET['id']) && $_GET['id'] != '') {
	$num = 1;
	$where = " AND id=".$_GET['id'];
	$id_out = $_GET['id'];
	$edit_array = $object->getData('*','products',$where);
	if (count($edit_array[0])==0) {
		header("location: $baseURL");exit;
	}
} elseif ($_GET['act'] == 'edit' && isset($_SESSION['id_edit'])) {
	$edit_array = $_SESSION['id_edit'];
	unset($_SESSION['id_edit']);
	if (count($edit_array)==0) {
		header("location: $baseURL");exit;
	}
	$num = count($edit_array);
}

if (isset($_POST['editnow'])) {
	$data = $_POST['data'];
	


    if ($_FILES['avatar']['tmp_name'] != '') {
        $imgFile = getimagesize($_FILES['avatar']['tmp_name']);
        $imgType = strtolower(pathinfo($_FILES['avatar']['name'], PATHINFO_EXTENSION));
    } 
    if ($_FILES["avatar"]["error"] != 0) {
        $error[] = "Vui lòng chọn ảnh để Upload";
    } elseif (!is_array($imgFile)) {
        $error[] = "File chọn không phải là ảnh";
    } elseif ($_FILES["avatar"]["size"] > 2000000) {
        $error[] = 'Ảnh không được quá 2 MB';
    } else {
        $save = 'upload/' . $_FILES['avatar']['name'];
        $_SESSION['user']['avatar'] = $save;
        move_uploaded_file($_FILES['avatar']['tmp_name'], $save);
        header('Location: test.php');
    }




	if (count($error) == 0) {
		$error[] = $object->update($set_array,$id_array);
		if ($error[0] == '') {
			$_SESSION['flash_msg'] = 'Sửa Thành Công!';
			header("location: $baseURL");exit;
		}
	}
	
}
?>
<section class="sp_add clear">
	<div class="namepage">
		<div class="container">
			<?php if ($_GET['act']=='add'):?>
				<h1>Thêm sản phẩm Sản phẩm</h1>
			<?php endif; ?>
			<?php if ($_GET['act']=='edit'):?>
				<h1>Sửa sản phẩm Sản phẩm</h1>
			<?php endif; ?>
		</div>
	</div>
	<div class="container" >
		<div class="message" style="padding-left:5%">
			<?php if(isset($_SESSION['flash_msg'])) :?>
				<span><?php echo $_SESSION['flash_msg'];?></span>
				<?php unset($_SESSION['flash_msg']); endif;?>
				<?php if(count($error)>0) :
				for ($i=0; $i < count($error); $i++) :?>
				<span><?php echo $error[$i]."<br>";?></span>
			<?php endfor; endif;?>
		</div>
	</div>
	<div class="back">
		<div class="container">
			<a onclick="return testBack()" href="<?php echo $baseURL;?>">Trở Về</a>
		</div>
	</div>
	<div class="form-create clear tb-border">
		<div class="container">
			<form action="" method="post" enctype="multipart/form-data">
				<?php for ($i=0; $i < $num; $i++):?>
				<table border="1">
					<tr>
						<th>id</th>
						<th>sku</th>
						<th>Tên sản phẩm</th>
						<th>Giá</th>
						<th>màu sắc</th>
						<th>sizes</th>
						<th>Thương hiệu</th>
						<th>Danh mục</th>
					</tr>
					<tr>
						<td><?php if(isset($edit_array)) {echo $edit_array[$i]['id'];} else {echo ++$idMax;} ?></td>
						<td class="data-show"><?php echo $edit_array[$i]['sku']; ?></td>
						<td class=""><?php echo $edit_array[$i]['name']; ?></td>
						<td class="data-show"><?php echo $edit_array[$i]['price']; ?></td>
						<td class="data-show"><?php echo $edit_array[$i]['colors']; ?></td>
						<td class="data-show"><?php echo $edit_array[$i]['sizes']; ?></td>
						<td class="data-show"><?php echo $object->brandName($edit_array[$i]['brand_id']); ?></td>
						<td class="data-show"><?php echo $object->categoryName($edit_array[$i]['product_category_id']); ?></td>
					</tr>
				</table>
				<table>
					<tr>
						<td><input type="file" name="photo1"></td>
						<td class="dt-center radio"><input type="radio" name="is_featured" checked> Ảnh đại diện</td>
					</tr>
					<tr>
						<td><input type="file" name="photo2"></td>
						<td class="dt-center radio"><input type="radio" name="is_featured"> Ảnh đại diện</td>
					</tr>
					<tr>
						<td><input type="file" name="photo3"></td>
						<td class="dt-center radio"><input type="radio" name="is_featured"> Ảnh đại diện</td>
					</tr>
					<tr>
						<td><input type="file" name="photo4"></td>
						<td class="dt-center radio"><input type="radio" name="is_featured"> Ảnh đại diện</td>
					</tr>
				</table>
					<?php endfor;?>
				<div class="container button">
					<?php if ($_GET['act']=='add'):?>
						<input onclick="return testform(<?php echo $num; ?>)" type="submit" name="addnow" value="Thêm Vào" class="submit">
					<?php endif; ?>
					<?php if ($_GET['act']=='edit'):?>
						<input onclick="return testform(<?php echo $num; ?>)" type="submit" name="editnow" value="Cập Nhật" class="submit">
					<?php endif; ?>
				</div>
			</form>
		</div>
	</div>
</section>
<?php endif; ?>
<!-- end create  -->

<script type="text/javascript" src="js/script.js"></script>
<?php
require('page_footer.php');
?>