<?php

function getAll($field='*',$table='',$where='',$limit=10,$offset=0,$join='',$orderby='')
{
	global $db;
	if ($table=='') {
		return null;
	}
	$sql = sprintf("SELECT %s FROM %s %s WHERE 1=1 %s %s LIMIT %d OFFSET %d", $field, $table, $join, $where, $orderby, $limit, $offset);
	$query = $db->query($sql);

	return ($query ? $query->fetch_all(MYSQLI_ASSOC) : null);
}

function getOne($field='*',$table='',$where='',$join='',$orderby='')
{
	global $db;
	if ($table=='') {return null;}
	$sql = sprintf("SELECT %s FROM %s %s WHERE 1=1 %s %s", $field, $table, $join, $where, $orderby);
	$query = $db->query($sql);

	return ($query ? $query->fetch_assoc() : null);
}

function totalRecord($table='',$where='')
{
	global $db;
	if ($table=='') {return null;}

	$sql = sprintf("SELECT COUNT(*) FROM %s WHERE 1=1 %s", $table, $where);
	$query = $db->query($sql);
	$result = $query->fetch_row();

	return ($query ? $result[0] : 0);
}

function paging($baseURL='',$queryString='',$totalRecord=1,$page=1,$limit=10)
{
	if ($totalRecord==null || $totalRecord<=$limit) {return null;}
	$totalPage = ceil($totalRecord/$limit);
	$offset = ($page-1)*$limit;
	$active = '';
	
	$html = '<ul>';
// tạo nút prev
	$html .= sprintf("<li><a class='move' href='%s%spage=%d'>prev</a></li>", $baseURL, $queryString, $page-1);
// tạo các page
	for ($i=1; $i <= $totalPage; $i++) { 
		if ($page==$i) {$active='acti';}
		$html .= sprintf("<li><a href='%s%spage=%d' class='page %s'>%d</a></li>", $baseURL, $queryString, $i, $active, $i);
		$active='';
	}
// tạo nút next
	$html .= sprintf("<li><a class='move' href='%s%spage=%d'>next</a></li>", $baseURL, $queryString, $page+1);
	$html .= '</ul>';
	return $html;
}

function queryString($get='',$unset='')
{
	if (count($get)==0) {return '?';}

	if (is_array($unset)) {
		foreach ($unset as $key) {
			unset($get[$key]);
		}
	} else {
		unset($get[$unset]);
	}
	$queryString = http_build_query($get);

	return (($queryString == '') ? '?' : '?'.$queryString.'&');
}

function insertInto($table='',$table_cols='',$value='')
{
	global $db;
	if ($table=='' || count($table_cols)==0 || count($value)==0) {
		return 'Lỗi nạp dữ liệu!';
	}

	$cols = implode(',', $table_cols);
	for ($i=0; $i < count($value); $i++) { 
		$sql = sprintf("INSERT INTO %s (%s) VALUES (%s)", $table, $cols, $value[$i]);
		$query = $db->query($sql);
		if (!$query) {
			return 'lỗi '.$value[$i];
		}
	}
	return null;
}

function delete($table='',$where='',$table_relate='',$value_relate='',$table_relate_2='',$value_relate_2='')
{
	global $db;
	if ($table==''||$where=='') {
		return 'Lỗi thiếu dữ liệu đầu vào';
	}
	if (count($table_relate_2)!=0) {
		for ($i=0; $i < count($table_relate); $i++) { 
			for ($j=0; $j < count($table_relate_2[$i]); $j++) { 
				if (!isset($value_relate_2[$i][$j])) {continue;}
				$sql = sprintf("DELETE FROM %s WHERE 1=0 OR %s IN (%s)", $table_relate_2[$i][$j][0], $table_relate_2[$i][$j][1], $value_relate_2[$i][$j]);
				$query = $db->query($sql);
				if (!$query) {
					return 'Xóa Lỗi ở cấp 2! sql: '.$sql."<br>";
				}
			}
		}
	}
	if (count($table_relate)!=0 && $value_relate!='') {
		for ($i=0; $i < count($table_relate); $i++) { 
			$sql = sprintf("DELETE FROM %s WHERE 1=0 OR %s IN (%s)", $table_relate[$i][0], $table_relate[$i][1], $value_relate);
			$query = $db->query($sql);
			if (!$query) {
				return 'Xóa Lỗi ở cấp 1! hãy kiểm tra cấp 2! sql: '.$sql."<br>";
			}
		}
	}
	$sql = sprintf("DELETE FROM %s WHERE 1=0 %s", $table, $where);
	$query = $db->query($sql);
	if (!$query) {
		return 'Xóa Lỗi! hãy kiểm tra cấp 1 sql: '.$sql."<br>";
	}
	return 'Xóa Thành Công!';
}

function to_slug($str) {
	$str = trim(mb_strtolower($str));
	$str = preg_replace('/(à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ)/', 'a', $str);
	$str = preg_replace('/(è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ)/', 'e', $str);
	$str = preg_replace('/(ì|í|ị|ỉ|ĩ)/', 'i', $str);
	$str = preg_replace('/(ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ)/', 'o', $str);
	$str = preg_replace('/(ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ)/', 'u', $str);
	$str = preg_replace('/(ỳ|ý|ỵ|ỷ|ỹ)/', 'y', $str);
	$str = preg_replace('/(đ)/', 'd', $str);
	$str = preg_replace('/[^a-z0-9-\s]/', '', $str);
	$str = preg_replace('/([\s]+)/', '-', $str);
	return $str;
}


function update($table='',$set_cols='',$where='')
{
	global $db;
	if ($table=='' || count($set_cols)==0 || count($where)==0 || count($set_cols)!=count($where)) {
		return 'Lỗi nạp dữ liệu!';
	}
	for ($i=0; $i < count($where); $i++) {
		$sql = sprintf("UPDATE %s SET %s WHERE %s", $table, $set_cols[$i], $where[$i]);
		$query = $db->query($sql);
		if (!$query) {
			return 'Lỗi Cập Nhật '.$where;
		}
	}
	return null;
}

function create_sku($category_id='',$brand_id='',$product_id='')
{
	if ($category_id=='' || $brand_id=='' || $product_id=='') {
		return random_int(100000000, 1000000000);
	}
	$sku = '100'.$category_id.'00'.$brand_id.'00'.$product_id;
	return $sku;
}


